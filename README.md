Work in progress warning: Much stuff here is missing or outdated since it was written for an earlier version of the software. This readme is still work in progress.

##VR Spreadsheet Interactions

To get this project running you need:
- Unity (2019.3.5f1 or higher)
- Running Optitrack (Motive) system and various configured rigid bodies (more on this later)
- An optically tracked MS Surface
	- Was only tested on Surface Pro 4 and various parts of the project are currently hard-coded to assume it (such as browser resolution in Unity). But should in principle work on other versions as well
	- With some tweaking should work on generic notebooks as well
- A stylus / touch pen with at least 2 buttons that transmit clicks even when physically away from the laptop
- The accompanying touch-tracking application that will run on the laptop
- A google account set up to use the Google Sheets API and some pre-existing Google Sheets files to play around with.
- Visual Studio 2017 installed on the Surface (to build and run the surfaceinput project)
- For trying it out in VR, a Vive Headset that is also tracked using Optitrack. Other headsets probably work, too, but the project is only set up to use SteamVR and untested in other systems.

### Getting it running.

There are three parts to this: 1. Setting up the Optitrack rigid bodies and accompanying Unity objects. 2. Configuring and running the SurfaceInput project. 3. Configuring and running the Unity project.

To get a first look at the project you can open up the dev scene (Assets/Scenes/dev scene) and take in the environment.

####Setting up Optitrack rigid bodies

You'll need to configure three Optitrack rigid bodies: One for the pen, one for the surface, and if you want to try everything out in VR, one for the headset.
- Surface uses ID 70 in Unity
- Pen uses ID 6
- Headset by default is ID 3.
- (You can of course change these values at will)
- For the headset, refer to this guide: https://v22.wiki.optitrack.com/index.php?title=Template:HMD_Setup


The Unity objects representing these rigid bodies need to be manually aligned so their pivot points and coordinate system axes relative to the mesh geometry match the ones of the motive rigid body relative to the real world object.

- In general, we executed this matching by running the scene, displaying a large amount of unlabeled (not belonging to any rigid body) markers that were randomly placed on and around the object, moving desired objects around and copying over the transform via right-click on the transform -> Copy Component, stopping execution and right clicking -> Paste Component Values
 
- The pen itself has a separate calibration step that needs to be followed first. Use the "Standalone Pen Cal" scene for this. 
	- There are three pens already defined in there (ms surface pen, az link pen, az link pen 2). The old surface pen we don't use anymore and two AZLink pens. 
	  You should follow their structure if you need to define your own from scratch, for example by copying an existing pen object and swapping out the 3D model of the pen and redoing the calibration
	- The "Calibrate Tooltip" script does the actual calibration. It needs to be enabled and calibration itself is done when the scene is running.
		- Computes an offset from the rigid body position received from optitrack to the (referenced in the component) tooltip object and sets its position relative to the pen parent object.
		- The calibrated offset coordinates are stored in a text file whose filename can be set in the "Calibration Output File" field.
	- Calibration is performed by running the project and rotating the pen around its tip which should be fixed in position, while clicking the 'AddOne' of the Calibrate Tooltip script button at least 4 times (the calibration file will be output on the fourth click)
	  (More samples can be added, not sure if they actually improve quality). This essentially gathers samples on the sphere centered around the pen tip and the calibrator computes the tooltip offset from it.
	- Make sure the ID in the OptitrackRigidBody script fits yours in Motive
	- If a new output file with the provided name is created in the root directory of the project, you know that it worked. if there's no new file, make sure Unity has correct access privileges (run as admin in the worst case).
	- The tooltip contains the pen 3D model as a child (pose manually adjusted to match up with the real world geometry). You can swap this out with yours if you have one. In any case, the geometry will be misaligned unless
	  your physical rigid body looks exactly like ours (unlikely).
	- The tooltip position is sourced from calibration and shouldn't be changed. Offsetting should be done with the children below that.
	- We just used trial and error and translated and rotated the 3D model's gameobject around until it looked and felt right.
	- After all of this is done, you can turn your pen into a prefab by dragging it into some asset folder and use it in other scenes. The instance of this prefab will be modified later on in this guide because it's still missing
	  some spreadsheet-specific stuff.
		
- The MS Surface parent object in the dev scene is named "surface" and various parts of its children are 
  offset relative to it (in terms of position/rotation) to match up with real world geometry when tracked. You're going to have to adjust this, too.
	- At the time of writing this guide, there are several variations of the surface found as children of the object. These are for actual different surfaces with different rigid bodies in our lab. 
	- The dev scene contains a "Finger Tracker" component attached to the 'azlink pen with stick' object. When active, it visualizes unlabeled Optitrack markers as spheres in Unity world space. 
	- If you distribute unlabeled markers around the surface of the notebook, their virtual representations can be a great visual guide to get a good match since you can align the virtual screen to the spheres

#### Setting up and runnung the SurfaceInput project.

There's a small C# WPF application that transmits various information to a remote address using separate UDP connections for various information types. It's just an white fullscreen app that logs touches to the screen into a label.
It can be found at ./SurfaceInput/SurfaceInputWPF.sln

An important step here is changing the target IP address. Open up the project's MainWindow.xaml.cs and you should find the "ServerAddress" variable at the top. Change this to your actual destination PC that will run the Unity app.
Also make sure that the ports are not blocked by any firewalls in between the laptop and the server.

After that you should be able to build the project and either run it from VS directly or from its build directory.

#### Setting up and running the Unity project.
Open up the project and its dev scene. First, an overview of the important GameObjects found in the Hierarchy:

- The 'application stuff' object is home to things like the network communication component that talks to the laptops SurfaceInput project. It also contains an 'app config' script 
  which at the moment just references the currently used pen so application-wide scripts can reference it.
- Most visible objects are parented to the "Room" GameObject. Besides the headset, pen and surface, this also contains a tracked monitor and table from our lab, but these are disabled by default.
- The WheelMenu is controlled by the application code and by default ist just turned off and positioned far away so the user can't see it.
- The "azlink pen with stick" can be found here (currently used pen) as well as the "surface" object (tracked MS Surface)
- You'll need to replace the pen with yours from the calibration step. 
	- You'll need to add a new 'Sphere' GameObject to your pen and position it where the actual pen of the tip is. This sphere will be used in the code to interact
	  with the environment, via its Sphere Collider. 
	- Add the 'Pen Tip' component to this shpere. You should be able to leave all the values at their default.
	- Make sure to update the "Application Stuff"s App Config script reference to the ActivePen with your new PenTip.
	- Disable our old pens you don't need.
- The surface has several nested layers of hierarchy, with the Browser parented to the 3D Model of the surface's screen.
	- Most of the important stuff the application does is somewhere in the 'base browser' object there, but explaining it all is outside the scope of this guide.
- If the Vive remains disabled but you want to see something in the Game window, you can set up a separate MainCamera somewhere else,just make sure to disable it if you want to use the Vive Headset later.

##### Setting up
- Local domain redirects for windows hosts.
	- Because chromium forces all sites of a certain domain to have the same zoom level, the project uses different dummy domains (that all redirect to localhost) to be able to use (for example) the same underlying 
	  sheet on different browser instances at different zoom levels.
	- Find your hosts file at C:\Windows\System32\Drivers\etc\hosts and add:
	```
	127.0.0.1	localhostdummy.com
	127.0.0.1	localhostdummy1.com
	127.0.0.1	localhostdummy2.com
	127.0.0.1	localhostdummy3.com
	```
	
- All the various browser instances in Unity access actual HTML websites (which themselves often embed the Google Sheets themselves). These can all be found in the ./BrowserAssets directory.
	- An http server needs to be running on port 80 that serves these websites to local requests. Python has a simple one that can be started with 
		'python -m SimpleHTTPServer 80' in Python 2 and 'python -m http.server 80' in Python 3 (inside the BrowserAssets directory)
	- The 'browsercommon.js' file references your CLIENT_ID and the API_KEY, you'll need to swap them out for your own. In the uploaded version of this project, they are blank, so it won't work until you set up yours correctly.
		- refer to https://developers.google.com/sheets/api/quickstart/js  on how to get these
	- Each html file generally refers to a spreadsheet id and a default URL for your sheet. You need to swap these out for your own, too.
	- The domains used in Unity need to be whitelisted on your google API account. To do this, go to the google API dashboard, then into the google sheets dashboard, and either use 
	  pre-existing credentials there or set up new ones (this will give you a new client id that you need to swap out in browsercommon.js). Then go into its settings and add the following URLs as 
	  'Authorized Javascript origins':
		- https://game.local
		- http://localhost
		- https://localhost
		- http://localhost:8080
		- http://localhost:8000
		- http://localhostdummy.com
	- You may need to add google sheets API to your list of enabled APIs first. Also make sure you have some kind of project created in your account, early on in development of this project (which was many months ago at the time of writing of this readme), the API just didn't respond to requests otherwise.
	
	
- The browser plugin used in Unity uses a user-supplied profile path in order to store cookies (etc.) 
	- Important so you don't have to log in on every run of the application.
	- Set your path in "Application Stuff"'s Browser System Settings component. 
	- Make sure Unity has correct access rights to the supplied path. 
	
- There's a deactivated 'in case of browser login needed' game object near the bottom of the hierarchy.
	- This will be used to log into your google account whenever the cookies have expired.
	- To do so, enable the object, make sure other cameras in the scene are disabled, and then use the GUI browser (that will show up in the game window)
	  to log in.
	- Make sure you have set the profile path in "Application Stuff" correctly.
	- Mouseclicks are executed from the game view in Unity, but the position you click in there and what it actually clicks in the browser may be offset quite a bit, so you will have to experiment a bit to hit the button correctly. 
	- Theres also a separate "browser login scene" which may be more easy to use. Just set its profile path in the system settings component to a correct path in your system (like in all the other scenes of the project).
	- After you've logged in once, all sites on the same domain should log in automatically. If you need to log into different domains, you can just change the URL in the Browser component of the Browser game object

### Putting it all together
If you've followed the previous steps correctly, you should be able to hit the Play button in Unity and interact with the application using your pen and laptop.

### Navigating the code.

Writing full documentation for the code would be quite a task, but I'll point out the most important places and concepts used in the code:

	