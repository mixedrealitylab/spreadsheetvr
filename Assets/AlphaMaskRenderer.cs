﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaMaskRenderer : MonoBehaviour
{
    public Camera maskCamera;
    public RenderTexture AlphaMask;
    public Vector2Int Resolution;
    public Shader AlphaMaskShader;
    public float AlphaMaskValue;
    public GameObject[] ObjectsToRender;
    public float AspectRatio;
    public GameObject parent;
    public float SizeMultiplier = 1.0f;
    public float AspectMultiplier = 1.0f;

    public void RemoveObjectToRender(GameObject o)
    {
        int idx = Array.IndexOf(ObjectsToRender, o);
        if(idx >=0)
        {
            ObjectsToRender[idx] = ObjectsToRender[ObjectsToRender.Length - 1];
            Array.Resize(ref ObjectsToRender, ObjectsToRender.Length - 1);
        }
    }

    public void AddObjectToRender(GameObject o)
    {
        int idx = Array.IndexOf(ObjectsToRender, o);
        if (idx < 0)
        {
            Array.Resize(ref ObjectsToRender, ObjectsToRender.Length +1);
            ObjectsToRender[ObjectsToRender.Length - 1] = o;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (maskCamera == null) maskCamera = GetComponent<Camera>();

        AlphaMask = new RenderTexture(Resolution.x, Resolution.y, 0, RenderTextureFormat.RFloat);
        AlphaMask.name = "alphamask_texture";
        maskCamera.targetTexture = AlphaMask;
       
    }

    private struct ObjectInfo
    {
        public int oldLayer;
        public bool wasActive;
        public bool rendererActive;
    }

    public void RenderAlphaMask()
    {
        ObjectInfo[] infos = new ObjectInfo[ObjectsToRender.Length];

        Shader.SetGlobalFloat("_mrlabAlphaMaskValue", AlphaMaskValue);
        
        for(int i=0;i<ObjectsToRender.Length;++i)
        {
            var obj = ObjectsToRender[i];
            if (!obj.CompareTag("alpha_mask_object")) continue;

            var renderer = obj.GetComponent<Renderer>();
            infos[i] = new ObjectInfo
            {
                oldLayer = obj.layer,
                wasActive = obj.activeSelf,
                rendererActive = renderer.enabled
            };

            renderer.enabled = true;
            obj.SetActive(true);
            obj.layer = LayerMask.NameToLayer("alphamask_layer");
        }


        maskCamera.RenderWithShader(AlphaMaskShader, "");

        for (int i = 0; i < ObjectsToRender.Length; ++i)
        {
            var obj = ObjectsToRender[i];
            if (!obj.CompareTag("alpha_mask_object")) continue;

            obj.SetActive(infos[i].wasActive);
            obj.GetComponent<Renderer>().enabled = infos[i].rendererActive;
            obj.layer = infos[i].oldLayer;

        }
    }

    // Update is called once per frame
    void Update()
    {
        var parentSize = parent.transform.lossyScale;
        maskCamera.orthographicSize = (parentSize.y/2.0f) * SizeMultiplier;

        maskCamera.aspect = Mathf.Abs(parentSize.x / parentSize.y) * AspectMultiplier;
        RenderAlphaMask();
    }
}
