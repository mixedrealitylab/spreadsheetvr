﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignAlphaMask : MonoBehaviour
{
    public Renderer Renderer;
    public AlphaMaskRenderer alphaMaskRenderer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Renderer.material.SetTexture("_AlphaMask", alphaMaskRenderer.AlphaMask);
    }
}
