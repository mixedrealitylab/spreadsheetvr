﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheetSwitchTest : MonoBehaviour
{
    public int sheetIndex = 1;
    public GoogleSheetsInteraction sheetsInteraction;

    public bool switchToSheet = false;

    [Range(0.0f, 1.0f)]
    public float relCoordsX;
    [Range(0.0f, 1.0f)]
    public float relCoordsY;

    public GameObject debug;
    // Start is called before the first frame update
    void Start()
    {
                
    }

    // Update is called once per frame
    void Update()
    {
        if(switchToSheet)
        {
            switchToSheet = false;
           
        }

        //if(debug != null)
           // debug.transform.position = sheetsInteraction.MaskedInputAreas.NormalizedTouchCoordToWorldCoord(new Vector2(relCoordsX,relCoordsY));
    }
}
