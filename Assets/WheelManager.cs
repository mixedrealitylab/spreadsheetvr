﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[System.Serializable]
public enum WheelPositionMode
{
    STACKED,
    OFFSET
};

public class WheelManager : MonoBehaviour
{
    public WheelPenInteraction[] Wheels;

    public WheelPenInteraction BaseWheel;
    public WheelPenInteraction CurrentActiveWheel;
    public bool WheelMenuBlocked = false;

    [Header("Layout and Visuals")]
    public float StartDistance = 0.001f;
    public float Gap = 0.03f;
    public float InactiveAlpha = 0.5f;
    public float ActiveAlpha = 1.0f;
    public Vector2 WheelToParentOffset = Vector2.zero;
    public float WheelToParentDistance = 0.1f;


    private Stopwatch StartTimer = new Stopwatch();

    public WheelPositionMode WheelPositionMode;

    public AudioSource AudioSource;
    public AudioClip OnHoverSoundEffect;
    public AudioClip OnOpenSoundEffect;
    public AudioClip OnCloseSoundEffect;

    private Stopwatch TimeoutTimer = new Stopwatch();
    public float ReopenTimeout = 0.1f; //a timeout used to prevent the menu from being opened if it was just closed. used when certain menu options are selected!

    // Start is called before the first frame update
    void Start()
    {
        StartTimer.Start();
        TimeoutTimer.Start();

        if(AudioSource == null)
        {
            AudioSource = GetComponent<AudioSource>();
        }
    }

    public void PositionWheels(WheelPenInteraction node, Vector2 parentOffset, int depth)
    {
        var newOffset = parentOffset + WheelToParentOffset;

        //float currentAngle = 0.0f;
        float angleOffset = 360.0f / node.Pizza.Length;
        foreach(var slice in node.Pizza)
        {
            if(slice.SubMenu != null)
            {
                PositionWheels(slice.SubMenu,newOffset, depth+1);
            }
        }

        switch (WheelPositionMode)
        {
            case WheelPositionMode.STACKED:
                node.transform.localPosition = new Vector3(0, 0, -StartDistance - depth * Gap);
                break;
            case WheelPositionMode.OFFSET:
                node.transform.localPosition = new Vector3(newOffset.x, newOffset.y, -StartDistance - depth * Gap);
                break;
        }
        
    }

    public bool CanActivate()
    {
        if (TimeoutTimer.Elapsed.TotalSeconds > ReopenTimeout)
            return true;
        else
            return false;
    }

    public void ActivateWheel()
    {
        foreach (var wheel in Wheels)
        {
            if(wheel != BaseWheel) wheel.gameObject.SetActive(false);

            wheel.OnActivate();
        }

        BaseWheel.gameObject.SetActive(true);

        AudioSource.PlayOneShot(OnOpenSoundEffect);
    }

    public void DeactivateWheel(bool muteSoundEffect = false)
    {
        foreach(var wheel in Wheels)
        {
            wheel.gameObject.SetActive(false);
            wheel.OnDeactivate();
        }

        this.gameObject.SetActive(false);

        if(!muteSoundEffect) AudioSource.PlayClipAtPoint(OnCloseSoundEffect, transform.position);

        TimeoutTimer.Restart();
    }

    private void UpdateWheelVisibility(WheelPenInteraction wheel, bool activeEncountered, int depthFromInteractive)
    {
        foreach (var slice in wheel.Pizza)
        {
            if (slice.SubMenu != null)
            {
                if (slice != wheel.GetHoveredSlice() || ( activeEncountered && depthFromInteractive > 0))
                {
                    if(slice.SubMenu.gameObject.activeSelf) print("closing submenu: " + slice.wheelTitle);
                    wheel.CloseSubMenu(slice.SubMenu);
                }
                else
                {
                    if (!slice.SubMenu.gameObject.activeSelf) print("showing submenu: " + slice.wheelTitle);
                    slice.SubMenu.gameObject.SetActive(true);
                }

                if (wheel.EnabledForInteraction)
                {
                    depthFromInteractive = 0;
                }

                UpdateWheelVisibility(slice.SubMenu, activeEncountered || wheel.EnabledForInteraction, depthFromInteractive + 1);
            }
        }
    }

    //Update is called once per frame
    void Update()
    {
        WheelPenInteraction closest = null;
        float minDistance = float.MaxValue;
        foreach(var wheel in Wheels)
        {
            if (!wheel.gameObject.activeSelf) continue;

            var dist = Mathf.Abs(Vector3.Dot(wheel.transform.position - AppConfig.Instance.ActivePen.transform.position, this.transform.forward));
            if(dist < minDistance)
            {
                minDistance = dist;
                closest = wheel;
            }
            wheel.EnabledForInteraction = false;
            var mat = wheel.GetComponent<Renderer>().material;
            mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, InactiveAlpha);
        }

        if(closest != null)
        {
            closest.EnabledForInteraction = true;
            CurrentActiveWheel = closest;
            var mat = closest.GetComponent<Renderer>().material;
            mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, ActiveAlpha);
        }
        else
        {
            CurrentActiveWheel = null;
        }

        PositionWheels(BaseWheel,Vector2.zero,0);

        if(StartTimer.Elapsed.TotalSeconds > 1)
        {
            UpdateWheelVisibility(BaseWheel, BaseWheel.EnabledForInteraction, 0);
        }
        
    }
}
