﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrollbox : MonoBehaviour
{
    public static Scrollbox LastEnteredScrollbox;

    public Transform CellStartLocation;

    public Vector2Int PenLocationInCellCoords;
    public Collider EnteredCollider;
    public PenTip EnteredPenTip;
    public GoogleSheetsInteraction targetSheetsInteraction;
    public Transform ScrollCellReference;
    public Transform LocationIndicator;
    public Transform LocationPreviewIndicator;

    public bool PreviewIndicatorOnly = false;
    public bool RememberRelativePosition = false;
    public Vector2Int CurrentPositon = Vector2Int.zero;

    // Start is called before the first frame update
    void Start()
    {
        Communication.OnKey += Communication_OnKey;

        LocationPreviewIndicator.gameObject.SetActive(false);
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        if(EnteredPenTip != null && NetworkStylusInteraction.FirstButtonPressed(obj) && obj.pressed)
        {
            if(RememberRelativePosition)
            {
                CurrentPositon += PenLocationInCellCoords;
                print("current pos: " + CurrentPositon);
                print("cell location: " + PenLocationInCellCoords);
                
                targetSheetsInteraction.ScrollToCell(CurrentPositon.x, CurrentPositon.y);
            }
            else
            {
                targetSheetsInteraction.ScrollToCell(PenLocationInCellCoords.x, PenLocationInCellCoords.y);
            }
            
            PositionLocationIndicator(LocationIndicator);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(PreviewIndicatorOnly)
        {
            LocationIndicator.gameObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
        LocationPreviewIndicator.gameObject.SetActive(false);
        
        if(PreviewIndicatorOnly) LocationIndicator.gameObject.SetActive(false);
    }

    public void PositionLocationIndicator(Transform indicator)
    {
        var scale = indicator.localScale;
        indicator.localPosition = ComputeLocalPosition(PenLocationInCellCoords) + new Vector3(scale.x/2.0f, -scale.y/2.0f, 0.0f) + CellStartLocation.localPosition;
    }

    public Vector3 ComputeLocalPosition(Vector2Int cellpos)
    {
        var cellSize = ScrollCellReference.localScale;
        return new Vector3(cellpos.x * cellSize.x, -cellpos.y * cellSize.y, 0);
    }

    public Vector2Int ComputeCellPosition(Vector3 worldPos)
    {
        var start = CellStartLocation.localPosition;
        var local = this.transform.InverseTransformPoint(worldPos) - start;
        var cellSize = ScrollCellReference.localScale;

        int x = Mathf.FloorToInt((local.x) / cellSize.x);
        int y = Mathf.FloorToInt((-local.y) / cellSize.y);

        return new Vector2Int(x,y);
    }

    private void OnTriggerEnter(Collider other)
    {
        var pen = other.GetComponent<PenTip>();
        if (pen)
        {
            EnteredPenTip = pen;
            EnteredCollider = other;
            PenLocationInCellCoords = ComputeCellPosition(other.transform.position);
            PenLocationInCellCoords.x = Math.Max(PenLocationInCellCoords.x, 0);
            PenLocationInCellCoords.y = Math.Max(PenLocationInCellCoords.y, 0);

            LastEnteredScrollbox = this;
            LocationPreviewIndicator.gameObject.SetActive(true);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        var pen = other.GetComponent<PenTip>();
        if (pen)
        {
            PenLocationInCellCoords = ComputeCellPosition(other.transform.position);
            PositionLocationIndicator(LocationPreviewIndicator);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        EnteredPenTip = null;
        EnteredCollider = null;
        LocationPreviewIndicator.gameObject.SetActive(false);
    }

    
}
