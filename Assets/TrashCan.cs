﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashCan : PenHoverAndClickObject
{
    public Renderer Lid;
    public Renderer Can;

    public AudioClip DeleteClip;

    // Start is called before the first frame update
    public new void Start()
    {
        base.Start();
    }


    public override void OnInteraction(PenTip pentip)
    {
        if (pentip.AttachedFunc != null)
        {
            pentip.AttachedFunc.OnDelete(pentip);
            if(AudioSource != null && DeleteClip != null)
            {
                AudioSource.PlayOneShot(DeleteClip);
            }
        }
    }

    public override void ApplyHoverEffect(PenTip other)
    {
        Lid.material = HoverMaterial;
        Can.material = HoverMaterial;
    }

    public override void UnapplyHoverEffect(PenTip other)
    {
        Lid.material = RegularMaterial;
        Can.material = RegularMaterial;
    }
}
