﻿using System;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

//Controls which sheet is active on the laptop screen and can be used for cell selection, etc.
public class ActiveSheetSwitcher : MonoBehaviour
{
    public static ActiveSheetSwitcher Instance;
    public GoogleSheetsInteraction[] Browsers;

    public GoogleSheetsInteraction startBrowser;

    public SelectedCellTracker cellTracker;
    public NetworkStylusInteraction networkStylusInteraction;

    public int activeIdx = -1;
    public GoogleSheetsInteraction activeBrowser;

    public Vector3 averageOffset = Vector3.zero;

    public float AlphaFadePerIndex = 0.1f;
    public float SpacingLeftAndRight = 0.05f;

    [Header("Indicator Stuff")]
    public GameObject SheetIndicatorsLeft;
    public GameObject SheetIndicatorsRight;
    public GameObject SheetIndicatorPrefab;
    public float IndicatorSpacing = 0.01f;

    public GameObject[] SheetIndicators;

    [Header("Animation")]
    public float AnimationSpeed = 1.0f;
    public float SnapDistanceThreshold = 0.001f;
    public float MaxSpeed = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        InitStuff();
        PositionBrowsers();

        cellTracker.ActiveCellAdjuster = startBrowser.ActiveCellAdjuster;
    }

    [ContextMenu("Init stuff")]
    private void InitStuff()
    {
        //delete old sheet indicators in case there are any
        if(SheetIndicators != null && SheetIndicators.Length > 0)
        {
            foreach(var sheet in SheetIndicators)
            {
                if(sheet != null)
                {
                    if(Application.isPlaying)
                    {
                        GameObject.Destroy(sheet);
                    }
                    else
                    {
                        GameObject.DestroyImmediate(sheet);
                    }
                }
            }
        }

        SheetIndicators = new GameObject[Browsers.Length];

        

        for (int i = 0; i < Browsers.Length; ++i)
        {
            if (i != activeIdx)
            {
                Browsers[i].OnDeactivate();
            }


            SheetIndicators[i] = GameObject.Instantiate(SheetIndicatorPrefab);
            SheetIndicators[i].SetActive(true);
        }
        ActivateSheet(startBrowser);
        SheetIndicatorsLeft.SetActive(false);
        SheetIndicatorsRight.SetActive(false);

        
    }

    [ContextMenu("Position browsers")]
    private void PositionBrowsersInstantly()
    {
        //note: position stuff assumes they all have the same parent.
        for (int i = 0; i < Browsers.Length; ++i)
        {
            var realI = (i - activeIdx);

            var target = realI * averageOffset;

            if (realI != 0)
            {
                target += Mathf.Sign(realI) * averageOffset.normalized * SpacingLeftAndRight;
            }

            Browsers[i].transform.localPosition = target;

        }
    }

    public void UpdateIndicators()
    {
        for(int i=0;i<Browsers.Length;++i)
        {
            var indicator = SheetIndicators[i];

            var distanceToActiveIdx = Math.Abs(activeIdx - i);
            if (i < activeIdx)
            {
                indicator.transform.SetParent(SheetIndicatorsLeft.transform, false);
                indicator.SetActive(true);

                indicator.transform.localPosition = new Vector3(-distanceToActiveIdx * IndicatorSpacing, 0, 0);
            }
            else if (i > activeIdx)
            {
                indicator.transform.SetParent(SheetIndicatorsRight.transform, false);

                indicator.transform.localPosition = new Vector3(distanceToActiveIdx * IndicatorSpacing, 0, 0);
                indicator.SetActive(true);
            }
            else
            {
                indicator.SetActive(false);
                indicator.transform.SetParent(this.transform, false);
            }
        }
    }

    public void ToggleNonActiveBrowsers(bool Toggled)
    {
        //while the browsers are turned off, the indicators are turned on, and vice versa
        SheetIndicatorsLeft.SetActive(!Toggled);
        SheetIndicatorsRight.SetActive(!Toggled);

        for(int i=0;i<Browsers.Length;++i)
        {
            var sheet = Browsers[i];
            var indicator = SheetIndicators[i];

            if(!Toggled) 
            {
                UpdateIndicators();
            }
            

            if (sheet != activeBrowser)
            {
                sheet.gameObject.SetActive(Toggled);
            }
        }
    }

    private void PositionBrowsers()
    {
        //note: position stuff assumes they all have the same parent.
        for (int i = 0; i < Browsers.Length; ++i)
        {
            var realI = (i - activeIdx);

            var target =  realI * averageOffset;

            if(realI != 0)
            {
                target += Mathf.Sign(realI) * averageOffset.normalized * SpacingLeftAndRight;
            }

            var dist = (Browsers[i].transform.localPosition - target).magnitude;
            Browsers[i].transform.localPosition =  Vector3.MoveTowards(Browsers[i].transform.localPosition, target, Time.fixedDeltaTime * Mathf.Min(MaxSpeed, AnimationSpeed * dist));

            if((Browsers[i].transform.localPosition - target).magnitude < SnapDistanceThreshold)
            {
                Browsers[i].transform.localPosition = target;
            }
        }
    }

    public void SetActiveSheetIndex(int newIdx)
    {
        activeIdx = newIdx;
        PositionBrowsers();
    }

    public void ActivateSheet(GameObject g)
    {
        ActivateSheet(g.GetComponent<GoogleSheetsInteraction>());
    }

    public void ActivateSheet(GoogleSheetsInteraction sheetsInteraction)
    {
        var browser = sheetsInteraction.GetComponent<Browser>();
        networkStylusInteraction.browser = browser;
        networkStylusInteraction.sheetsInteraction = sheetsInteraction;
        networkStylusInteraction.stylusIndicator = sheetsInteraction.PointerIndicator;
        sheetsInteraction.PointerIndicator.SetActive(false);

        cellTracker.browser = browser;
        cellTracker.sheetsInteraction = sheetsInteraction;
        cellTracker.ActiveCellAdjuster = sheetsInteraction.ActiveCellAdjuster;

        if (activeIdx >= 0 && activeIdx < Browsers.Length)
        {
            Browsers[activeIdx].OnDeactivate();
            
        }

        activeBrowser = sheetsInteraction;
        var idx = Array.IndexOf(Browsers, sheetsInteraction);
        SetActiveSheetIndex(idx);
        Browsers[idx].OnActivate();
        UpdateIndicators();
    }

    [ContextMenu("Left")]
    private void Left()
    {
        if (activeIdx <= 0) return;
        ActivateSheet(Browsers[activeIdx - 1]);
    }

    [ContextMenu("Right")]
    private void Right()
    {
        if (activeIdx >= Browsers.Length) return;

        ActivateSheet(Browsers[activeIdx + 1]);
    }

    private void FixedUpdate()
    {
        PositionBrowsers();
    }

    // Update is called once per frame
    void Update()
    {
        var white = Color.white;
       
        for(int i=0;i<Browsers.Length;++i)
        {
            var dist = Mathf.Abs(i - activeIdx);
            var alpha = 1 - (AlphaFadePerIndex * dist);
            white.a = alpha;

            Browsers[i].GetComponent<Renderer>().material.color = white;
        }

        
    }
}
