﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using ZenFulcrum.EmbeddedBrowser;

[System.Serializable]
public class PizzaSlice
{
    public string wheelTitle; //title that is displayed on the wheel
    public PenAttachable Attachable; //attachable that should be used on the pen when the option is selected by the user. may be null (nothing happens then)
    public WheelPenInteraction SubMenu;
    public UnityEvent ActionToExecute;
}

public class WheelPenInteraction : MonoBehaviour
{
    public Browser browser;
    public PizzaSlice[] Pizza; //the Attachable items should be pre-existing objects (I suspect the code could be easily modified to use prefabs and create new instances every time, but that seems wasteful since they can be quite fat objects)

    private PizzaSlice CurrentHoveredSlice = null;
    public string CurrentHoveredSliceTitle; // just shown for debugging purposes
    public PenTip penTip;
    public bool EnabledForInteraction = false;
    public WheelManager Parent;

    public PizzaSlice GetHoveredSlice()
    {
        return CurrentHoveredSlice;
    }

    //Start is called before the first frame update
    void Start()
    {
        browser = GetComponent<Browser>();
        penTip = AppConfig.Instance.ActivePen;

        //args[0] is the title of the pizza slice, args[1] is the corresponding javascript mouse event 
        browser.RegisterFunction("unityHoverCallback", (args) =>
        {
            if (!EnabledForInteraction || !this.gameObject.activeSelf || !this.enabled) return;
            Parent.AudioSource.PlayOneShot(Parent.OnHoverSoundEffect);

            var title = (string)args[0];
            //Debug.Log("hover ON " + title);
       
            foreach(var slice in Pizza)
            {
                if(slice.wheelTitle == title)
                {
                    CurrentHoveredSlice = slice;
                    
                    //Debug.Log("active slice set to: " + title);
                    //if(slice.SubMenu)
                    //{
                    //    slice.SubMenu.gameObject.SetActive(true);
                    //}
                    break;
                }
            }
        });

        browser.RegisterFunction("unityHoverEndCallback", (args) =>
        {
            if (!EnabledForInteraction || !this.gameObject.activeSelf || !this.enabled) return;

            var title = (string)args[0];
            //Debug.Log("hover OFF: " + title);

            foreach (var slice in Pizza)
            {
                if (slice.wheelTitle == title)
                {
                    if(slice.SubMenu != null)
                    {
                        CloseSubMenu(slice.SubMenu);   
                    }
                    break;
                }
            }

            //if(CurrentHoveredSlice != null && CurrentHoveredSlice.wheelTitle == title)
            //{
            //    if(CurrentHoveredSlice.SubMenu)
            //    {
            //        CurrentHoveredSlice.SubMenu.gameObject.SetActive(false);
            //    }
            //    CurrentHoveredSlice = null;
            //}
        });

        Communication.OnKey += Communication_OnKey;
    }

    public void OnActivate()
    {
        CurrentHoveredSlice = null;
    }

    public void CloseSubMenu(WheelPenInteraction menu)
    {
        menu.OnDeactivate();
        menu.gameObject.SetActive(false);
    }

    public void OnDeactivate()
    {
        CurrentHoveredSlice = null;
        foreach(var slice in Pizza)
        {
            if(slice.SubMenu != null)
            {
                slice.SubMenu.OnDeactivate();
                slice.SubMenu.gameObject.SetActive(false);
            }
        }
    }

    public void UseSliceAttachable(PizzaSlice slice)
    {
        slice.Attachable.Activate(penTip.transform);
        Parent.DeactivateWheel(true);
    }

    public void ExecuteSliceFunction(PizzaSlice slice)
    {
        slice.ActionToExecute.Invoke();
        Parent.DeactivateWheel(true);
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        if (!EnabledForInteraction) return;

        if (penTip.AttachedFunc == null && (obj.keyCode == (KeyCode)19 || obj.keyCode == (KeyCode)26))
        {
            if (obj.pressed && CurrentHoveredSlice != null)
            {
                if(CurrentHoveredSlice.Attachable != null)
                {
                    UseSliceAttachable(CurrentHoveredSlice);
                }
                else if(CurrentHoveredSlice.ActionToExecute != null)
                {
                    ExecuteSliceFunction(CurrentHoveredSlice);
                }
            }
        }
    }

    public void UpdateInteraction()
    {
        var penPos = penTip.transform.position;
        var fromTo = penPos - this.transform.position;

        var x = Vector3.Dot(fromTo, this.transform.right);
        var y = Vector3.Dot(fromTo, this.transform.up);

        var mySize = this.transform.lossyScale;
        x /= mySize.x;
        y /= mySize.y;

        x += 0.5f;
        y = 1.0f - (y + 0.5f);

        //print("debug: " + x + " | " + y);

        BrowserNative.zfb_mouseMove?.Invoke(browser.ID, x, y);
    }

    private void OnEnable()
    {
        
    }

    private void OnDisable()
    {
        browser.CallFunction("deHoverAll");
    }

    // Update is called once per frame
    void Update()
    {
        if(CurrentHoveredSlice != null)
        {
            CurrentHoveredSliceTitle = CurrentHoveredSlice.wheelTitle;

            if(EnabledForInteraction && CurrentHoveredSlice.SubMenu !=null&& !CurrentHoveredSlice.SubMenu.gameObject.activeSelf)
            {
                print("showing submenu from update: " + CurrentHoveredSlice.SubMenu.name);
                CurrentHoveredSlice.SubMenu.gameObject.SetActive(true);
            }
        }
        else
        {
            CurrentHoveredSliceTitle = "";
        }

        if(EnabledForInteraction)
        {
            UpdateInteraction();
        }
    }
}
