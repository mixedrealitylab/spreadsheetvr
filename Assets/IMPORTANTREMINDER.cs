﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IMPORTANTREMINDER : MonoBehaviour
{
    [TextArea()]
    public string Reminder = "Change the User Agent attribute in the User System Settings compontent to this when trying to log into google. It fakes the browser into thinking it's a browser that's allowed by google's login services. Change it back after because " +
        "Google Sheets will complain about the browser not being supported";

    public string UserAgent = "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10136";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
