﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

public class BrowserTypeTextTest : MonoBehaviour
{
    public Browser browser;
    public string text="test";
    public bool type = false;

    // Start is called before the first frame update
    void Start()
    {
        //(browser.UIHandler as PointerUIBase).ForceKeyboardHasFocus(true);
    }

    // Update is called once per frame
    void Update()
    {
        var ev = new Event
        {
            type = EventType.KeyDown,
            character = 'h'
        };

        var keyCode = KeyMappings.GetWindowsKeyCode(ev);

        if (type)
        {
            type = false;

          
            BrowserNative.zfb_keyEvent(browser.ID, true, keyCode);
            BrowserNative.zfb_keyEvent(browser.ID, false, keyCode);

            //BrowserNative.zfb_characterEvent(browser.ID, (int)'h', keyCode);

        }

        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            BrowserNative.zfb_keyEvent(browser.ID, true, keyCode);
            BrowserNative.zfb_keyEvent(browser.ID, false, keyCode);
        }
    }
}
