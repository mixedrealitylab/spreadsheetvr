﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

public class BrowserJSTest : MonoBehaviour
{
    public Browser browser;

    public bool test = false;

    public TextAsset tasksJSON;

    private TasksFile tasksFile;

    public Transform testbettername;
     public Vector2 outTest;

    //Start is called before the first frame update
    void Start()
    {
                    
    }

    // Update is called once per frame
    void Update()
    {
        if(testbettername)
        {
            outTest = GoogleSheetsInteraction.WorldPosToTouchCoord(testbettername.transform.position, this.transform);
        }

        if(test)
        {
            
            tasksFile = JsonUtility.FromJson<TasksFile>(tasksJSON.text);

            test = false;
            browser.CallFunction("test").Then(args =>
           {
               var test = args[0];
               print(test);
           });
        }
    }
}
