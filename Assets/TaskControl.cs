﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

public class TaskControl : MonoBehaviour
{
    public Browser TaskBrowser;

    [Header("Debug Info")]
    public string CurrentTaskType = "none";

    // Start is called before the first frame update
    void Start()
    {
        TaskBrowser.RegisterFunction("unityBeginTaskCallback", (args) =>
        {

        });

        TaskBrowser.RegisterFunction("unityFinishTaskCallback", (args) =>
        {

        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
