﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestFuncCen : MonoBehaviour
{
    public GridCellInfo cell;
    public string TestString;
    public GoogleSheetsInteraction interaction;

    public bool run = false;
    // Start is called before the first frame update
    void Start()
    {
          
    }

    // Update is called once per frame
    void Update()
    {
        if(!run)
        {
            run = true;
            interaction.GenerateRelationship(cell, TestString);
        }
        
    }
}
