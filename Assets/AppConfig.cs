﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppConfig : MonoBehaviour
{
    public static AppConfig Instance;
    public PenTip ActivePen;

    AppConfig()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
