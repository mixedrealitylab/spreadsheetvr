﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClusterManager : MonoBehaviour
{
    public CellCluster[] Clusters;
    private Vector3[] ClusterOldPositions;
    public SelectedCellTracker cellTracker;
    public GenericGameObjectToggle ToggleSwitch;
    public GameObject RelationPrefab;

    public int UsedClusters = 0;

    public float startOffset = 0.1f;
    public float animationSpeed = 1.0f;
    private void OnEnable()
    {
        if(!Application.isPlaying) return;
        if(ClusterOldPositions == null) ClusterOldPositions = new Vector3[Clusters.Length];

        for(int i=0;i<Clusters.Length;++i)
        {
            ClusterOldPositions[i] = Clusters[i].transform.localPosition;
        }

        foreach(var cluster in Clusters)
        {
            var pos = cluster.transform.localPosition;
            pos.y = startOffset;
            cluster.transform.localPosition = pos;
        }
    }

    public GameObject MakeRelation(CellCluster cluster, GridCellInfo cellInfo)
    {
        GameObject result = GameObject.Instantiate(RelationPrefab);

        var relationInfo = result.GetComponent<ClusterRelation>();
        relationInfo.From = cluster;
        relationInfo.To = cellInfo;

        relationInfo.TubeTarget = TubeTarget.TargetCellCenter;
        
        result.transform.SetParent(cluster.transform, false);
        return result;
    }

    public void AddCluster()
    {
        //could rewrite this to create new clusters instead, but would need to implement some layouting algorithm
        if (UsedClusters >= Clusters.Length)
            return;

        var newCluster = Clusters[UsedClusters];

        var selectedCells = cellTracker.SelectedCells;
        newCluster.Cells = new GridCellInfo[selectedCells.Length];

        Array.Copy(selectedCells, newCluster.Cells, selectedCells.Length);

        UsedClusters++;

        newCluster.Relations = new GameObject[selectedCells.Length];
        for(int i=0;i<selectedCells.Length;++i)
        {
            var cell = selectedCells[i];

            newCluster.Relations[i] = MakeRelation(newCluster, cell);
            newCluster.Relations[i].SetActive(false);
        }

        ToggleSwitch.Toggle(true);

        cellTracker.ClearSelectedCells();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for(int i=0;i<UsedClusters;++i)
        {
            Clusters[i].transform.parent.gameObject.SetActive(true);
            Clusters[i].transform.localPosition = Vector3.MoveTowards(Clusters[i].transform.localPosition, ClusterOldPositions[i], animationSpeed);
        }

        for(int i=UsedClusters;i<Clusters.Length;++i)
        {
            Clusters[i].transform.parent.gameObject.SetActive(false);
        }
    }
}
