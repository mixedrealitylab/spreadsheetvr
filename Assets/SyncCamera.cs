﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncCamera : MonoBehaviour
{
    public Camera other;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = other.transform.position;
        this.transform.rotation = other.transform.rotation;
    }
}
