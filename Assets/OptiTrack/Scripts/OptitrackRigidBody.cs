﻿//======================================================================================================
// Copyright 2016, NaturalPoint Inc.
//======================================================================================================

using System;
using UnityEngine;


public class OptitrackRigidBody : MonoBehaviour
{
    public OptitrackStreamingClient StreamingClient;
    public Int32 RigidBodyId;

    [Header("Rotate")]
    public float x = 0.0f;
    public float y = 0.0f;
    public float z = 0.0f;

    public float positionFlipX = 1.0f;
    public float positionFlipY = 1.0f;
    public float positionFlipZ = 1.0f;

    public float orientationFlipX = 1.0f;
    public float orientationFlipY = 1.0f;
    public float orientationFlipZ = 1.0f;

    public bool VisibleInOptitrack = false;
    void Start()
    {
        // If the user didn't explicitly associate a client, find a suitable default.
        if (this.StreamingClient == null)
        {
            this.StreamingClient = OptitrackStreamingClient.FindDefaultClient();

            // If we still couldn't find one, disable this component.
            if (this.StreamingClient == null)
            {
                Debug.LogError(GetType().FullName + ": Streaming client not set, and no " + typeof(OptitrackStreamingClient).FullName + " components found in scene; disabling this component.", this);
                this.enabled = false;
                return;
            }
        }
    }

    public bool Position = true;
    public bool Rotation = true;

    void Update()
    {
        try
        {
            OptitrackRigidBodyState rbState = StreamingClient.GetLatestRigidBodyState(RigidBodyId);
            if (rbState != null)
            {
                VisibleInOptitrack = true;
                if (Position)
                {
                    this.transform.localPosition = Vector3.Scale(rbState.Pose.Position, new Vector3(positionFlipX, positionFlipY, positionFlipZ));
                }

                var orientation = rbState.Pose.Orientation * Quaternion.AngleAxis(y, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(x, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(z, new Vector3(0, 0, 1));
                orientation.x *= orientationFlipX;
                orientation.y *= orientationFlipY;
                orientation.z *= orientationFlipZ;

                if (Rotation) this.transform.localRotation = orientation;

                //Debug.Log(rbState.Pose.Orientation);

            }
            else
            {
                VisibleInOptitrack = false;
            }
        }
        catch
        {
            VisibleInOptitrack =false;
        }
    }
}
