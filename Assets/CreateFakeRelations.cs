﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct FakeRelation
{
    public GridCellInfo Target;
    public GridCellInfo[] Related;
}

[System.Serializable]
public struct CellMappingIndicator
{
    public GridCellInfo InSourceSheet;
    public GridCellInfo InTargetSheet;
}

public class CreateFakeRelations : MonoBehaviour
{
    public FakeRelation[] FakeRelations;
    public CellMappingIndicator[] CellMappingIndicators;

    public GoogleSheetsInteraction sheetsInteraction;
    public bool HideHandles = true;
    public Material IndicatorMaterial;
    public bool DrawIndicators;
    // Start is called before the first frame update
    void Start()
    {
        foreach(var relation in FakeRelations)
        {
            foreach(var related in relation.Related)
            {
                var obj = sheetsInteraction.MakeRelationObject(relation.Target, related);
                if(HideHandles)
                {
                    obj.Handle.SetActive(false);
                    obj.TubeTarget = TubeTarget.TargetCellCenter;
                }
            }
        }


        if(DrawIndicators) foreach(var indicator in CellMappingIndicators)
        {
            var obj = sheetsInteraction.MakeRelationObject( indicator.InTargetSheet,indicator.InSourceSheet);
            obj.GetComponentInChildren<TubeRenderer>().material = IndicatorMaterial;
            obj.GetComponentInChildren<TubeRenderer>().gameObject.GetComponent<MeshRenderer>().material = IndicatorMaterial;
            if (HideHandles)
            {
                obj.Handle.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
