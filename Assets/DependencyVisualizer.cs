﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DependencyVisualizer : MonoBehaviour
{
    public GameObject[] DependencyLayers;

    private Vector3[] StartPositions;

    public float animationSpeed = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        StartPositions = new Vector3[DependencyLayers.Length];
        
        for(int i=0;i<DependencyLayers.Length;++i)
        {
            StartPositions[i] = DependencyLayers[i].transform.localPosition;
        }

        ResetToStart();
    }

    private void OnDisable()
    {
        ResetToStart();
    }

    public void ResetToStart()
    {
        for (int i = 0; i < DependencyLayers.Length; ++i)
        {
            DependencyLayers[i].transform.localPosition = Vector3.zero;
        }
    }

    public void PositionLayers()
    {
        for (int i = 0; i < DependencyLayers.Length; ++i)
        {
            var dist = (DependencyLayers[i].transform.localPosition - StartPositions[i]).magnitude;
            DependencyLayers[i].transform.localPosition = Vector3.MoveTowards( DependencyLayers[i].transform.localPosition, StartPositions[i], animationSpeed * dist);

        }
    }

    // Update is called once per frame
    void Update()
    {
        PositionLayers();
    }
}
