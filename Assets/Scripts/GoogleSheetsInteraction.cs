﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

public delegate void GoogleSheetsCallbackFunc(JSONNode args);

[System.Serializable]
public class Sheet
{
    public int GID;
    public string Name;
}

public class GoogleSheetsInteraction : MonoBehaviour
{
    

    public string Url = "";
    [Header("Features")]
    public bool HasSideSheetToggle = true;
    public bool HasOverview = false;
    public bool HasDependencyOverview = false;
    public bool HasDataClusters = false;
    public bool HasStackedFunctionPreview = false;

    [Header("Feature Config")]
    public GameObject OverviewBrowser;
    public GameObject DependencyParent;
    public GameObject ClusterParent;
    public GameObject StackedFunctionParent;

    public GenericGameObjectToggle OverviewToggle;
    public GenericGameObjectToggle SideSheetToggle;
    public GenericGameObjectToggle ClusterToggle;
    public GenericGameObjectToggle DependencyToggle;
    public GenericGameObjectToggle StackedFunctionToggle;

    public bool InitExistingRelationships = true;

    [Header("Other stuff")]
    /*
     * todo: some of these need to be pulled out into an application-wide component. every browser has a GoogleSheetsInteraction but we only need 1 definition for all Sheet[] Sheets, the prefabs
     */

    public SelectedCellTracker selectedCellTracker;
    public event GoogleSheetsCallbackFunc OnGenericGoogleSheetsResponse;
    public event GoogleSheetsCallbackFunc OnSetFunctionResponse;
    public event GoogleSheetsCallbackFunc OnInsertChartResponse;
    public MaskedInputAreas MaskedInputAreas;
    public AlphaMaskRenderer AlphaMaskRenderer;

    public GameObject RelationPrefab;
    public GameObject NewRelationPrefab;
    public GameObject MoveRelationPrefab;
    public GameObject ToggleRelationHandlesPrefab;
    public GameObject PointerIndicator;
    public GameObject GazeIndicator;

    public ActiveSheetSwitcher SheetSwitcher;

    public CellAdjuster ActiveCellAdjuster;
    public WheelManager WheelMenu;
    public Browser browser;
    public GameObject[] OnlyVisibleWhenActive;
    public bool CurrentlyGazedAt = false;
    public bool DontGazeActiveSheet = true;

    public Sheet CurrentSheet = new Sheet
    {
        Name = "Sheet1",
        GID = 0
    };

    public float ZoomLevel = 8.0f;

    [Header("Layout stuff")]
    // Start is called before the first frame update
    public int ChartSizeX = 300;
    public int ChartSizeY = 300;

    [Header("Scroll Control Values")]
    public int CurrentScrollX = 0;
    public int CurrentScrollY = 0;
    public int CurrentCellScrollX = 0;
    public int CurrentCellScrollY = 0;

    public int CellSizeX = 50;
    public int CellSizeY = 50;
    public int MinScrollX = 96;
    public int MinScrollY = 64;

    [Header("DebugStuff")]
    public bool debugReset = false;
    public bool callFunction = false;
    public int deltaX = 0;
    public int deltaY = 0;
    public int numScrolls = 1;
    public int cellX = 0, cellY = 0;

    //this mode just translates touch input from the surface 1:1 to input in the browser. mostly used if you need to interact with the underlying web page without all the VR interactions (for logging into google for example)
    public bool SimpleClickMode = false;

    public Dictionary<string, GridCellInfo> CellsByName = new Dictionary<string, GridCellInfo>();

    public static Dictionary<string, Dictionary<string, List<GoogleSheetsInteraction>>> SheetsByAddressAndSheet = new Dictionary<string, Dictionary<string, List<GoogleSheetsInteraction>>>();

    // Start is called before the first frame update
    void Start()
    {
        if (selectedCellTracker == null) selectedCellTracker = FindObjectOfType<SelectedCellTracker>();
        if (browser == null) browser = FindObjectOfType<Browser>();
        if (GazeIndicator != null) GazeIndicator.SetActive(false);
        if (WheelMenu == null) WheelMenu = FindObjectOfType<WheelManager>();

        browser.LoadURL(Url, true);

        browser.RegisterFunction("GenericGoogleSheetsCallback", (args) =>
        {
            OnGenericGoogleSheetsResponse?.Invoke(args);
            print("generic js callback invoked");
        });

        browser.RegisterFunction("SetFunctionCallback", (args) =>
        {
            OnSetFunctionResponse?.Invoke(args);
            print("set function callback ");
        });

        browser.RegisterFunction("InsertChartCallback", (args) =>
        {
            OnInsertChartResponse?.Invoke(args);
            print("insert chart callback ");
        });

        browser.RegisterFunction("unityOnLoadCallback", (args) =>
        {
            UpdateCellValues();
        });

        InitGridCells();

        SwitchToSheet(CurrentSheet);

    }

    public ZenFulcrum.EmbeddedBrowser.Browser Browser;

    GridCellInfo minXCell = null;
    GridCellInfo maxXCell = null;
    GridCellInfo minYCell = null;
    GridCellInfo maxYCell = null;

    private void InitGridCells()
    {
        var cells = GetComponentsInChildren<GridCellInfo>();

        //parse range of cells so we know how many values to retrieve from the page
        int minx = int.MaxValue;
        int maxx = -int.MaxValue;
        int miny = int.MaxValue;
        int maxy = -int.MaxValue;

        foreach (var cell in cells)
        {
            if (!cell.RealGridCell || cell.sheetsInteraction != this) continue;

            cell.Sheet = CurrentSheet.Name;
            cell.SheetID = CurrentSheet.GID;
            CellsByName[cell.CellName] = cell;

            if (cell.RowIndex <= miny)
            {
                miny = cell.RowIndex;
                minYCell = cell;
            }
            if(cell.RowIndex >= maxy)
            {
                maxy = cell.RowIndex;
                maxYCell = cell;
            }
            if(cell.ColumnIndex <= minx)
            {
                minx = cell.ColumnIndex;
                minXCell = cell;
            }
            if(cell.ColumnIndex >= maxx)
            {
                maxx = cell.ColumnIndex;
                maxXCell = cell;
            }
        }

        
        browser.RegisterFunction("ReadCellValuesCallback", (args) =>
        {         
            var grid = args[0];

            //assuming this is a nested 2d array. might be null etc. if the sheet returns an error. hope for the best o.o
            var columns = (List<JSONNode>)grid.Value;
            for(int y=0;y<columns.Count;++y)
            {
                var column = (List<JSONNode>)(columns[y].Value);
                for (int x = 0; x < column.Count; ++x)
                {
                    string cellContent = column[x].Value.ToString();
                    var cellName = GetCellNameFromCoords(x, y);
                    GridCellInfo cell;
                    if(CellsByName.TryGetValue(cellName, out cell))
                    {
                        cell.SetContent(cellContent);
                    }
                }
            }

            if(InitExistingRelationships) GenerateRelationships(this.CellsByName);    
        });
    }
    
    public string ParseSheetName(string arg)
    {
        string result = "";

        for(int i=0;i<arg.Length;++i)
        {
            if(arg[i] == '!')
            {
                result = arg.Substring(0, i);
                break;
            }
        }

        return result;
    }

    //converts a range expression like A1:B3 into all the single cells it addresses
    public List<string> ParseRange(string arg)
    {
        //assumes single-letter columns for now
        List<string> result = new List<string>();

        string rangeLeft="";
        string rangeRight="";

        for(int i=0;i<arg.Length;++i)
        {
            if(arg[i] == ':')
            {
                rangeLeft = arg.Substring(0, i);
                rangeRight = arg.Substring(i + 1, arg.Length - (i + 1));
                break;
            }
        }

        char xStart = rangeLeft[0];
        char xEnd = rangeRight[0];
        int yStart = int.Parse(rangeLeft.Substring(1));
        int yEnd = int.Parse(rangeRight.Substring(1));

        for (int x = xStart; x <= xEnd; ++x)
        {
            for(int y=yStart;y<=  yEnd;++y)
            {
                result.Add((char)x + y.ToString());
            }
        }

        return result;
    }

    public GridCellInfo[] ParseCells(string cellsString)
    {
        List<GridCellInfo> cells = new List<GridCellInfo>();

        cellsString = cellsString.Replace(" ", "");

        List<string> parameters = new List<string>();
        int nextCellStartIndex = 0;
        for (int i = 0; i < cellsString.Length; ++i)
        {
            if(cellsString[i] == ',')
            {
                //this can either be a single cell, a single cell with sheet identifier, or a cell range. have to parse that, too
                var arg = cellsString.Substring(nextCellStartIndex, i - nextCellStartIndex);
                parameters.Add(arg);

                nextCellStartIndex = i + 1;
            }
        }

        var lastLength = cellsString.Length  - (nextCellStartIndex);
        if(lastLength > 0)
        {
            parameters.Add(cellsString.Substring(nextCellStartIndex, lastLength));
        }
        

        foreach(var param in parameters)
        {
            string arg = param;
            string sheetName = ParseSheetName(arg);
            if (sheetName != "") //remove e.g. sheet1! from the parameter string
            {
                arg = arg.Substring(sheetName.Length + 1);
            }

            List<string> argCells;
            //if it contains a ':', its a range, if not its a cell and we can add it directly
            if (arg.Contains(":"))
            {
                argCells = ParseRange(arg);
            }
            else
            {
                argCells = new List<string>();
                argCells.Add(arg);
            }

            if(sheetName == "") //at this point, if there was no sheet target in the argument, it's assumed to be from this same sheet
            {
                sheetName = CurrentSheet.Name;
            }

            //now try to add the cells!
            foreach (var cell in argCells)
            {
                var cellInfos = TryGetCellByNameAndSheet(sheetName, cell);

                foreach(var cellInfo in cellInfos)
                {
                    cells.Add(cellInfo);
                }
            }
        }

        return cells.ToArray();
    }

    public List<GridCellInfo> TryGetCellByNameAndSheet(string sheetName, string cellName)
    {
        List<GridCellInfo> result = new List<GridCellInfo>();
        Dictionary<string, List<GoogleSheetsInteraction>> bySheet;
        if (SheetsByAddressAndSheet.TryGetValue(Url, out bySheet))
        {
            List<GoogleSheetsInteraction> entries;
            if(bySheet.TryGetValue(sheetName, out entries))
            {
                foreach(var sheet in entries)
                {
                    GridCellInfo target;
                    if(sheet.CellsByName.TryGetValue(cellName, out target))
                    {
                        result.Add(target);
                    }
                }
            }
        }

        return result;
    }

    public void GenerateRelationship(GridCellInfo targetCell, string content)
    {

        //parse function from text
        if (content.Length > 0)
        {
            int openIdx = -1;
            int closeIdx = -1;

            for (int i = 0; i < content.Length; ++i)
            {
                if (content[i] == '(')
                {
                    openIdx = i;
                }
                else if (content[i] == ')')
                {
                    closeIdx = i;
                }
            }

            //we assume it has a function if there's a ( and ) in the formula. no recursive parsing for now :-(
            if (openIdx > -1 && closeIdx > -1)
            {
                string funcName = content.Substring(1, openIdx - 1);
                string funcCells = content.Substring(openIdx + 1, closeIdx - openIdx - 1);

                GridCellInfo[] cells;
                if (closeIdx - openIdx >= 2)
                {
                    cells = ParseCells(funcCells);
                }
                else
                {
                    cells = new GridCellInfo[0];
                }

                FuncCubeBasicFunc.MakeRelationship(targetCell, funcName, cells);
            }

        }
    }

    public void GenerateRelationships(Dictionary<string, GridCellInfo> cellsByName)
    {
        foreach(var entry in cellsByName)
        {
            var cell = entry.Value;

            var content = cell.Content;

            GenerateRelationship(cell, content);
        }
    }

    public static string GetCellNameFromCoords(int x,int y)
    {
        return ""+ (char)('A' + x) + "" + (y + 1);
    }

    public void SwitchToSheet(Sheet sheet)
    {
        UnregisterSheet(this, Url, CurrentSheet.Name);

        JSONNode gidarg = new JSONNode(sheet.GID);
        CurrentSheet = sheet;
        Browser.CallFunction("SwitchToSheet", gidarg);

        foreach(var cell in CellsByName)
        {
            cell.Value.Sheet = sheet.Name;
            cell.Value.SheetID = sheet.GID;
        }

        if (CellsByName.Count <= 0)
            return;

        var range = minXCell.ColumnName + minYCell.RowName + ":" + maxXCell.ColumnName + maxYCell.RowName;

        Browser.CallFunction("ReadCellValues", range, CurrentSheet.Name);

        RegisterSheet(this, Url, sheet.Name);
    }

    public void RegisterSheet(GoogleSheetsInteraction sheet,  string sheetAddress, string sheetTab)
    {
        Dictionary<string, List<GoogleSheetsInteraction>> bySheetTab;
        if(!SheetsByAddressAndSheet.TryGetValue(sheetAddress, out bySheetTab))
        {
            SheetsByAddressAndSheet[sheetAddress] = bySheetTab =  new Dictionary<string, List<GoogleSheetsInteraction>>();
        }

        List<GoogleSheetsInteraction> entries;
        if (!bySheetTab.TryGetValue(sheetTab, out entries))
        {
            bySheetTab[sheetTab] = entries = new List<GoogleSheetsInteraction>();
        }

        entries.Add(sheet);
    }

    public void UnregisterSheet(GoogleSheetsInteraction sheet, string sheetAddress, string sheetTab)
    {
        Dictionary<string, List<GoogleSheetsInteraction>> bySheetTab;

        if (SheetsByAddressAndSheet.TryGetValue(sheetAddress, out bySheetTab))
        {
            List<GoogleSheetsInteraction> entries;
            if (bySheetTab.TryGetValue(sheetTab, out entries))
            {
                entries.Remove(sheet);
            }
        }
    }

    [ContextMenu("Update Cell Values From Sheet")]
    public void UpdateCellValues()
    {
        var range = minXCell.ColumnName + minYCell.RowName + ":" + maxXCell.ColumnName + maxYCell.RowName;
        Browser.CallFunction("ReadCellValues", range, CurrentSheet.Name);
    }

    public void ExecuteFunction(string function, GridCellInfo targetCell, GridCellInfo[] range, bool updateRelationships = true)
    {
        if(range == null)
        {
            range = selectedCellTracker.SelectedCells;
        }

        JSONNode argFunc = new JSONNode(function);
        JSONNode argTargetField = new JSONNode(targetCell.GetCellName());
        JSONNode argRange = new JSONNode(selectedCellTracker.GetRangeString(range));

        Browser.CallFunction("setFunction", argFunc, argTargetField, argRange);
        targetCell.SetContent(function);
        if (updateRelationships)
        {
            foreach (var cell in range)
            {
                targetCell.Relationship.AddRelationship(cell);
            }
        }
    }

    public void ExecuteFunction(string function, GridCellInfo targetCell, bool updateRelationships = true)
    {
        ExecuteFunction(function, targetCell, selectedCellTracker.SelectedCells, updateRelationships);
    }

    public void ClearField(GridCellInfo targetCell)
    {
        JSONNode argTargetField = new JSONNode(targetCell.GetCellName());

        Browser.CallFunction("clearField", argTargetField);
        targetCell.ClearContent();
    }

    
    public static Vector2 WorldPosToTouchCoord(Vector3 worldPos, Transform browserPlane)
    {
        var localPos = browserPlane.InverseTransformPoint(worldPos);

        var touchPos = new Vector2( localPos.x + 0.5f, 1.0f - (localPos.y +0.5f) );

        return touchPos;
    }

    public GenericGameObjectToggle MakeRelationHandleToggleObject(GridCellInfo cell)
    {
        var toggle = GameObject.Instantiate(ToggleRelationHandlesPrefab);

        toggle.transform.SetParent(cell.transform, true);
        toggle.transform.localRotation = Quaternion.identity;

        toggle.SetActive(true);

        toggle.transform.position = cell.transform.position;

        var toggleHandle = toggle.GetComponentInChildren<GenericGameObjectToggle>(); 
        toggle.transform.position -= cell.transform.forward * toggleHandle.transform.lossyScale.z / 2.5f;
        toggle.transform.position -= (cell.transform.right * toggleHandle.transform.lossyScale.x) * 1.6f;

        toggle.transform.SetGlobalScale(ToggleRelationHandlesPrefab.transform.localScale);
        return toggleHandle;
    }

    public RelationInfo MakeNewRelationObject(GridCellInfo cell)
    {
        var newRelation = GameObject.Instantiate(NewRelationPrefab);

        newRelation.transform.SetParent(cell.transform, false);
        newRelation.transform.localPosition = Vector3.zero;
        newRelation.transform.localRotation = Quaternion.identity;
        //newRelation.transform.localScale = Vector3.one;

        var relationInfo = newRelation.GetComponent<RelationInfo>();
        relationInfo.From = cell;
        relationInfo.Handle.transform.localPosition = Vector3.zero;

        newRelation.SetActive(true);

        var prevParent = relationInfo.Handle.transform.parent;
        var oldScale = relationInfo.Handle.transform.localScale;
        relationInfo.Handle.transform.SetParent(null);

        var relativeForward = relationInfo.Handle.transform.InverseTransformDirection(cell.transform.forward);
        relationInfo.Handle.transform.position = cell.transform.position;
        relationInfo.Handle.transform.localScale = oldScale;
        relationInfo.Handle.transform.SetParent(prevParent);

        relationInfo.Handle.transform.position -= cell.transform.forward * relationInfo.Handle.transform.lossyScale.z / 2.0f;
        relationInfo.Handle.transform.position -= (cell.transform.right * relationInfo.Handle.transform.lossyScale.x ) * 0.6f;

        return relationInfo;
    }

    public RelationInfo MakeMoveRelationObject(GridCellInfo cell)
    {
        var newRelation = GameObject.Instantiate(MoveRelationPrefab);

        newRelation.transform.SetParent(cell.transform, false);
        newRelation.transform.localPosition = Vector3.zero;
        newRelation.transform.localRotation = Quaternion.identity;
        //newRelation.transform.localScale = Vector3.one;

        var relationInfo = newRelation.GetComponent<RelationInfo>();
        relationInfo.From = cell;
        relationInfo.Handle.transform.localPosition = Vector3.zero;

        newRelation.SetActive(true);

        var prevParent = relationInfo.Handle.transform.parent;
        var oldScale = relationInfo.Handle.transform.localScale;
        relationInfo.Handle.transform.SetParent(null);

        var relativeForward = relationInfo.Handle.transform.InverseTransformDirection(cell.transform.forward);
        relationInfo.Handle.transform.position = cell.transform.position;
        relationInfo.Handle.transform.localScale = oldScale;
        relationInfo.Handle.transform.SetParent(prevParent);

        relationInfo.Handle.transform.position -= cell.transform.forward * relationInfo.Handle.transform.lossyScale.z / 2.0f;
        relationInfo.Handle.transform.position -= (cell.transform.right * relationInfo.Handle.transform.lossyScale.x ) * -0.45f;

        return relationInfo;
    }

    public RelationInfo MakeRelationObject(GridCellInfo targetCell, GridCellInfo relatedCell)
    {
        var relation = GameObject.Instantiate(RelationPrefab);
        relation.gameObject.SetActive(true);
        var relationInfo = relation.GetComponent<RelationInfo>();

        relationInfo.From = targetCell;
        relationInfo.To = relatedCell;

        var vertices = relationInfo.TubeRenderer.vertices;
        var template = vertices[0];

        relation.transform.SetParent(targetCell.transform, false);
        relation.transform.localPosition = Vector3.zero;
        relation.transform.localRotation = Quaternion.identity;

        relationInfo.TubeColor = template.color;
        relationInfo.TubeRadius = template.radius;

        relationInfo.RebuildTube(relation.transform, relatedCell.transform);

        var prevParent = relationInfo.Handle.transform.parent;
        var oldScale = relationInfo.Handle.transform.localScale;
        relationInfo.Handle.transform.SetParent(null);

        var relativeForward = relationInfo.Handle.transform.InverseTransformDirection(relatedCell.transform.forward);
        relationInfo.Handle.transform.position = relatedCell.transform.position;
        relationInfo.Handle.transform.localScale = oldScale;
        relationInfo.Handle.transform.SetParent(prevParent);

        relationInfo.Handle.transform.position -= relatedCell.transform.forward * relationInfo.Handle.transform.lossyScale.z / 2.0f;
        relationInfo.Handle.transform.position -= (relatedCell.transform.right * relationInfo.Handle.transform.lossyScale.x) * 1.3f;

        return relationInfo;
    }

    public void RawScroll(int deltaX, int deltaY)
    {
        BrowserNative.zfb_mouseMove(browser.ID, 0.5f, 0.5f);
        BrowserNative.zfb_mouseScroll(browser.ID, -deltaX, 0);
        BrowserNative.zfb_mouseScroll(browser.ID,  0, -deltaY);
    }

    public void ScrollSheet(int deltaX, int deltaY)
    {
        BrowserNative.zfb_mouseMove(browser.ID, 0.5f, 0.5f);

        //below a certain value , chromium (or the google sheets app, not sure) clamps the scroll amount to a min value. we have to replicate the behaviour
        if (deltaX != 0)
        {
            var absX = Math.Abs(deltaX);
            deltaX = Math.Max(MinScrollX, absX) * Math.Sign(deltaX);
        }
        if (deltaY != 0)
        {
            var absY = Math.Abs(deltaY);
            deltaY = Math.Max(MinScrollY, absY) * Math.Sign(deltaY);
        }

        //scroll separately for each direction, chromium cant scroll x and y simultaneously
        BrowserNative.zfb_mouseScroll(browser.ID, 0, -deltaY);
        BrowserNative.zfb_mouseScroll(browser.ID, -deltaX, 0);

        //scroll only in cell size increments
        var realScrollX = (int)(deltaX / CellSizeX) * CellSizeX;
        var realScrollY = (int)(deltaY / CellSizeY) * CellSizeY;

        CurrentScrollX += realScrollX;
        CurrentScrollY += realScrollY;
        
        CurrentCellScrollX += (int)(deltaX / CellSizeX);

        var yCellDelta = (int)(deltaY / CellSizeY);
        if (Math.Abs(yCellDelta) == 1) //chromium google sheets only scrolls by a minimum of 2 cells in the y direction
        {
            CurrentCellScrollY += Math.Sign(yCellDelta) * 2;
        }
        else
        {
            CurrentCellScrollY += ( Math.Abs(yCellDelta) +1 ) * Math.Sign( yCellDelta );
        }

        //make sure the values dont go below 0 (cant scroll to negative coords)
        CurrentCellScrollX = Math.Max(0, CurrentCellScrollX);
        CurrentCellScrollY = Math.Max(0, CurrentCellScrollY);
        CurrentScrollX = Math.Max(0, CurrentScrollX);
        CurrentScrollY = Math.Max(0, CurrentScrollY);
    }

    public void ScrollToCell(int x, int y)
    {
        var diffX = x - CurrentCellScrollX;
        var diffY = y - CurrentCellScrollY;

        ScrollByCellAmount(diffX, diffY);
    }

    public void ScrollByCellAmount(int deltaX, int deltaY)
    {
        var pixelDeltaX = deltaX * CellSizeX;

        //(in chromium) google sheets has a minimum scroll distance of 2 cells in the y direction (1 in the x direction is fine), at least at zoom level 8 (others untested)
        //so we can only address every second row if we want to guarantee accurate view placement from anywhere
        deltaY = (deltaY / 2) * 2;
        int pixelDeltaY = 0;
        var absDelta = Math.Abs(deltaY);
        if (absDelta > 1)
        {
           pixelDeltaY = (absDelta- 1) * CellSizeY * Math.Sign(deltaY);
        }
        else
        {
            pixelDeltaY = 0; 
        }

        ScrollSheet(pixelDeltaX, pixelDeltaY);
    }

    public void InsertChartHorizontal(int sheetID, Vector2Int pos, Vector2Int rangeMin, Vector2Int rangeMax)
    {
        JSONNode idArg = new JSONNode(sheetID);
        JSONNode argPosX = new JSONNode(pos.x);
        JSONNode argPosY = new JSONNode(pos.y);
        JSONNode argSizeX = new JSONNode(ChartSizeX);
        JSONNode argSizeY = new JSONNode(ChartSizeY);
        JSONNode argChartId = new JSONNode(1);

        JSONNode argBottomXStart = new JSONNode(rangeMin.x);
        JSONNode argBottomXEnd = new JSONNode(rangeMax.x);
        JSONNode argBottomYStart = new JSONNode(rangeMin.y);
        JSONNode argBottomYEnd = new JSONNode(rangeMin.y+1);


        JSONNode argLeftXStart = new JSONNode(rangeMin.x);
        JSONNode argLeftXEnd = new JSONNode(rangeMax.x);
        JSONNode argLeftYStart = new JSONNode(rangeMin.y+1);
        JSONNode argLeftYEnd = new JSONNode(rangeMin.y + 2);

        Browser.CallFunction("insertChartHorizontal", idArg, argPosX, argPosY, argSizeX,argSizeY, argChartId, argBottomXStart, argBottomXEnd, argBottomYStart, argBottomYEnd, argLeftXStart, argLeftXEnd, argLeftYStart, argLeftYEnd);
    }

    public void OnActivate()
    {
        OverviewBrowser?.SetActive(HasOverview && OverviewToggle.Toggled);
        OverviewToggle?.gameObject.SetActive(HasOverview);

        SideSheetToggle?.gameObject.SetActive(HasSideSheetToggle);

        DependencyParent?.SetActive(HasDependencyOverview && DependencyToggle.Toggled);
        DependencyToggle?.gameObject.SetActive(HasDependencyOverview);

        if (ClusterParent != null) ClusterParent.SetActive(HasDataClusters && ClusterToggle.Toggled);
         if(ClusterToggle != null) ClusterToggle.gameObject.SetActive(HasDataClusters);

        if (StackedFunctionParent != null) StackedFunctionParent.SetActive(HasStackedFunctionPreview && StackedFunctionToggle.Toggled);
        if (StackedFunctionToggle != null) StackedFunctionToggle.gameObject.SetActive(HasStackedFunctionPreview);

        foreach (var obj in OnlyVisibleWhenActive)
        {
            obj.SetActive(true);
        }
    }

    public void OnDeactivate()
    {
        OverviewBrowser?.SetActive(false);
        OverviewToggle?.gameObject.SetActive (false);

        SideSheetToggle?.gameObject.SetActive (false);

        DependencyParent?.SetActive(false);
        DependencyToggle?.gameObject.SetActive(false);

        if(ClusterParent != null) ClusterParent.SetActive(false);
        if(ClusterToggle != null) ClusterToggle.gameObject.SetActive(false);

        if (StackedFunctionParent != null) StackedFunctionParent.SetActive(false);
        if (StackedFunctionToggle != null) StackedFunctionToggle.gameObject.SetActive(false);

        foreach (var obj in OnlyVisibleWhenActive)
        {
            obj.SetActive(false);
        }
    }

    [ContextMenu("Scroll to Cell")]
    public void ScrollToCell()
    {
        ScrollToCell(cellX, cellY);
    }

    public RegisteredSheet[] DebugRegisteredSheets;

    // Update is called once per frame
    void Update()
    {
        browser.Zoom = ZoomLevel;
        if(callFunction)
        {
            callFunction = false;
            //string rangeResult = selectedCellTracker.GetRangeString();

            //ExecuteFunction(FuncName,TargetField, rangeResult);
            //InsertChartHorizontal(new Vector2Int(0, 6), new Vector2Int(0, 3), new Vector2Int(5, 4));
            //for(int i=0;i<numScrolls;++i) ScrollSheet(deltaX, deltaY);
            //ScrollToCell(cellX, cellY);
            //ScrollByCellAmount(cellX, cellY);
            RawScroll(cellX, cellY);
        }

        if(debugReset)
        {
            ScrollSheet(-10000000, -100000000);
            debugReset = false;
        }

        if(AlphaMaskRenderer)
        {
            var mat = this.GetComponent<Renderer>().material;
            mat.SetTexture("_AlphaMask", AlphaMaskRenderer.AlphaMask);
        }

        
        if(GazeIndicator != null && SheetSwitcher != null) 
        {
            GazeIndicator.SetActive(CurrentlyGazedAt && (!DontGazeActiveSheet || SheetSwitcher.activeBrowser != this));
        }

        //List<RegisteredSheet> registeredSheets = new List<RegisteredSheet>();
        //foreach(var entry in SheetsByAddressAndSheet)
        //{
        //    foreach(var bySheetTab in entry.Value)
        //    {
        //        registeredSheets.Add(new RegisteredSheet { name = entry.Key + " : " + bySheetTab.Key, sheetsInteraction = bySheetTab.Value.ToArray() });
        //    }
        //}

        //DebugRegisteredSheets = registeredSheets.ToArray();
    }

    [System.Serializable]
    public struct RegisteredSheet
    {
        public GoogleSheetsInteraction[] sheetsInteraction;
        public string name;
    }
}
