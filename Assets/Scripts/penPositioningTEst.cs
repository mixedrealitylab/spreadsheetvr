﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class penPositioningTEst : MonoBehaviour
{
    public GameObject Tip;
    public Vector3 p_t = new Vector3(0,0.1f,0);

    // Start is called before the first frame update
    void Start()
    {
        
    }

    float[] resultRot = new float[9];

    // Update is called once per frame
    void Update()
    {
        Matrix4x4 rot = Matrix4x4.TRS(Vector3.zero, this.transform.rotation, Vector3.one);


        //rot = rot.inverse;
        
        resultRot[0] = rot[0]; resultRot[1] = rot[1]; resultRot[2] = rot[2];
        resultRot[3] = rot[4]; resultRot[4] = rot[5]; resultRot[5] = rot[6];
        resultRot[6] = rot[8]; resultRot[7] = rot[9]; resultRot[8] = rot[10];

        Matrix4x4 realRot = new Matrix4x4(new Vector4(resultRot[0], resultRot[1], resultRot[2], 0.0f),
                                          new Vector4(resultRot[3], resultRot[4], resultRot[5], 0.0f),
                                          new Vector4(resultRot[6], resultRot[7], resultRot[8], 0.0f),
                                          new Vector4(0,0,0,1));

        var rotateOffset = realRot * new Vector4(p_t.x, p_t.y, p_t.z, 1.0f);
        Tip.transform.position = this.transform.position + new Vector3(rotateOffset.x, rotateOffset.y, rotateOffset.z);
    }
}
