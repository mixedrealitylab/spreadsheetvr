﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerNetworkControl : MonoBehaviour
{
    public InformationLayers layers;

    public bool LayersActivated = false;
    // Start is called before the first frame update
    void Start()
    {
        Communication.OnKey += Communication_OnKey;    
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        if (obj.keyCode == (KeyCode)18)
        {
            if (obj.pressed == true)
            {
                LayersActivated = true;
            }
            else
            {
                LayersActivated = false;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        layers.enabled = LayersActivated;
    }
}
