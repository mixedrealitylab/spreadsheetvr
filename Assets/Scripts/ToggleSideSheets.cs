﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class ToggleSideSheets : GenericGameObjectToggle
{
    public ActiveSheetSwitcher sheetSwitcher;

    public new void Start()
    {
        base.Start();

        if (sheetSwitcher == null) sheetSwitcher = FindObjectOfType<ActiveSheetSwitcher>();
    }

    public override void Toggle(bool toggle)
    {
        base.Toggle(toggle);
        sheetSwitcher.ToggleNonActiveBrowsers(toggle);
    }

}

