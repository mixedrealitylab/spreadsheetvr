﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class FuncCubeBasicFunc : FuncCubeBase
{
    public string Function;

    public static void MakeRelationship(GridCellInfo cell, string function, GridCellInfo[] cells)
    {
        if (cell.Relationship != null)
        {
            cell.Relationship.Destroy();
        }

        var newRelationInfo = cell.sheetsInteraction.MakeNewRelationObject(cell);
        var moveRelationInfo = cell.sheetsInteraction.MakeMoveRelationObject(cell);

        cell.Relationship = new SheetRelationShipBasicFunc(function, cell, cell.sheetsInteraction.RelationPrefab, newRelationInfo, moveRelationInfo);

        foreach(var relatedCell in cells)
        {
            cell.Relationship.AddRelationship(relatedCell);
        }
    }

    public static void ApplyFunc(GridCellInfo cell, string function, GridCellInfo[] SelectedCells = null)
    {
        if (cell.Relationship != null)
        {
            cell.Relationship.Destroy();
        }

        var newRelationInfo = cell.sheetsInteraction.MakeNewRelationObject(cell);
        var moveRelationInfo = cell.sheetsInteraction.MakeMoveRelationObject(cell);

        cell.Relationship = new SheetRelationShipBasicFunc(function, cell, cell.sheetsInteraction.RelationPrefab, newRelationInfo, moveRelationInfo);

        cell.sheetsInteraction.ExecuteFunction(function, cell,SelectedCells);
    }

    public override void OnGridCellInteraction(GridCellInfo cell, PenTip pen)
    {
        base.OnGridCellInteraction(cell,pen);

        //////////////////////////////////
        //TODO: REMOVE COMMMENT ABOUT   //
        //      DESTROYING OLD RELATION-//
        //      SHIP, IT'S ALREADY DONE //
        //////////////////////////////////
        //////////////////////////////////
        //TODO: DESTROY OLD RELATIONSHIP//
        //////////////////////////////////
        ApplyFunc(cell, Function);
    }
}