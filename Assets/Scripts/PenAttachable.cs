﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenAttachable : MonoBehaviour
{
    public static PenAttachable LastTriggeredAttachable;

    public GameObject SelectionIndicator;
    public GameObject ActivationIndicator;
    public AudioSource AudioSource;
    public AudioClip PickupSoundEffect;
    public AudioClip GridCellInteractionSoundEffect;

    public bool IsActivated = false;
    public bool IsSelected = false;

    public Transform originalParent;
    private Vector3 originalPosition;
    private Quaternion originalRotation;
    private Vector3 originalScale;

    public bool UseDwellTime = false;
    public bool UseKeyActivation = true;
    public float ActivationHoverTime = 1.0f;
    public float ScaleOnAttachMultiplier = 0.3333f;
    public bool ZeroPositionOnAttach = true;
    public bool AllowCellSelectionWhileAttached = false;

    private System.Diagnostics.Stopwatch hoverTimer = new System.Diagnostics.Stopwatch();

    private Transform enteredTransform;
    private PenTip enteredPentip;

    //Start is called before the first frame update
    public void Start()
    {
        RememberParent();

        if(AudioSource == null)
        {
            AudioSource = GetComponent<AudioSource>();
        }

        Communication.OnKey += Communication_OnKey;
    }

    public void RememberParent()
    {
        originalParent = transform.parent.parent;
        originalPosition = transform.parent.localPosition;
        originalRotation = transform.parent.localRotation;
        originalScale = transform.parent.localScale;
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        if (!UseKeyActivation)
            return;

        if ((obj.keyCode == (KeyCode)19 || obj.keyCode == (KeyCode)26) && obj.pressed == false) //key.pause is the key sent by the first button on the AZLink key
        {
            if (IsSelected)
            {
                var penTip = enteredTransform.GetComponent<PenTip>();
                if(penTip.AttachedFunc == null && (GridCellInfo.LastSelectedGridCell == null || !GridCellInfo.LastSelectedGridCell.IsSelected))
                {
                    Activate();
                }
            }
        }
    }

    public virtual void OnGridCellInteraction(GridCellInfo cell, PenTip pen)
    {
        cell.Sheet = cell.sheetsInteraction.CurrentSheet.Name;
        if(AudioSource != null && GridCellInteractionSoundEffect != null)
        {
            AudioSource.PlayClipAtPoint( GridCellInteractionSoundEffect, cell.transform.position);
        }
    }

    public virtual void OnDelete(PenTip penTip)
    {

    }

    public virtual void OnPickup(PenTip penTip)
    {

    }

    public virtual void OnRelease(PenTip penTip)
    {

    }

    public void AttachToGameObject(Transform go)
    {
        var penTip = go.GetComponent<PenTip>();
        if (penTip)
        {
            penTip.OnAttach(this);
        }

        this.transform.parent.SetParent(go, true);
        this.transform.parent.localScale *= ScaleOnAttachMultiplier;
        if(ZeroPositionOnAttach) this.transform.parent.localPosition = Vector3.zero;

        hoverTimer.Start();
    }

    public void DetachFromGameObject()
    {
        this.transform.parent.SetParent(originalParent);
        this.transform.parent.localPosition = originalPosition;
        this.transform.parent.localRotation = originalRotation;
        this.transform.parent.localScale = originalScale;

        OnRelease(enteredPentip);

        IsSelected = false;
        IsActivated = false;

    }

    public void Activate(bool muteSoundEffect = false)
    {
        IsActivated = true;

        AttachToGameObject(enteredTransform);
        OnPickup(enteredPentip);
        if (AudioSource != null &&  PickupSoundEffect != null && !muteSoundEffect)  AudioSource.PlayOneShot(PickupSoundEffect);
    }

    public void Activate(Transform targetToAttachTo)
    {
        enteredTransform = targetToAttachTo;
        Activate();
    }

    // Update is called once per frame
    public void Update()
    {
        if (!IsActivated && IsSelected)
        {
            SelectionIndicator.SetActive(true);

            if (UseDwellTime && hoverTimer.Elapsed.TotalSeconds >= ActivationHoverTime)
            {
                Activate();
            }
        }
        else
        {
            SelectionIndicator.SetActive(false);
        }

        if (IsActivated)
        {
            ActivationIndicator.SetActive(true);
        }
        else
        {
            ActivationIndicator.SetActive(false);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        var pentip = other.transform.GetComponent<PenTip>();
        if(!pentip || pentip.AttachedFunc != null)
        {
            return;
        }



        print("entered pen attachable");
        IsSelected = true;

        enteredTransform = other.transform;
        enteredPentip = pentip;

        LastTriggeredAttachable = this;
        
        hoverTimer.Restart();
    }

    private void OnTriggerExit(Collider other)
    {
        IsSelected = false;
    }

    private void OnTriggerStay(Collider other)
    {

    }
}
