﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ReplacedReference
{
    GameObject Old;
    GameObject New;
}

[ExecuteInEditMode]
public class CellAdjuster : MonoBehaviour
{
    public GameObject prototype;
    public AlphaMaskRenderer alphaMaskRenderer; //in order to automatically add cells to the mask renderer
    public CreateFakeRelations fakeRelations;

    public int NumColumns = 5;
    public int NumRows = 5;

    public float GapX = 0.0f, GapY = 0.0f;
    public float OffsetX = 0.0f, OffsetY = 0.0f;

    //these prevent the cells at the edges from going outside the cell area in the browser
    //these are lazy manual float fix factors so I don't have to implement clipping against the input mask areas
    public float LastColumnScaleFactor = 1.0f;
    public float LastRowScaleFactor = 1.0f; 

    public GameObject[] cells;


    private Dictionary<string, GridCellInfo> oldCells = new Dictionary<string, GridCellInfo>();
    private Dictionary<string, GridCellInfo> newCells = new Dictionary<string, GridCellInfo>();


    // Start is called before the first frame update
    void Start()
    {
       
    }


    public void RemoveOldCells()
    {
        foreach (var cell in cells)
        {
            if(alphaMaskRenderer)
            {
                var go = cell;
                alphaMaskRenderer.RemoveObjectToRender(go);
            }

            if(Application.isPlaying)
            {
                Object.Destroy(cell);
            }
            else
            {
                Object.DestroyImmediate(cell);
            }
        }
    }

    [ContextMenu("Update Cells")]
    public void GenerateCells()
    {
        newCells.Clear();
        oldCells.Clear();

        foreach(var cell in cells)
        {
            oldCells[cell.GetComponent<GridCellInfo>().CellName] = cell.GetComponent<GridCellInfo>();
        }

        var addedCells = new GameObject[NumColumns * NumRows];
        int idx = 0;
        for(int x=0;x<NumColumns;++x)
        {
            for (int y = 0; y < NumRows; ++y)
            {
                var cell = GameObject.Instantiate(prototype);

                cell.transform.SetParent(this.transform);
                cell.transform.localRotation = Quaternion.identity;

                var scale = prototype.transform.localScale;
                
                var cellX = (x + 0.5f) * prototype.transform.localScale.x + GapX * x + OffsetX;
                var cellY = (-y - 0.5f) * prototype.transform.localScale.y + GapY * -y - OffsetY;

                if (x == NumColumns - 1) //last column
                {
                    scale.x *= LastColumnScaleFactor;
                    cellX += (scale.x  - prototype.transform.localScale.x)/2.0f;
                }

                if (y == NumRows - 1) //last row
                {
                    scale.y *= LastRowScaleFactor;
                    cellY -= (scale.y - prototype.transform.localScale.y) / 2.0f;
                }

                cell.transform.localScale = scale;

                cell.transform.localPosition = new Vector3(cellX, cellY, 0.0f);

                var cellInfo = cell.GetComponent<GridCellInfo>();
                addedCells[idx] = cellInfo.transform.gameObject;
                idx++;

                cellInfo.RowIndex = y;
                cellInfo.ColumnIndex = x;
                cellInfo.ColumnName = ((char)('A' + x)).ToString();
                cellInfo.RowName = (y + 1).ToString();
                cellInfo.RealGridCell = true;

                cellInfo.transform.name = cellInfo.CellName = GoogleSheetsInteraction.GetCellNameFromCoords(x, y);

                newCells[cellInfo.CellName] = cellInfo;

                if (alphaMaskRenderer)
                {
                    alphaMaskRenderer.AddObjectToRender(cell.gameObject);
                }
            }

            

        }

        //update references for cells which were replaced in outside scripts
        UpdateReferences();

        if (cells != null)
        {
            RemoveOldCells();
        }

        cells = new GameObject[addedCells.Length];
        for (int i = 0; i < addedCells.Length; ++i)
        {
            cells[i] = addedCells[i];
        }
    }

    private void UpdateReferences()
    {
        if (!fakeRelations) return;

        for(int j=0;j<fakeRelations.FakeRelations.Length;++j)
        {
            var relation = fakeRelations.FakeRelations[j];

            for(int i=0;i<relation.Related.Length;++i)
            {
                var cell = relation.Related[i];
                GridCellInfo oldCell, newCell;

                if(oldCells.TryGetValue(cell.CellName, out oldCell) && newCells.TryGetValue(cell.CellName, out newCell))
                {
                    if(oldCell == cell) //cell with the same name could be from another sheet
                    {
                        relation.Related[i] = newCells[cell.CellName];
                    }
                    
                }
            }

            {
                GridCellInfo oldCell, newCell;
                if (oldCells.TryGetValue(relation.Target.CellName, out oldCell) && newCells.TryGetValue(relation.Target.CellName, out newCell))
                {
                    if(oldCell == relation.Target)
                    {
                        relation.Target = newCells[relation.Target.CellName];
                    }
                }
            }
        }

        for(int i=0;i<fakeRelations.CellMappingIndicators.Length;++i)
        {
            ref var indicator = ref fakeRelations.CellMappingIndicators[i];

            {
                GridCellInfo oldCell, newCell;
                if (oldCells.TryGetValue(indicator.InSourceSheet.CellName, out oldCell) && newCells.TryGetValue(indicator.InSourceSheet.CellName, out newCell))
                {
                    if(oldCell == indicator.InSourceSheet)
                    {
                        indicator.InSourceSheet = newCells[indicator.InSourceSheet.CellName];
                    }
                }
            }

            {
                GridCellInfo oldCell, newCell;
                if (oldCells.TryGetValue(indicator.InTargetSheet.CellName, out oldCell) && newCells.TryGetValue(indicator.InTargetSheet.CellName, out newCell))
                {
                    if (oldCell == indicator.InTargetSheet)
                    {
                        indicator.InTargetSheet = newCells[indicator.InTargetSheet.CellName];
                    }
                }
            }
        }
    }

    //Update is called once per frame
    void Update()
    {
    }
}
