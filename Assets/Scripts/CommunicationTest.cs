﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommunicationTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Communication.onRemoteTouches += Communication_onRemoteTouches;

        Communication.OnStylusButtonDown += Communication_OnStylusButtonDown;
        Communication.OnStylusButtonUp += Communication_OnStylusButtonUp;
        Communication.OnStylusMove += Communication_OnStylusMove;
        Communication.OnStylusTouchDown += Communication_OnStylusTouchDown;
        Communication.OnStylusTouchUp += Communication_OnStylusTouchUp;
    }

    private void Communication_OnStylusTouchUp(NormalizedStylusEvent obj)
    {
        print("stylus touch up");
    }

    private void Communication_OnStylusTouchDown(NormalizedStylusEvent obj)
    {
        print("stylus touch down");
    }

    private void Communication_OnStylusMove(NormalizedStylusEvent obj)
    {
        print("stylus move: " + obj.normalizedPosition);
    }

    private void Communication_OnStylusButtonUp(NormalizedStylusEvent obj)
    {
        print("stylus button up");
    }

    private void Communication_OnStylusButtonDown(NormalizedStylusEvent obj)
    {
        print("stylus button down");
    }

    public int cnt = 0;
    private void Communication_onRemoteTouches(NormalizedTouch[] touches)
    {
        Debug.Log("still working" + (++cnt));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
