﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenTip : MonoBehaviour
{
    public Transform AttachedObject;
    public PenAttachable AttachedFunc;

    public float SamplingPeriod = 0.025f;
    public float Alpha = 0.75f;

    private bool startFlag = false;
    
    public float WindowLength = 0.35f;
    public float ShakeThreshold = 2f * 9.81f * 0.75f;

    private int WindowLengthInSamples;
    private int Counter;

    private float LastXF = 0.0f, LastYF = 0.0f, LastZF = 0.0f;

    public float lastMagnitude = 0.0f;
    public float LargestMeasuredMagnitude = 0.0f;

    private System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();

    private Queue<float> averageMagnitude = new Queue<float>();
    public float averageMagnitudeLastFewFrames = 0.0f;
    public int averageMagnitudeFrameCount = 100;

    private bool firstMeasurement = true;

    public bool UseShakeDetach = false; //should i be able to detach attached objects with a shake
    public bool UseKeyDetach = true; //should i be able to detach attached objects with a key press on the pen?

    //Start is called before the first frame update
    void Start()
    {
        WindowLengthInSamples = (int) (WindowLength / SamplingPeriod);

        lastPosition = this.transform.position;
        lastVelocity = currentVelocity = currentAcceleration = Vector3.zero;

        timer.Start();

        Communication.OnKey += Communication_OnKey;
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        if(UseKeyDetach && ((int)obj.keyCode == 20 || obj.keyCode == (KeyCode)24)  && obj.pressed == false)
        {
            DetachAttachedObject();
        }
    }

    private Vector3 lastPosition;
    private Vector3 lastVelocity;
    private Vector3 currentVelocity;
    private Vector3 currentAcceleration;

    public void OnAttach(PenAttachable cube)
    {
        AttachedObject = cube.transform.parent;
        AttachedFunc = cube;
    }

    public void OnDetach()
    {
        AttachedObject = null;
        AttachedFunc = null;
    }

    public void MeasureAcceleration()
    {
        var pos = this.transform.position;

        if(!firstMeasurement)
        {
            currentVelocity = (pos - lastPosition) / Time.deltaTime;
            currentAcceleration = (currentVelocity - lastVelocity);
        }
        else
        {
            firstMeasurement = false;
            currentAcceleration = Vector3.zero;
            currentVelocity = Vector3.zero;
        }

        lastVelocity = currentVelocity;
        lastPosition = pos;
    }

    public void DetectShake()
    {
        if (!UseShakeDetach)
            return;

        var x = currentAcceleration.x;
        var y = currentAcceleration.y;
        var z = currentAcceleration.z;

        var xf = Alpha * x + (1f - Alpha) * LastXF;
        var yf = Alpha * y + (1f - Alpha) * LastYF;
        var zf = Alpha * z + (1f - Alpha) * LastZF;

        var magnitude = (Mathf.Abs(xf) + Mathf.Abs(yf) + Mathf.Abs(zf)) / 3.0f;
        LargestMeasuredMagnitude = Mathf.Max(magnitude, LargestMeasuredMagnitude);
        if (startFlag)
        {
            Counter++;
            if (Counter > WindowLengthInSamples)
            {
                Counter = WindowLengthInSamples;
                startFlag = false;
            }
        }

        if (lastMagnitude <= ShakeThreshold && magnitude > ShakeThreshold)
        {
            if(Counter == WindowLengthInSamples)
            {
                DetachAttachedObject();
            }

            Counter = 0;
            startFlag = true;
        }

        if(averageMagnitude.Count >= averageMagnitudeFrameCount)
        {
            averageMagnitude.Dequeue();
        }

        averageMagnitude.Enqueue(magnitude);

        var avg = 0.0f;
        foreach(var mag in averageMagnitude)
        {
            avg += mag;
        }

        avg /= averageMagnitude.Count;
        averageMagnitudeLastFewFrames = avg;

        lastMagnitude = magnitude;
    }

    public void DetachAttachedObject()
    {
        if(AttachedFunc != null)
        {
            AttachedFunc.DetachFromGameObject();

        }

        OnDetach();
    }

    // Update is called once per frame
    void Update()
    {
        MeasureAcceleration();
        DetectShake();

        if(timer.Elapsed.TotalSeconds > 2.0)
        {
            startFlag = true;
        }
    }
}
