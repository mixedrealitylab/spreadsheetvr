﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

public class SetBrowserZoom : MonoBehaviour
{
    public float Zoom = 1.0f;
    public Browser browser;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        browser.Zoom = Zoom;
    }
}
