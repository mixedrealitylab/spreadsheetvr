﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[ExecuteInEditMode]
public class LoadCalibration : MonoBehaviour
{
    public CalibrateTooltip calibrateTooltip;
    public GameObject PenModel;

    public bool ShouldPositionPenModel = false;
    
    // Start is called before the first frame update
    void Start()
    {
        if(calibrateTooltip == null)
        {
            calibrateTooltip = FindObjectOfType<CalibrateTooltip>();
        }

        var calib = File.ReadAllText(calibrateTooltip.CalibrationOutputFile);
        var realCalib = JsonUtility.FromJson<CalibrationOutput>(calib);

        this.transform.localPosition = new Vector3(realCalib.x, realCalib.y, realCalib.z);

        if(ShouldPositionPenModel) PositionPenModel();
    }

    public Vector3 TipPositionDebug;
    public void PositionPenModel()
    {
        
        var info = PenModel.GetComponent<SurfacePenInfo>();
        var tip = info.tip.transform;
        var back = info.back.transform;

        var fromBackToTip = calibrateTooltip.controller.transform.position - calibrateTooltip.tooltip.transform.position;

        var rot = Quaternion.FromToRotation(Vector3.forward, -fromBackToTip); /*Quaternion.AngleAxis(0.0f, Vector3.forward)*/
        PenModel.transform.rotation = rot;

        PenModel.transform.localPosition = Vector3.zero;
        var tipInTooltipSpace = calibrateTooltip.tooltip.transform.InverseTransformPoint( tip.transform.TransformPoint(tip.transform.localPosition));
        TipPositionDebug = tipInTooltipSpace;
        //var posOffset = (tip.transform.position - PenModel.transform.position).magnitude;
        //PenModel.transform.localPosition = calibrateTooltip.tooltip.transform.TransformDirection( PenModel.transform.forward) * posOffset;
        PenModel.transform.localPosition = -tipInTooltipSpace/2.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(ShouldPositionPenModel)
        {
            ShouldPositionPenModel = false;
            PositionPenModel();
        }
    }
}
