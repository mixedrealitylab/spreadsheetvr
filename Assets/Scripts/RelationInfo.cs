﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TubeTarget
{
    HandleCenter,
    TargetCellCenter
}

[ExecuteInEditMode]
public class RelationInfo : MonoBehaviour
{
    public GridCellInfo From;
    public GridCellInfo To;
    public TubeRenderer TubeRenderer;
    public GameObject Handle;
    public float TubeRadius = 0.01f;
    public Color TubeColor = Color.red;

    private GridCellInfo LastFrom;
    private GridCellInfo LastTo;
    private float LastRadius;
    public float[] BezierPoints = new float[] { 0.5f };
    public float BezierDragPointWeight = 0.5f;

    public TubeTarget TubeTarget = TubeTarget.HandleCenter;

    //Start is called before the first frame update
    void Start()
    {
        LastFrom = From;
        LastTo = To;
        LastRadius = TubeRadius;

        if(TubeRenderer == null) TubeRenderer = GetComponentInChildren<TubeRenderer>();    
    }

    Vector3 BezierQuadratic(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        return (1.0f - t) * ((1.0f-t) * p0 + t*p1) + t * ((1.0f-t) * p1+t*p2);
    }

    public void RebuildTube(Transform startTransform, Transform endTransform)
    {
        var up = -endTransform.forward;

        var startPos = startTransform.position;
        var endPos = endTransform.position;

        var startVertex = transform.InverseTransformPoint(startPos);
        var endVertex = transform.InverseTransformPoint(endPos);

        Vector3 start;
        Vector3 end;

        {
            var upStartPos = Vector3.Dot(up, startPos);
            var upEndPos = Vector3.Dot(up, endPos);

            if(upStartPos > upEndPos)
            {
                end = startPos;
                start = endPos;
            }
            else
            {
                end = endPos;
                start = startPos;
            }
        }

        var bezierDragPoint = transform.InverseTransformPoint( end - up * Vector3.Dot(up, end - start));

        var middle = startVertex + (endVertex - startVertex)/2;

        TubeRenderer.vertices = new TubeVertex[2 + BezierPoints.Length];
        TubeRenderer.vertices[0] = new TubeVertex(startVertex, TubeRadius, TubeColor);

        for(int i=0;i<BezierPoints.Length;++i)
        {
            TubeRenderer.vertices[1+i] = new TubeVertex(BezierQuadratic(startVertex,  middle + BezierDragPointWeight * ( bezierDragPoint - middle), endVertex, BezierPoints[i]), TubeRadius, TubeColor);
        }
        
        TubeRenderer.vertices[TubeRenderer.vertices.Length-1] = new TubeVertex(endVertex, TubeRadius,   TubeColor);

        TubeRenderer.Rebuild();
        TubeRenderer.RebuildTube = false;
    }

    // Update is called once per frame
    void Update()
    {
        Transform target = null;
        switch (TubeTarget)
        {
            case TubeTarget.HandleCenter:
                target = Handle.transform;
                break;
            case TubeTarget.TargetCellCenter:
                target = To.transform;
                break;
        }

        if (From != null && Handle != null)
        {
            RebuildTube(From.transform, target);
        }

        LastTo = To;
        LastFrom = From;
    }
}
