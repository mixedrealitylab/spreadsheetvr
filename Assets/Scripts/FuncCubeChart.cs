﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class FuncCubeChart : FuncCubeBase
{
    //public string Function;
    public override void OnGridCellInteraction(GridCellInfo cell, PenTip pen)
    {
        base.OnGridCellInteraction(cell, pen);

        var pos = new Vector2Int(cell.ColumnIndex, cell.RowIndex);
        var cells = cell.sheetsInteraction.selectedCellTracker.SelectedCells;

        if(cells != null)
        {
            Vector2Int min = new Vector2Int(int.MaxValue, int.MaxValue);
            Vector2Int max = new Vector2Int(int.MinValue, int.MinValue);

            foreach(var selectedCell in cells)
            {
                var x = selectedCell.ColumnIndex;
                var y = selectedCell.RowIndex;

                if (x < min.x) min.x = x;
                if (x > max.x) max.x = x;
                if (y < min.y) min.y = y;
                if (y > min.y) max.y = y;
            }

            max.x += 1;
            max.y += 1;
            cell.sheetsInteraction.InsertChartHorizontal(cell.SheetID, pos, min, max);
        }
    }
}