﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerParent : MonoBehaviour
{
    private GridCellInfo parent;
    // Start is called before the first frame update
    void Start()
    {
        parent = this.transform.parent.GetComponent<GridCellInfo>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        parent.OnTriggerEnter(other);
    }
}
