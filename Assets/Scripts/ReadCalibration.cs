﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;

public class ReadCalibration : MonoBehaviour
{
    public string ConfigPath = "pen_result.txt";
    public bool load = true;
    // Start is called before the first frame update
    void Start()
    {
        if(load)
        {
            string contents = File.ReadAllText(ConfigPath);
            string[] separated = contents.Split('\n');

            var style = new CultureInfo("en-US");
            Vector3 result = new Vector3(float.Parse(separated[0], style), float.Parse(separated[1], style), float.Parse(separated[2], style));
            this.transform.localPosition = result;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
