﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using System.Text;

public class NormalizedTouch
{
    public Vector2 normalizedPosition;
    public TouchPhase phase;
    public bool trackedThisFrame = false;
}

[System.Serializable]
public class TouchPinch
{
    public NormalizedTouch[] touches = new NormalizedTouch[2];
    public float Scale = 1.0f;
    public float StartDistance;
    public TouchPinch(NormalizedTouch[] touches)
    {
        this.touches[0] = touches[0];
        this.touches[1] = touches[1];

        StartDistance = (touches[0].normalizedPosition - touches[1].normalizedPosition).magnitude;
        //Debug.Log("startdistance is " + StartDistance);
    }
}

public enum StylusEventType
{
    TOUCH_DOWN = 0,
    TOUCH_UP = 1,
    BUTTON_DOWN = 2,
    BUTTON_UP = 3,
    MOVE = 4
};

public class NormalizedStylusEvent
{
    public Vector2 normalizedPosition;
    public StylusEventType type;
    
}

public class UdpReceiver
{
    public delegate void OnMessageReceivedFunc(byte[] bytes);
    public event OnMessageReceivedFunc OnMessageReceived;

    public UdpClient Client;
    Thread listenThread;
    public int Port;
    ConcurrentQueue<byte[]> packetsReceived = new ConcurrentQueue<byte[]>();

    public UdpReceiver(int port)
    {
        this.Port = port;
        
    }

    public void Start()
    {
        this.listenThread = new Thread(new ThreadStart(listen));
        this.listenThread.IsBackground = true;
        this.listenThread.Start();
    }

    private void listen()
    {
        try
        {
            this.Client = new UdpClient(this.Port);
            Debug.Log("Listen for UDP on port " + this.Port);
        }
        catch
        {

        }

        while (this.Client != null)
        {
            try
            {
                //receive bytes and store them into queue for later processing in Update()
                IPEndPoint src = new IPEndPoint(IPAddress.Any, 0);
                byte[] bytes = this.Client.Receive(ref src);
                this.packetsReceived.Enqueue(bytes);
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
    }

    public void Update()
    {
        while (!this.packetsReceived.IsEmpty)
        {
            byte[] bytes;

            try
            {
                this.packetsReceived.TryDequeue(out bytes);
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
                continue;
            }

            OnMessageReceived(bytes);
        }
    }

    public void Shutdown()
    {
        if(listenThread.IsAlive)
        {
            listenThread.Abort();
        }

        Client.Close();
    }
}

public struct KeyMessage
{
    public KeyCode keyCode;
    public bool pressed;
    public bool isRepeat;
}

public class Communication : MonoBehaviour
{
	public delegate void remoteTouches(NormalizedTouch[] touches);

	public delegate void PinchUpdateCallback(TouchPinch pinch);
    public delegate void PinchBeginCallback(TouchPinch pinch);
	public delegate void PinchEndCallback(TouchPinch pinch);

	public static event PinchUpdateCallback OnPinchUpdate;
	public static event PinchBeginCallback OnPinchBegin;
	public static event PinchEndCallback OnPinchEnd;

    public static event Action<NormalizedStylusEvent> OnStylusButtonUp;
    public static event Action<NormalizedStylusEvent> OnStylusButtonDown;
    public static event Action<NormalizedStylusEvent> OnStylusTouchUp;
    public static event Action<NormalizedStylusEvent> OnStylusTouchDown;
    public static event Action<NormalizedStylusEvent> OnStylusMove;

    public static event Action<KeyMessage> OnKey;

    public static event remoteTouches onRemoteTouches;

    public int portTouch = 1337;
    public int portKeys = 8652;
    public int portStylus = 8653;
	public int maxTouches = 5;      //most smartphones won't track more than 5 touches; keep this in sync with the smartphone app

    public Vector2 StylusTouchInputOffset = Vector2.zero;
    public Vector2 StylusTouchInputScale = Vector2.one;
	/* 
	we want to keep only the most relevant attributes of a Touch struct, which are the following:

	deltaPosition: Vector2 -> 2x Float (2x 4 bytes)
	phase: enum (1 byte should be enough)
	position: Vector2 -> 2x Float (2x 4 bytes)
	tapCount: int (4 bytes)

	That makes a total of 21 bytes, which is why we want to use 21 bytes per block (per Touch struct)
	The final amount of bytes is then: maxTouches*bytesPerBlock

	Keep in sync with SmartphoneApp
	*/
	readonly int bytesPerBlockTouch = sizeof(int)*2 + sizeof(byte)*2 + sizeof(int)*8; //see surface input project TouchesToBytes
    readonly int bytesPerBlockStylus = sizeof(byte) + sizeof(float) * 4;
    readonly int bytesPerBlockKey = sizeof(int) + sizeof(bool) + sizeof(bool);

    private List<UdpReceiver> receivers = new List<UdpReceiver>();

	private TouchPinch CurrentPinch = null;
	
    [ContextMenu("First Pen Button")]
    public void FakeFirstButton()
    {
        OnKey.Invoke(new KeyMessage
        {
            keyCode = (KeyCode)19,
            pressed = true,
            isRepeat = false
        });

        OnKey.Invoke(new KeyMessage
        {
            keyCode = (KeyCode)19,
            pressed = false,
            isRepeat = false
        });
    }

    [ContextMenu("Second Pen Button")]
    public void FakeSecondButton()
    {
        OnKey.Invoke(new KeyMessage
        {
            keyCode = (KeyCode)20,
            pressed = true,
            isRepeat = false
        });

        OnKey.Invoke(new KeyMessage
        {
            keyCode = (KeyCode)20,
            pressed = false,
            isRepeat = false
        });
    }
	//Unity is not thread safe, yet we have to run the UDP listener in a seperate Thread. In order to still use the data in Unity,
	//we will use a thread-safe ConcurrentQueue.
    void Start()
    {
        var touchReceiver = new UdpReceiver(portTouch);
        touchReceiver.OnMessageReceived += OnTouchMessageReceived;

        var stylusReceiver = new UdpReceiver(portStylus);
        stylusReceiver.OnMessageReceived += OnStylusMessageReceived;

        var keyReceiver = new UdpReceiver(portKeys);
        keyReceiver.OnMessageReceived += OnKeyMessageReceived;

        receivers.Add(touchReceiver);
        receivers.Add(stylusReceiver);
        receivers.Add(keyReceiver);

        foreach(var rec in receivers)
        {
            rec.Start();
        }
    }

    private void HandlePinch(NormalizedTouch[] touches)
    {   
        //hardcoded to two fingers for now
        if(CurrentPinch == null)
        {
            CurrentPinch  = new TouchPinch(touches);
			OnPinchBegin?.Invoke(CurrentPinch);
        }
        else
        {
            CurrentPinch.touches[0] = touches[0];
            CurrentPinch.touches[1] = touches[1];
            //check how scale changed
            var newDistance = (touches[0].normalizedPosition - touches[1].normalizedPosition).magnitude;

            
            var newScale =  newDistance / CurrentPinch.StartDistance;

            //print("updating scale: newdistance is " + newDistance + ", startdistance is " + CurrentPinch.StartDistance + " and scale is " + newScale);
            CurrentPinch.Scale = newScale;

            OnPinchUpdate?.Invoke(CurrentPinch);
        }
    }

    KeyMessage[] BytesToKeyMessages(byte[] bytes)
    {
        List<KeyMessage> result = new List<KeyMessage>();
        int readIndex = 0;

        while (readIndex < bytes.Length)
        {
            byte[] buffer = new byte[bytesPerBlockKey];

            //checking, if we can read another <bytesPerBlock> bytes, to create a Touch struct
            if ((bytes.Length - readIndex) < bytesPerBlockKey)
            {
                Debug.Log("Could not read " + bytesPerBlockKey + " bytes of block. Only got " + (bytes.Length - readIndex) + " bytes.");
                break;
            }

            Array.Copy(bytes, readIndex, buffer, 0, bytesPerBlockKey);
            readIndex += bytesPerBlockKey;

            int pos = 0;
            int key = BitConverter.ToInt32(buffer, pos); pos += 4;
            bool pressed = BitConverter.ToBoolean(buffer, pos); pos += sizeof(bool);
            bool isrepeat = BitConverter.ToBoolean(buffer, pos); pos += sizeof(bool);

            //print("received touch: " + width + " " + height + " " + dPosX + " " +dPosY + " " + ((TouchPhase)phase) + " " + posX + " " +posY + " " +tapCount);
            KeyMessage keyMessage = new KeyMessage
            {
                keyCode = (KeyCode)key,
                pressed = pressed,
                isRepeat = isrepeat
            };


            result.Add(keyMessage);
        }


        return result.ToArray();

    }

    public void OnKeyMessageReceived(byte[] bytes)
    {
        var keys = BytesToKeyMessages(bytes);

        for (int i = 0; i < keys.Length; ++i)
        {
            OnKey.Invoke(keys[i]);
        }
    }

    public bool printTouches = false;
    public void OnStylusMessageReceived(byte[] bytes)
    {
        var events = BytesToStylusEvents(bytes);

        foreach(var ev in events)
        {
            ev.normalizedPosition = Vector2.Scale(ev.normalizedPosition, StylusTouchInputScale);
            ev.normalizedPosition += StylusTouchInputOffset;
            switch (ev.type)
            {
                case StylusEventType.TOUCH_DOWN:
                    OnStylusTouchDown(ev);
                    break;
                case StylusEventType.TOUCH_UP:
                    OnStylusTouchUp(ev);
                    break;
                case StylusEventType.BUTTON_DOWN:
                    OnStylusButtonDown(ev);
                    break;
                case StylusEventType.BUTTON_UP:
                    OnStylusButtonUp(ev);
                    break;
                case StylusEventType.MOVE:
                    OnStylusMove(ev);
                    break;
            }
        }
    }

    public void OnTouchMessageReceived(byte[] bytes)
    {
        NormalizedTouch[] touches = new NormalizedTouch[0];
       
        touches = BytesToTouches(bytes);
        //Debug.Log("Received " + bytes.Length + " bytes: got " + touches.Length + " Touch struct(s) out of it");

        //sending data to all subscribers
        if (onRemoteTouches != null)
            onRemoteTouches(touches);

        if (touches.Length <= 1)
        {
            if (CurrentPinch != null)
            {
                OnPinchEnd?.Invoke(CurrentPinch);
                CurrentPinch = null;
            }
        }
        else if (touches.Length >= 2)
        {
            HandlePinch(touches);
        }
        
    }

	void Update()
	{
        foreach(var rec in receivers)
        {
            rec.Update();
        }
    }

	void OnApplicationQuit()
	{
		foreach(var rec in receivers)
        {
            rec.Shutdown();
        }
	}

    private List<NormalizedTouch> lastTouches;

    public NormalizedStylusEvent[] BytesToStylusEvents(byte[] bytes)
    {
        List<NormalizedStylusEvent> stylusEvents = new List<NormalizedStylusEvent>();
        int readIndex = 0;

        while(readIndex < bytes.Length)
        {
            byte[] buffer = new byte[bytesPerBlockStylus];

            //checking, if we can read another <bytesPerBlock> bytes, to create a Touch struct
            if ((bytes.Length - readIndex) < bytesPerBlockStylus)
            {
                Debug.Log("Could not read " + bytesPerBlockStylus + " bytes of block. Only got " + (bytes.Length - readIndex) + " bytes.");
                break;
            }

            Array.Copy(bytes, readIndex, buffer, 0, bytesPerBlockStylus);
            readIndex += bytesPerBlockStylus;

            int pos = 0;
            byte type = buffer[pos]; pos++;
            float posX = BitConverter.ToSingle(buffer, pos); pos += 4;
            float posY = BitConverter.ToSingle(buffer, pos); pos += 4;
            float width = BitConverter.ToSingle(buffer, pos); pos += 4;
            float height = BitConverter.ToSingle(buffer, pos); pos += 4;

            //print("received touch: " + width + " " + height + " " + dPosX + " " +dPosY + " " + ((TouchPhase)phase) + " " + posX + " " +posY + " " +tapCount);
            NormalizedStylusEvent stylusEvent = new NormalizedStylusEvent
            {
                normalizedPosition = new Vector2((float)posX / width,1.0f- (float)posY / height),
                type = (StylusEventType) type
            };


            stylusEvents.Add(stylusEvent);
        }

        
        return stylusEvents.ToArray();
    }

    //recreating the touches from the raw bytes
    NormalizedTouch[] BytesToTouches(byte[] bytes)
	{
		List<NormalizedTouch> touches = new List<NormalizedTouch>(this.maxTouches);
		int readIndex = 0;
		
		for(int i=0; i<this.maxTouches; i++)
		{
			byte[] buffer = new byte[bytesPerBlockTouch];

			if( bytes.Length == readIndex )	//we're finished
				break;

			//checking, if we can read another <bytesPerBlock> bytes, to create a Touch struct
			if( (bytes.Length - readIndex) < bytesPerBlockTouch )
			{
				Debug.Log("Could not read " + bytesPerBlockTouch + " bytes of block. Only got " + (bytes.Length - readIndex) + " bytes.");
				break;
			}

			Array.Copy(bytes, readIndex, buffer, 0, bytesPerBlockTouch);
			readIndex += bytesPerBlockTouch;

            int pos = 0;
            float width = BitConverter.ToSingle(buffer, pos); pos += 4;
            float height = BitConverter.ToSingle(buffer, pos); pos += 4;
            byte phase = buffer[pos]; pos += 1;
            int tapCount = buffer[pos]; pos += 1;
            float posX = BitConverter.ToSingle(buffer, pos); pos += 4;
            float posY = BitConverter.ToSingle(buffer, pos); pos += 4;
            

			//print("received touch: " + width + " " + height + " " + dPosX + " " +dPosY + " " + ((TouchPhase)phase) + " " + posX + " " +posY + " " +tapCount);
            NormalizedTouch touch = new NormalizedTouch
            {
                normalizedPosition = new Vector2((float)posX / width, (float)posY / height),
                phase = (TouchPhase)phase
            };

           
			touches.Add(touch);
		}

        if(lastTouches != null) foreach(var touch in touches)
        {
                if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Ended)
                    continue;
            foreach(var lastTouch in lastTouches)
            {
                if(lastTouch.normalizedPosition.x == touch.normalizedPosition.x && lastTouch.normalizedPosition.y == touch.normalizedPosition.y)
                {
                    touch.phase = TouchPhase.Stationary;
                    break;
                }
            }
        }

        if(printTouches) foreach(var touch in touches)
        {
            print("received touch: " + touch.normalizedPosition + " " + touch.phase);
        }

        lastTouches = touches;
		return touches.ToArray();
	}

	void printRelevantTouchInfo(Touch[] touches)
	{
		foreach(Touch t in touches)
			printRelevantTouchInfo(t);
	}

	void printRelevantTouchInfo(Touch t)
	{
		Debug.Log("deltaPosition: " + t.deltaPosition);
		Debug.Log("phase: " + t.phase);
		Debug.Log("position: " + t.position);
		Debug.Log("tapCount: " + t.tapCount);
	}
}
