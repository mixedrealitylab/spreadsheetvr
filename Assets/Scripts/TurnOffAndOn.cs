﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffAndOn : MonoBehaviour
{
    public GameObject[] offAndOners;

    public bool turnedOn = false;
    // Start is called before the first frame update
    void Start()
    {
        foreach(var obj in offAndOners)
        {
            obj.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!turnedOn)
        {
            turnedOn = true;
            foreach(var obj in offAndOners)
            {
                obj.SetActive(true);
            }
        }
    }
}
