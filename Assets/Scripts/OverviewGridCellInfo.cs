﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class OverviewGridCellInfo : GridCellInfo
{
    public static OverviewGridCellInfo LastEnteredOverviewCell;
    public bool TriggerScroll = false;
    public GameObject  LocationIndicator;

    public override void OnInteract(Collider other)
    {
        if (other.gameObject)
        {
            var pen = other.GetComponent<PenTip>();
            if (pen)
            {
                sheetsInteraction.ScrollToCell(ColumnIndex,RowIndex);
                MoveIndicatorToMyPosition();
            }
        }
    }

    private void MoveIndicatorToMyPosition()
    {
        var myScale = this.transform.lossyScale;
        var indicatorScale = LocationIndicator.transform.lossyScale;

        LocationIndicator.transform.position = this.transform.position - this.transform.right * myScale.x / 2.0f + this.transform.up * myScale.y / 2.0f + LocationIndicator.transform.right * indicatorScale.x / 2.0f - LocationIndicator.transform.up * indicatorScale.y / 2.0f;
    }

    private new void Update()
    {
        base.Update();
        if(TriggerScroll)
        {
            TriggerScroll = false;
            sheetsInteraction.ScrollToCell(ColumnIndex, RowIndex);
            print("current cell scroll: " + sheetsInteraction.CurrentCellScrollX + " " + sheetsInteraction.CurrentCellScrollY);

            
            MoveIndicatorToMyPosition();
        }
    }

    public new void OnTriggerEnter(Collider other)
    {
        LastEnteredOverviewCell = this;
        base.OnTriggerEnter(other);
    }

    public new void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);
    }
}

