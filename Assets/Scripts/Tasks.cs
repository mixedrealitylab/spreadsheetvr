﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

    [System.Serializable]
    public class TasksFile
    {
        public CreateTask[] create;
        public AddTask[] add;
        public RemoveTask[] remove;
        public EditTask[] edit;
    }

    [System.Serializable]
    public class CreateTask
    {
        public string target;
        public string sourceSheet;
        public string targetSheet;
        public string function;
        public string[] sourceCells;
        public string text;
        public string result;
    }

    [System.Serializable]
    public class AddTask
    {
        public string target;
        public string sourceSheet;
        public string targetSheet;
        public string function;
        public string[] originalCells;
        public string[] finalCells;
        public string text;
        public string result;
    }

    [System.Serializable]
    public class RemoveTask
    {
        public string target;
        public string sourceSheet;
        public string targetSheet;
        public string function;
        public string[] originalCells;
        public string[] finalCells;
        public string text;
        public string result;
    }

    [System.Serializable]
    public class EditTask
    {
        public string target;
        public string sourceSheet;
        public string targetSheet;
        public string function;
        public string[] originalCells;
        public string[] finalCells;
        public string text;
        public string result;
    }
