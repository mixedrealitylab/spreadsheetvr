﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class CalibratePen : MonoBehaviour
{
    public InputField filenameInput;
    public Button startStopButton;

    public TextMeshProUGUI debugOutput;
    public Transform PenTransform;
    public TextMeshProUGUI samplesText;
    public GameObject penTip;

    private class CalibrationDataPoint
    {
        public Vector3 Pm;
        public float[] Rotation = new float[9];
    }

    private List<CalibrationDataPoint> dataPoints;
    // Start is called before the first frame update
    void Start()
    {
        filenameInput.text = "matrix_out.mat";
        dataPoints = new List<CalibrationDataPoint>();
        startStopButton.onClick.AddListener(() =>
        {
            if(!Started)
            {
                dataPoints = new List<CalibrationDataPoint>();
                Started = true;
                startStopButton.transform.GetChild(0).GetComponent<Text>().text = "Stop and save";
                timer.Stop();
                timer.Reset();
                timer.Start();
            }
            else
            {
                Started = false;
                WriteToFile(filenameInput.text, dataPoints);
                startStopButton.transform.GetChild(0).GetComponent<Text>().text = "Start";
            }
        });
    }

    public void SaveStuff()
    {
        WriteToFile(filenameInput.text, dataPoints);
    }

    private void WriteToFile(string filename, List<CalibrationDataPoint> data)
    {
        string result = "# name: A\n";
        {
            result += "# type: matrix\n";

            int numColumns = 6;
            int numRows = 3 * data.Count;
            result += "# rows: " + numRows + "\n";
            result += "# columns: " + numColumns + "\n";
        }
        var cult = new CultureInfo("en-US");

        CultureInfo.CurrentCulture = cult;

        for (int i=0;i<data.Count;++i)
        {
            var dataPoint = data[i];
            if (i == data.Count - 1)
            {
                result += "1 0 0 " + dataPoint.Rotation[0] + " " + dataPoint.Rotation[1] + " " + dataPoint.Rotation[2] + " ";
                result += "0 1 0 " + dataPoint.Rotation[3] + " " + dataPoint.Rotation[4] + " " + dataPoint.Rotation[5] + " ";
                result += "0 0 1 " + dataPoint.Rotation[6] + " " + dataPoint.Rotation[7] + " " + dataPoint.Rotation[8] + "\n";
            }
            else
            {
                result += "1 0 0 " + dataPoint.Rotation[0] + " " + dataPoint.Rotation[1] + " " + dataPoint.Rotation[2] + " ";
                result += "0 1 0 " + dataPoint.Rotation[3] + " " + dataPoint.Rotation[4] + " " + dataPoint.Rotation[5] + " ";
                result += "0 0 1 " + dataPoint.Rotation[6] + " " + dataPoint.Rotation[7] + " " + dataPoint.Rotation[8] + " ";
            }
        }
        result += "\n\n";

        {
            result += "# name: y\n";
            result += "# type: matrix\n";

            int numColumns = 1;
            int numRows = 3 * data.Count;
            result += "# rows: " + numRows + "\n";
            result += "# columns: " + numColumns + "\n";
        }

        for (int i = 0; i < data.Count; ++i)
        {
            var dataPoint = data[i];

            if(i == data.Count-1)
            {
                result += dataPoint.Pm.x + " " + dataPoint.Pm.y + " " + dataPoint.Pm.z + "\n";
            }
            else
            {
                result += dataPoint.Pm.x + " " + dataPoint.Pm.y + " " + dataPoint.Pm.z + " ";
            }
            
        }
        result += "\n\n\n";

        File.WriteAllText(filename, result);  
    }

    private CalibrationDataPoint Measure(Transform t)
    {
        Matrix4x4 rot = Matrix4x4.TRS(Vector3.zero, t.rotation, Vector3.one);

        var result = new CalibrationDataPoint
        {
            Pm = t.position
        };

        //result.Rotation[0] = rot[0]; result.Rotation[1] = rot[1]; result.Rotation[2] = rot[2];
        //result.Rotation[3] = rot[4]; result.Rotation[4] = rot[5]; result.Rotation[5] = rot[6];
        //result.Rotation[6] = rot[8]; result.Rotation[7] = rot[9]; result.Rotation[8] = rot[10];

        result.Rotation[0] = rot.m00; result.Rotation[1] = rot.m01; result.Rotation[2] = rot.m02;
        result.Rotation[3] = rot.m10; result.Rotation[4] = rot.m11; result.Rotation[5] = rot.m12;
        result.Rotation[6] = rot.m20; result.Rotation[7] = rot.m21; result.Rotation[8] = rot.m22;

        for (int i=0;i<9;++i)
        {
            result.Rotation[i] *= -1;
        }
        
        return result;
    }

    public void Calculate()
    {

    }

    public void Sample()
    {
        dataPoints.Add(Measure(PenTransform));
    }

    public bool Started = false;
    public float SampleInterval = 0.1f;

    private Stopwatch timer = new Stopwatch();
    public int SampleCount = 0;
    // Update is called once per frame
    void Update()
    {
        //if(dataPoints != null) SampleCount = dataPoints.Count;

        //samplesText.text = "samples collected: " + SampleCount;
        //if(Started && timer.Elapsed.TotalSeconds >= SampleInterval)
        //{
        //    timer.Stop();
        //    timer.Reset();
        //    timer.Start();

        //    dataPoints.Add(Measure(PenTransform));
        //}
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(CalibratePen))]
public class CalibratePenEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var target = this.target as CalibratePen;
        if(GUILayout.Button("sample"))
        {
            target.Sample();
        }

        if(GUILayout.Button("Save results"))
        {
            target.SaveStuff();
        }
    }
}
#endif
