﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelationHandle : PenAttachable
{
    public RelationInfo Parent;

    //Start is called before the first frame update
    public new void Start()
    {
        base.Start();
    }

    public override void OnDelete(PenTip penTip)
    {
        base.OnDelete(penTip);

        if(Parent.From != null && Parent.To != null)
        {
            var relationships = Parent.From.Relationship;
            if(relationships != null)
            {
                relationships.RemoveRelationship(Parent.To);
                relationships.OnGridCellInteraction(Parent.From);
                penTip.DetachAttachedObject();
                GameObject.Destroy(Parent.gameObject);
            }
        }
    }

    public void AddSelectedCellRelations(GridCellInfo interactedCell)
    {
        var selected = interactedCell.sheetsInteraction.selectedCellTracker.SelectedCells;

        if(interactedCell.sheetsInteraction == ActiveSheetSwitcher.Instance.activeBrowser)
        {
            foreach(var cell in selected)
            {
                Parent.From.Relationship.AddRelationship(cell);
            }
        }
         else
        {
            Parent.From.Relationship.AddRelationship(interactedCell);
        }
    }

    public override void OnGridCellInteraction(GridCellInfo cell, PenTip pen)
    {
        print("RelationHandle OnGridCellInteraction");
        base.OnGridCellInteraction(cell, pen);

        this.transform.position = cell.transform.position - cell.transform.forward * transform.lossyScale.z/2.0f;
        var vis = Parent.From.Relationship.RemoveRelationship(Parent.To);
        Object.Destroy(vis.gameObject);

        AddSelectedCellRelations(cell);

        Parent.To = cell;
        Parent.From.Relationship.OnGridCellInteraction(cell);

    }

    // Update is called once per frame
    public new void Update()
    {
        base.Update();

        if(IsSelected)
        {

        }
    }
}
