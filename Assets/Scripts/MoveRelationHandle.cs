﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the new relation handle works like the basic one (creates a relationship between the target and source cell when it interacts with a grid cell)
//but it also creates a new copy of itself in the old location. this is used for adding new relationships to existing functions, etc.
public class MoveRelationHandle : RelationHandle
{
    public override void OnDelete(PenTip penTip)
    {
        if(Parent.From != null)
        {
            if(Parent.From.Relationship != null)
            {
                Parent.From.Relationship.Destroy();
                Parent.From.Relationship = null;
                Parent.From.sheetsInteraction.ClearField(Parent.From);
            }
        }
    }

    public override void OnGridCellInteraction(GridCellInfo cell, PenTip pen)
    {
        if (AudioSource != null && GridCellInteractionSoundEffect != null)
        {
            AudioSource.PlayClipAtPoint(GridCellInteractionSoundEffect, transform.position);
        }

        var oldCell = Parent.From;
        oldCell.sheetsInteraction.ClearField(oldCell);

        //transfer relationship to new cell
        var newCell = cell;
        var relationShip = oldCell.Relationship;

        if(newCell.Relationship != null)
        {
            newCell.Relationship.Destroy();
        }

        newCell.Relationship = oldCell.Relationship;
        oldCell.Relationship = null;

        foreach(var related in relationShip.Visualizations)
        {
            related.From = newCell;
        }

        Parent.From = newCell;
        relationShip.TargetCell = newCell;
        relationShip.OnGridCellInteraction(newCell);

        //move handles over
        if (relationShip.NewRelationHandleInfo != null)
        {
            relationShip.NewRelationHandleInfo.transform.SetParent(newCell.transform, false);
            relationShip.NewRelationHandleInfo.Handle.GetComponent<PenAttachable>().RememberParent();
            relationShip.NewRelationHandleInfo.From = newCell;
        }

        if(relationShip.RelationHandlesToggle != null)
        {
            relationShip.RelationHandlesToggle.transform.parent.SetParent(newCell.transform, false);
        }

        Parent.transform.SetParent(newCell.transform, false);
        RememberParent();

        //also move all the existing relation handles
        foreach (var handle in relationShip.Visualizations)
        {
            var oldpos = handle.Handle.transform.position;
            handle.transform.SetParent(newCell.transform, false);
            handle.Handle.GetComponent<PenAttachable>().RememberParent();
            handle.Handle.transform.position = oldpos;
        }

        ////and reset the NewRelationHandle back to the Parent.From cell       
        //Parent.Handle.transform.position = Parent.transform.position - Parent.transform.forward * Parent.Handle.transform.lossyScale.z / 2.0f;
        //Parent.Handle.transform.localRotation = Quaternion.identity;
    }
}
