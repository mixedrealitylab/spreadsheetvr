﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the new relation handle works like the basic one (creates a relationship between the target and source cell when it interacts with a grid cell)
//but it also creates a new copy of itself in the old location. this is used for adding new relationships to existing functions, etc.
public class NewRelationHandle : RelationHandle
{
    public override void OnGridCellInteraction(GridCellInfo cell, PenTip pen)
    {
        if (AudioSource != null && GridCellInteractionSoundEffect != null)
        {
            AudioSource.PlayClipAtPoint(GridCellInteractionSoundEffect, transform.position);
        }


        //add a new relationship in the place of this NewRelationHandle
        AddSelectedCellRelations(cell);


        Parent.From.Relationship.OnGridCellInteraction(cell);

        //and reset the NewRelationHandle back to the Parent.From cell       
        Parent.Handle.transform.position = Parent.transform.position - Parent.transform.forward * Parent.Handle.transform.lossyScale.z/2.0f;
        Parent.Handle.transform.position -= (cell.transform.right * Parent.Handle.transform.lossyScale.x ) * 0.6f;
        Parent.Handle.transform.localRotation = Quaternion.identity;
    }
}
