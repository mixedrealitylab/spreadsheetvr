﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class GazeConfirm : PenHoverAndClickObject
{
    public GazeManager GazeManager;
    public ActiveSheetSwitcher switcher;

    public GoogleSheetsInteraction GazedAtStart;
    public GoogleSheetsInteraction GazedAtEnd;

    public bool DontGazeActiveSheet = true;

    private void Start()
    {
        base.Start();

        Communication.OnKey += Communication_OnKey;
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        if(NetworkStylusInteraction.LeftPressed(obj))
        {
            OnPenEntered(null);
            OnPenExit(null);
        }
    }

    public override void OnInteraction(PenTip pentip)
    {
        
    }

    public override void OnPenEntered(PenTip other)
    {
        GazedAtEnd = null;
        GazedAtStart = GazeManager.CurrentGazedSheet;
    }

    public override void OnPenExit(PenTip other)
    {
        GazedAtEnd = GazeManager.CurrentGazedSheet;

        if (GazedAtEnd != null )//&& (GazedAtEnd == GazedAtStart || GazedAtEnd == null))
        {
            switcher.ActivateSheet(GazedAtEnd);
        }
      
    }
}

