﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

public class JSTest : MonoBehaviour
{
    public ZenFulcrum.EmbeddedBrowser.Browser Browser;

    public string function = "SUM";
    public string targetField = "I2";
    public string range = "G2";

    public bool callFunction = false;
    // Start is called before the first frame update
    void Start()
    {
       
        
    }

    // Update is called once per frame
    void Update()
    {
        if(callFunction)
        {
            callFunction = false;

            JSONNode argFunc = new JSONNode(function);
            JSONNode argTargetField = new JSONNode(targetField);
            JSONNode argRange = new JSONNode(range);

            Browser.CallFunction("setFunction", argFunc, argTargetField, argRange);
        }
    }
}
