﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

public class GridCellInfo : MonoBehaviour
{
    public static GridCellInfo LastSelectedGridCell;

    public string Content = "";

    public bool RealGridCell = false; //the prototype dummy will have this for example. used to skip some processing.

    public GridCellInteractionManager InteractionManager;
    public GameObject SelectionIndicator;
    public GameObject ActivationIndicator;

    public bool SelectedInSheet = false;
    public string CellName;
    public GoogleSheetsInteraction sheetsInteraction;
    public int RowIndex = -1;
    public int ColumnIndex = -1;
    public string ColumnName;
    public string RowName;
    
    public bool UseKeyInteraction = true;
    public string Sheet = "";
    public int SheetID = 0; 

    [Header("Relationship Debug")]
    public GridCellInfo[] relatedCellDebug;
    public bool relateCells = false;

    public SheetRelationship Relationship;

    [Header("Visuals")]
    public bool IsSelected = false;
    public bool VisualizeSelection = false;
    public bool ForceAlphaMask = true;
    public bool DisableAlphaMasking = true;

    private Collider EnteredCollider = null;

    [Header("Debug")]
    public bool ClearField = false;

    //Start is called before the first frame update
    void Start()
    {
        if (InteractionManager == null) InteractionManager = GetComponentInParent<GridCellInteractionManager>();
        
    }


    [ContextMenu("Show Relationships")]
    void ShowRelationships()
    {
        Relationship.ShowRelationships();
    }

    [ContextMenu("Hide Relationships")]
    void HideRelationships()
    {
        Relationship.HideRelationships();
    }

    //use this and VisualizeDeselect if you only want to trigger the visualization without actually selecting the grid cell
    public void VisualizeSelect()
    {
        VisualizeSelection = true;
    }

    public void VisualizeDeselect()
    {
        VisualizeSelection = false;
    }

    public void Select(Collider entered)
    {
        IsSelected = true;
        if(InteractionManager) InteractionManager.OnSelect(this, entered);
        LastSelectedGridCell = this;
        VisualizeSelection = true;
    }

    public void Deselect(Collider exited)
    {
        IsSelected = false;
        if (InteractionManager) InteractionManager.OnDeselect(this, exited);
        VisualizeSelection = false;
    }

    private void OnEnable()
    {
       
    }

    private void OnDisable()
    {
        if(IsSelected) Deselect(EnteredCollider);
    }

    public string GetCellName()
    {
        return Sheet + "!" + CellName;
    }

    public void ClearContent()
    {
        Content = "";
    }

    public void SetContent(string newContent)
    {
        Content = newContent;
    }

    //Update is called once per frame
    public void Update()
    {
        if(!DisableAlphaMasking &&( Content == "" || ForceAlphaMask))
        {
            this.tag = "alpha_mask_object";
        }
        else
        {
            this.tag = "Untagged";
        }

        if(ClearField)
        {
            ClearField = false;
            sheetsInteraction.ClearField(this);
        }
        if(relateCells)
        {
            relateCells = false;
            Relationship = new SheetRelationShipBasicFunc("SUM", this, sheetsInteraction.RelationPrefab, null, null);

            foreach(var cell in relatedCellDebug)
            {
                Relationship.AddRelationship(cell);
            }

            Relationship.OnGridCellInteraction(this);
        }

        //some instances of this component will not have these references set because they dont need activation/selection visuals. stop update here in that case.
        if (SelectionIndicator == null || ActivationIndicator == null)
            return;

        if(SelectedInSheet)
        {
            //SelectionIndicator.SetActive(false);
            ActivationIndicator.SetActive(true);
        }
        else
        {
            ActivationIndicator.SetActive(false);
            
        }

        if (VisualizeSelection)
        {
            SelectionIndicator.SetActive(true);
        }
        else
        {
            SelectionIndicator.SetActive(false);
        }
    }

    [ContextMenu("simulate click")]
    public void ClickOnCell() //simulates a click on the cells location for interaction purposes
    {
        var touchPos = GoogleSheetsInteraction.WorldPosToTouchCoord(this.transform.position, sheetsInteraction.transform);
        var id = sheetsInteraction.browser.ID;

        BrowserNative.zfb_mouseMove(id, touchPos.x, touchPos.y);
        BrowserNative.zfb_mouseButton(id, BrowserNative.MouseButton.MBT_LEFT, true, 1);
        BrowserNative.zfb_mouseButton(id, BrowserNative.MouseButton.MBT_LEFT, false, 1);
    }

    public virtual void OnInteract(Collider other)
    {
        if (other.gameObject)
        {
            var pen = other.GetComponent<PenTip>();
            if (pen)
            {
                print("entering");
                if (pen.AttachedFunc)
                {
                    print("attaching");
                    var attached = pen.AttachedFunc;
                    attached.DetachFromGameObject();
                    pen.OnDetach();
                    attached.OnGridCellInteraction(this, pen);
                }
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (!sheetsInteraction || !sheetsInteraction.MaskedInputAreas) return;
        if (sheetsInteraction.MaskedInputAreas.CollidesWithMasks(other.transform.position)) return;

        EnteredCollider = other;
        //print("entered grid cell");
        Select(other);
        if (!UseKeyInteraction && InteractionManager) InteractionManager.OnInteract(this, other);
    }

    public void OnTriggerExit(Collider other)
    {
        Deselect(other);
    }

    public void OnTriggerStay(Collider other)
    {
        
    }
}

