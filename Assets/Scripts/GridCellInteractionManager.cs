﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//at times, multiple grid cells can be selected/activated at the same time, and we have to decide which one to interact with
//we do this here
public class GridCellInteractionManager : MonoBehaviour
{
    public bool InteractionQueued = false;
    public int EnteredCellCount = 0;
    private List<GridCellInfo> EnteredCells = new List<GridCellInfo>();
    private Collider interactionCollider;

    public void OnSelect(GridCellInfo info, Collider other)
    {
        interactionCollider = other;
        if(EnteredCells.IndexOf(info) < 0)
            EnteredCells.Add(info);
    }

    public void OnDeselect(GridCellInfo info, Collider other)
    {
        interactionCollider = other;
        var idx = EnteredCells.IndexOf(info);
        if (idx >= 0)
            EnteredCells.RemoveAt(idx);
    }

    public void OnInteract(GridCellInfo info, Collider other)
    {
        InteractionQueued = true;
        interactionCollider = other;
    }

    public void HandleInteraction()
    {
        if (EnteredCells.Count <= 0 || interactionCollider == null) return;

        GridCellInfo minDistanceCell = null;
        float minDist = float.MaxValue;
        foreach(var entered in EnteredCells)
        {
            var dist = (entered.transform.position - interactionCollider.transform.position).magnitude;
            if(dist<minDist)
            {
                minDist = dist;
                minDistanceCell = entered;
            }
        }

        if(minDistanceCell != null)
        {
            minDistanceCell.OnInteract(interactionCollider);
        }

        interactionCollider = null;
    }

    // Start is called before the first frame update
    void Start()
    {
        Communication.OnKey += Communication_OnKey;
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        if (NetworkStylusInteraction.FirstButtonPressed(obj) && obj.pressed == false) //key.pause is the key sent by the first button on the AZLink key
        {
            InteractionQueued = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        EnteredCellCount = EnteredCells.Count;
        if (InteractionQueued)
        {
            InteractionQueued = false;
            HandleInteraction();
        }
    }
}
