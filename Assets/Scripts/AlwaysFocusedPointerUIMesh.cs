﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class AlwaysFocusedPointerUIMesh : ZenFulcrum.EmbeddedBrowser.PointerUIMesh
{
    public bool KeyboardFocused = false;
    public bool MouseFocused = false;

    public override void InputUpdate()
    {
        KeyboardHasFocus = KeyboardFocused;
        MouseHasFocus = MouseFocused;
        base.InputUpdate();

        KeyboardHasFocus = KeyboardFocused;
        MouseHasFocus = MouseFocused;
    }
}

