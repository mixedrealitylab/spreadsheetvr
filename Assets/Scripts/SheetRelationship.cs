﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SheetRelationship
{
    public GridCellInfo TargetCell;
    public List<GridCellInfo> RelatedCells;
    public List<RelationInfo> Visualizations;
    public RelationInfo NewRelationHandleInfo;
    public RelationInfo MoveRelationHandleInfo;
    public GenericGameObjectToggle RelationHandlesToggle;

    private GameObject RelationPrefab;

    public SheetRelationship(GridCellInfo targetCell, GameObject RelationPrefab, RelationInfo NewRelationHandleInfo, RelationInfo MoveRelationInfo)
    {
        this.RelationPrefab = RelationPrefab;
        this.TargetCell = targetCell;
        this.NewRelationHandleInfo = NewRelationHandleInfo;
        this.MoveRelationHandleInfo = MoveRelationInfo;

        RelationHandlesToggle = TargetCell.sheetsInteraction.MakeRelationHandleToggleObject(targetCell);
        RelationHandlesToggle.AddGameObjectToToggle(this.NewRelationHandleInfo.gameObject);
        RelationHandlesToggle.AddGameObjectToToggle(this.MoveRelationHandleInfo.gameObject);

        RelatedCells = new List<GridCellInfo>();
        Visualizations = new List<RelationInfo>();
    }

    public abstract void OnGridCellInteraction(GridCellInfo cell);

    public void RemoveAllRelationships(bool deleteVis = false)
    {
        for (int i = RelatedCells.Count - 1; i >= 0; --i)
        {
            var result = Visualizations[i];

            RelatedCells.RemoveAt(i);

            Visualizations.RemoveAt(i);
            if (deleteVis)
            {
                GameObject.Destroy(result.gameObject);
            }
        }
    }

    public void AddRelationships(GridCellInfo[] infos)
    {
        foreach (var info in infos)
        {
            AddRelationship(info);
        }
    }

    public void AddRelationship(GridCellInfo info, RelationInfo visualizer = null)
    {
        if (visualizer == null)
        {
            visualizer = info.sheetsInteraction.MakeRelationObject(TargetCell, info);
        }

        RelatedCells.Add(info);
        Visualizations.Add(visualizer);
        RelationHandlesToggle.AddGameObjectToToggle(visualizer.gameObject);
    }

    public RelationInfo RemoveRelationship(GridCellInfo info, bool deleteVis = false)
    {
        for (int i = 0; i < RelatedCells.Count; ++i)
        {
            if (RelatedCells[i] == info)
            {
                var result = Visualizations[i];

                RelatedCells.RemoveAt(i);
               
                Visualizations.RemoveAt(i);
                RelationHandlesToggle.RemoveGameObjectToToggle(result.gameObject);
                if(deleteVis)
                {
                    GameObject.Destroy(result);
                }

                return result;
            }
        }

        return null;
    }

    public void Destroy()
    {
        RelatedCells.Clear();
        foreach(var vis in Visualizations)
        {
            GameObject.Destroy(vis.gameObject);
        }

        if(NewRelationHandleInfo != null)
        {
            GameObject.Destroy(NewRelationHandleInfo.gameObject);
        }

        if(MoveRelationHandleInfo != null)
        {
            GameObject.Destroy(MoveRelationHandleInfo.gameObject);
        }

        if(RelationHandlesToggle != null)
        {
            GameObject.Destroy(RelationHandlesToggle.gameObject);
        }
    }

    

    public void ShowRelationships()
    {
        foreach (var vis in Visualizations)
        {
            vis.gameObject.SetActive(true);
        }
    }

    public void HideRelationships()
    {
        foreach (var vis in Visualizations)
        {
            vis.gameObject.SetActive(false);
        }
    }
}

public class SheetRelationShipBasicFunc : SheetRelationship
{
    public string FuncName;

    public SheetRelationShipBasicFunc(string funcName, GridCellInfo targetCell, GameObject RelationPrefab, RelationInfo newRelationHandleInfo, RelationInfo MoveRelationInfo) : base(targetCell, RelationPrefab, newRelationHandleInfo,MoveRelationInfo)
    {
        FuncName = funcName;
    }

    public override void OnGridCellInteraction(GridCellInfo cell)
    {
        cell.sheetsInteraction.ExecuteFunction(FuncName, TargetCell, RelatedCells.ToArray(), false);
    }
}