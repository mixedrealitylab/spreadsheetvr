﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfacePenInfo : MonoBehaviour
{
    public Transform tooltip;
    public Transform tip;
    public Transform back;
    public Transform stift;
    public Transform PenParent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [ContextMenu("setup geometry")]
    public void SetupGeometry()
    {
        var penAxis = (tooltip.transform.position - PenParent.transform.position).normalized;

        var stiftAxis = stift.transform.up;

        //var rotateTowards = Quaternion.FromToRotation(stiftAxis, penAxis);

        //stift.transform.rotation =  Quaternion.Inverse(rotateTowards * stift.transform.rotation);

        stift.transform.up = penAxis;

        var offset = tooltip.transform.position - tip.transform.position;
        stift.transform.position += offset;
    }
}
