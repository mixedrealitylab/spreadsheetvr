﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct TrackedFinger : IComparable<TrackedFinger>
{
    public Vector3 position;
    public float cos_alpha; //needed for identifying fingers

    public int CompareTo(TrackedFinger other)
    {
        if(this.cos_alpha < other.cos_alpha)
        {
            return -1;
        }
        else if(this.cos_alpha > other.cos_alpha)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}


public class FingerTracker : MonoBehaviour {

    public OptitrackStreamingClient StreamingClient;
    public List<TrackedFinger> Fingers = new List<TrackedFinger>();
    public Material fingerVisualizationMaterial;

    //for visualization
    public bool VisualizeFingers = true;
    private List<GameObject> visualizedFingers = new List<GameObject>();

    
    // Use this for initialization
    void Start () {

        // If the user didn't explicitly associate a client, find a suitable default.
        if (this.StreamingClient == null)
        {
            this.StreamingClient = OptitrackStreamingClient.FindDefaultClient();

            // If we still couldn't find one, disable this component.
            if (this.StreamingClient == null)
            {
                Debug.LogError(GetType().FullName + ": Streaming client not set, and no " + typeof(OptitrackStreamingClient).FullName + " components found in scene; disabling this component.", this);
                this.enabled = false;
                return;
            }
        }
    }
	
    private GameObject CreateFingerVisualizationObject()
    {
        var result = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        result.transform.localScale = new Vector3(0.005f,0.005f,0.005f);
        result.GetComponent<Renderer>().material = fingerVisualizationMaterial;
        return result;
    }

    //checks if a point is inside an oriented bounding box
    public bool PointInsideOBB(Vector3 p, Vector3 obbCenter, Vector3 halfSizes, Vector3 x, Vector3 y, Vector3 z)
    {
        //idea: project the vector from p to obbCenter onto axes of the OBB. result has to be > -halfSizes[component] and < halfSizes[component] in each direction
        var pc = obbCenter - p;

        var dotx = Vector3.Dot(pc, x);
        if (dotx < -halfSizes.x || dotx > halfSizes.x)
            return false;

        var doty = Vector3.Dot(pc, y);
        if (doty < -halfSizes.y || doty > halfSizes.y)
            return false;

        var dotz = Vector3.Dot(pc, z);
        if (dotz < -halfSizes.z || dotz > halfSizes.z)
            return false;

        return true;
    }

    public float markerSize = 0.005f;
    // Update is called once per frame
    void Update ()
    {
        var freeMarkers = StreamingClient.UnassignedMarkers;

        Fingers.Clear();

        var validFingers = 0;
        try
        {
            for (var i = 0; i < freeMarkers.Count; ++i)
            {
                var finger = freeMarkers[i];

                var unityPosition = finger.Position;

                Fingers.Add(new TrackedFinger { position = unityPosition });
                validFingers++;
            }
        }
        catch (Exception e)
        {
            Debug.Log("Something went wrong: " + e + "\n" + e.StackTrace);
        }

        for (int i = 0; i < Fingers.Count; ++i)
        {
            var finger = Fingers[i];

            Fingers[i] = new TrackedFinger { position = finger.position, cos_alpha = finger.cos_alpha };
        }

        if (VisualizeFingers)
        {
            for(int i=0;i<Fingers.Count;++i)
            {
                if (visualizedFingers.Count < i +1)
                {
                     visualizedFingers.Add(CreateFingerVisualizationObject());
                }

                visualizedFingers[i].SetActive(true);
                visualizedFingers[i].transform.position = Fingers[i].position;
                visualizedFingers[i].transform.localScale = new Vector3(markerSize, markerSize, markerSize);
            }
        }

        

        for (var i = validFingers; i < visualizedFingers.Count; ++i)
        {
            visualizedFingers[i].SetActive(false);
        }
        
	}
}
