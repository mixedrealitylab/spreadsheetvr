﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class RegionCellInfo : MonoBehaviour
{
    public int TargetColumn;
    public int TargetRow;
    
    public GoogleSheetsInteraction sheetsInteraction;
    public GameObject SelectionIndicator;
    public GameObject ActivationIndicator;

    public bool IsActivated = false;
    public bool IsSelected = false;

    public bool UseKeyActivation = true;
    public bool UseTriggerEnterActivation = true;

    private void OnTriggerEnter(Collider other)
    {

        print("entered region cell!");
        if (other.gameObject)
        {
            var pen = other.GetComponent<PenTip>();
            if (pen)
            {
                IsSelected = true;
                if(UseTriggerEnterActivation)
                {
                    Activate();
                }

                print("moving to a new home!");
                sheetsInteraction.ScrollToCell(TargetColumn, TargetRow);
            }
        }
    }

    public void Activate()
    {
        IsActivated = true;

        var others = FindObjectsOfType<RegionCellInfo>();
        foreach(var other in others)
        {
            if(other != this)
                other.Deactivate();
        }
    }

    public void Deactivate()
    {
        IsActivated = false;
    }

    private void Update()
    {
        if (!IsActivated && IsSelected)
        {
            SelectionIndicator.SetActive(true);
        }
        else
        {
            SelectionIndicator.SetActive(false);
        }

        if (IsActivated)
        {
            ActivationIndicator.SetActive(true);
        }
        else
        {
            ActivationIndicator.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject)
        {
            var pen = other.GetComponent<PenTip>();
            if (pen)
            {
                IsSelected = false;
            }
        }
    }
}

