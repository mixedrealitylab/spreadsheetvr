﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

public class NetworkStylusInteraction : MonoBehaviour
{
    public Browser browser;

    public GameObject stylusIndicator;
    public GoogleSheetsInteraction sheetsInteraction;
    public PenTip penTip;

    public GameObject WheelBrowser;

    //if this is true, a touched location needs to move a certain minimum distance away from the original point to register as a move. if a new touch is not far enough away it will result in a touch to the same location as the previous one
    //this is used because it's very hard to do accurate double clicks using a pen (it will generally always move a few pixels between clicks), but many apps require a double click to happen on the same pixel without any movement whatsoever
    public bool UseTouchMoveThreshold = false;
    public float TouchMovementThresholdInPixels = 5.0f;

    //the zfulcrum browsers need to initialize so we allow them to load for a short time at start and turn them off later
    private bool WheelHiddenAtStart = false;
    private Stopwatch WheelHideTimer = new Stopwatch();
    private float HideDelay = 1.0f;
    public TrashCan trashCan;
    public bool ForwardStylusInput = false; //if this is set to true, things like touch down will be forwarded to the actual browser.

    // Start is called before the first frame update
    void Start()
    {
        if (browser == null)
        {
            browser = FindObjectOfType<Browser>();
        }
        penTip = AppConfig.Instance.ActivePen;
        if (trashCan == null) trashCan = FindObjectOfType<TrashCan>();

        WheelHideTimer.Start();

        Communication.OnStylusButtonDown += Communication_OnStylusButtonDown;
        Communication.OnStylusButtonUp += Communication_OnStylusButtonUp;
        Communication.OnStylusMove += Communication_OnStylusMove;
        Communication.OnStylusTouchDown += Communication_OnStylusTouchDown;
        Communication.OnStylusTouchUp += Communication_OnStylusTouchUp;

        Communication.OnKey += Communication_OnKey;

        stylusIndicator.SetActive(false);
    }

    public static bool FirstButtonPressed(KeyMessage obj)
    {
        return (obj.keyCode == (KeyCode)19 || obj.keyCode == (KeyCode)26);
    }

    public static bool SecondButtonPressed(KeyMessage obj)
    {
        return ((int)obj.keyCode == 20 || obj.keyCode == (KeyCode)24);
    }

    public static bool LeftPressed(KeyMessage obj)
    {
        return ((int)obj.keyCode == 23);
    }

    public static bool RightPressed(KeyMessage obj)
    {
        return ((int)obj.keyCode == 25);
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        print("key pressed: " + obj.keyCode + ", as int: " + (int)obj.keyCode + ", pressed: " + obj.pressed + ", repeat: " + obj.isRepeat);

        if(penTip.AttachedFunc == null)
        {
            if (FirstButtonPressed(obj) && obj.pressed)
            {
                ShowWheelBrowser();
            }
            else if(SecondButtonPressed(obj) && obj.pressed == false && WheelBrowser.activeSelf)
            {
                HideWheelBrowser();
            }
        }

    }

    private void UpdateStylusIndicator(NormalizedStylusEvent obj)
    {
        var oldz = stylusIndicator.transform.localPosition.z;
        stylusIndicator.transform.localPosition = new Vector3(obj.normalizedPosition.x - 0.5f, obj.normalizedPosition.y - 0.5f, oldz);
    }

    public void ShowWheelBrowser()
    {
        if (WheelBrowser.activeSelf || !WheelBrowser.GetComponent<WheelManager>().CanActivate()) return; //already active, no need to show it again

        var theWheel = WheelBrowser.GetComponent<WheelManager>();
        if (PenAttachable.LastTriggeredAttachable != null && PenAttachable.LastTriggeredAttachable.IsSelected) return;
        if (OverviewGridCellInfo.LastEnteredOverviewCell != null && OverviewGridCellInfo.LastEnteredOverviewCell.IsSelected) return;
        if (Scrollbox.LastEnteredScrollbox != null && Scrollbox.LastEnteredScrollbox.EnteredPenTip != null) return;
        if (PenHoverAndClickObject.LastHoveredPenHoverAndClickObject != null && PenHoverAndClickObject.LastHoveredPenHoverAndClickObject.IsHovered) return;

        if (trashCan && trashCan.IsHovered) return;
        if (CellCluster.LastHoveredCluster != null && CellCluster.LastHoveredCluster.IsHovered) return;

        WheelBrowser.SetActive(true);
        theWheel.ActivateWheel();
        WheelBrowser.transform.position = penTip.transform.position;
        WheelBrowser.transform.rotation = sheetsInteraction.transform.rotation;
    }

    public void HideWheelBrowser()
    {
        WheelBrowser.SetActive(false);
        WheelBrowser.GetComponent<WheelManager>().DeactivateWheel();
    }

    private void Communication_OnStylusTouchUp(NormalizedStylusEvent obj)
    {
        if(sheetsInteraction.selectedCellTracker.CanSelectCells() && !sheetsInteraction.MaskedInputAreas.CollidesWithMasksRelativeNormalized(obj.normalizedPosition))
        {
            print("stylus touch up");
            UpdateStylusIndicator(obj);
            stylusIndicator.SetActive(false);

            if(ForwardStylusInput)
            {
                BrowserNative.zfb_mouseMove(browser.ID, obj.normalizedPosition.x, 1.0f - obj.normalizedPosition.y);
                BrowserNative.zfb_mouseButton(browser.ID, BrowserNative.MouseButton.MBT_LEFT, false, 0);
            }
            
        }
    }

    private void Communication_OnStylusTouchDown(NormalizedStylusEvent obj)
    {
        if(sheetsInteraction.selectedCellTracker.CanSelectCells() && !sheetsInteraction.MaskedInputAreas.CollidesWithMasksRelativeNormalized(obj.normalizedPosition))
        {
            stylusIndicator.SetActive(true);
            if(ForwardStylusInput)
            {
                BrowserNative.zfb_mouseMove(browser.ID, obj.normalizedPosition.x, 1.0f - obj.normalizedPosition.y);
                BrowserNative.zfb_mouseButton(browser.ID, BrowserNative.MouseButton.MBT_LEFT, true, 0);
            }
            
            print("stylus touch down");
            UpdateStylusIndicator(obj);
        }
    }

    private void Communication_OnStylusMove(NormalizedStylusEvent obj)
    {
        if(sheetsInteraction.selectedCellTracker.CanSelectCells() && !sheetsInteraction.MaskedInputAreas.CollidesWithMasksRelativeNormalized(obj.normalizedPosition))
        {
            BrowserNative.zfb_mouseMove(browser.ID, obj.normalizedPosition.x, 1.0f - obj.normalizedPosition.y);
            UpdateStylusIndicator(obj);
        }
    }

    private void Communication_OnStylusButtonUp(NormalizedStylusEvent obj)
    {
        print("stylus button up");
        UpdateStylusIndicator(obj);
    }

    private void Communication_OnStylusButtonDown(NormalizedStylusEvent obj)
    {
        print("stylus button down");
        UpdateStylusIndicator(obj);
    }
    

    // Update is called once per frame
    void Update()
    {
        if(WheelHideTimer.Elapsed.TotalSeconds > HideDelay && !WheelHiddenAtStart)
        {
            WheelBrowser.SetActive(false);
            WheelHiddenAtStart = true;
        }
    }
}
