﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InformationLayers : MonoBehaviour
{
    public GameObject InteractionObject;
    public GameObject[] Layers;

    public float showThreshold = 0.05f;
    public bool LayersActivated = false;

    [Header("Layout")]
    public float LayerGap = 0.05f;
    public float startDistance = 0.1f;

    public float startupDelay = 2.0f;
    private System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
    public bool UseKeyActivation = false;

    //Start is called before the first frame update
    void Start()
    {
        timer.Start();
        var tooltips = FindObjectsOfType<CalibrateTooltip>();
        foreach(var tip in tooltips)
        {
            if(tip.gameObject.activeSelf)
            {
                InteractionObject = tip.tooltip;
            }
        }

        //HideLayers();
        Communication.OnKey += Communication_OnKey;
    }

    public void RepositionLayers()
    {
        for (int i =0;i<Layers.Length;++i)
        {
            Layers[i].transform.position = this.transform.position - this.transform.forward * (startDistance + LayerGap * i);
            Layers[i].transform.rotation = this.transform.rotation;
        }
    }

    public void ShowLayers()
    {
        foreach(var layer in Layers)
        {
            layer.SetActive(true);
        }
    }

    public void HideLayers()
    {
        foreach (var layer in Layers)
        {
            layer.SetActive(false);
        }
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        if (!UseKeyActivation) return;
        if (obj.keyCode == (KeyCode)18)
        {
            if (obj.pressed == true)
            {
                LayersActivated = true;
            }
            else
            {
                LayersActivated = false;
            }
        }
    }

    //checks if a point in 3d space is within the bounds of a finite size plane when projected onto it
    public bool IsProjectedPointInsidePlane(Vector3 point, Vector3 planePos, Vector3 planeNormal, Vector3 planeRight, Vector3 planeUp, float planeWidth, float planeHeight)
    {
        var fromTo = point - planePos;
        var x = Vector3.Dot(fromTo, planeRight);
        var y = Vector3.Dot(fromTo, planeUp);

        if (x > planeWidth / 2.0f || x < -planeWidth / 2.0f) return false;
        if (y > planeHeight / 2.0f || y < -planeHeight / 2.0f) return false;

        
        return true;
    }

    //Update is called once per frame
    void Update()
    {
        if (timer.Elapsed.TotalSeconds < startupDelay)
            return;

        GameObject closestLayer = null;
        float minDistance = float.MaxValue;
        foreach(var layer in Layers)
        {
            var planeNormal = layer.transform.forward;
            

            if (LayersActivated && IsProjectedPointInsidePlane(InteractionObject.transform.position, layer.transform.position, planeNormal, layer.transform.right, layer.transform.up, layer.transform.localScale.x, layer.transform.localScale.y))
            {
                var fromTo = InteractionObject.transform.position - layer.transform.position;
                var dist = Mathf.Abs(Vector3.Dot(layer.transform.forward, fromTo));
                if (dist <= showThreshold && dist < minDistance)
                {
                    closestLayer = layer;
                    minDistance = dist;
                }                
            }
        }

        foreach (var layer in Layers)
        {
            if (layer != closestLayer) //theres a bug/weird behaviour in unity that can prevent update() from being called if an object is disabled and then reenabled in the same frame. dont know why it happens but this is why i make sure not to disable it here
            {
                layer.SetActive(false);
            }
        }

        if (closestLayer != null)
        {
            
            closestLayer.SetActive(true);
        }
    }
}
