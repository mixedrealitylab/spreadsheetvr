﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

[System.Serializable]
public enum CellSizeSetting
{
    TWOX,
    FOURX,
    EIGHTX
}

public class SelectedCellTracker : MonoBehaviour
{
    public Browser browser;

    public Vector3 debugRelativePos;
    public CellSizeSetting SizeSetting;
    private CellSizeSetting LastSizeSetting;

    public GameObject EightXParent;
    public GameObject FourXParent;
    public GameObject TwoXParent;

    public GameObject ActiveGridParent;
    public CellAdjuster ActiveCellAdjuster;

    public GridCellInfo[] SelectedCells;
    public PenTip penTip;
    public GoogleSheetsInteraction sheetsInteraction;

    [Header("Debug")]
    public GridCellInfo[] CellsToSelect;

    [ContextMenu("Select Debug Cells")]
    public void SelectDebugCells()
    {
        foreach(var cell in CellsToSelect)
        {
            AddSelectedCell(cell);
        }
    }

    

    //Start is called before the first frame update
    void Start()
    {
        penTip = AppConfig.Instance.ActivePen;
        Communication.OnStylusTouchDown += Communication_OnStylusTouchDown;
        Communication.OnStylusTouchUp += Communication_OnStylusTouchUp;
        Communication.OnStylusMove += Communication_OnStylusMove;

        UpdateActiveParent();
        LastSizeSetting = SizeSetting;
    }

    public void AddSelectedCell(GridCellInfo cell)
    {
        Array.Resize(ref SelectedCells, SelectedCells.Length + 1);
        SelectedCells[SelectedCells.Length - 1] = cell;
        
        cell.SelectedInSheet = true;

        var activeSheet = sheetsInteraction.CurrentSheet.Name;
        cell.Sheet = activeSheet;
    }

    public void RemoveSelectedCell(GridCellInfo cell)
    {
        var idx = Array.IndexOf(SelectedCells, cell);
        if(idx >= 0)
        {
            SelectedCells[idx] = SelectedCells[SelectedCells.Length - 1];
        }

        Array.Resize(ref SelectedCells, SelectedCells.Length - 1);
        cell.SelectedInSheet = false;
        //cell.gameObject.SetActive(false);
    }

    [ContextMenu("Clear selected cells")]
    public void ClearSelectedCells()
    {
        if(SelectedCells != null)
        {
            foreach(var cell in SelectedCells)
            {
                //cell.gameObject.SetActive(false);
                cell.SelectedInSheet = false;
            }
        }

        Array.Resize(ref SelectedCells, 0);
    }

    public Vector2[] debugTest;
    int cnt = 0;

    private bool PointInCell(Transform cell, Vector2 p)
    {
        var pos = browser.transform.InverseTransformPoint(cell.parent.TransformPoint(cell.localPosition));
        debugTest[cnt] = pos;
        cnt++;
        var size = cell.localScale;

        if (p.x < pos.x - size.x / 2.0f || p.x > pos.x + size.x / 2.0f) return false;
        if (p.y < pos.y - size.y / 2.0f || p.y > pos.y + size.y / 2.0f) return false;

        return true;
    }

    public bool CanSelectCells()
    {
        return  (penTip.AttachedFunc == null || penTip.AttachedFunc.AllowCellSelectionWhileAttached) && !(sheetsInteraction.WheelMenu.gameObject.activeSelf);
    }

    private bool RectInCell(Transform cell, Rect rect)
    {
        var cellPos = browser.transform.InverseTransformPoint(cell.parent.TransformPoint(cell.localPosition));
        var size = cell.localScale;

        Rect otherRect = new Rect(cellPos - size / 2.0f, size);
        return rect.Overlaps( otherRect, true); 
    }

    private bool CurrentlyTouching = false;
    private Vector2 lastMousePosition;

    public void OnTouchDown(Vector2 relativePos)
    {
        if (!CanSelectCells()) return;

        CurrentlyTouching = true;
        debugRelativePos = relativePos;

        ClearSelectedCells();
        var grid = ActiveCellAdjuster.cells;
        debugTest = new Vector2[grid.Length];
        lastMousePosition = relativePos;

        cnt = 0;

        foreach(var cell in grid)
        {
            if(PointInCell(cell.transform, relativePos))
            {
                AddSelectedCell(cell.GetComponent<GridCellInfo>());
            }
        }
    }

    public void OnMove(Vector2 relativePos)
    {
        if (!CanSelectCells() || !CurrentlyTouching) return; //if currentlytouching is false but we still get here, we started touching a cell when CanSelectCells was not allowed (and therefor currentlytouching was not set to true)

        var grid = ActiveCellAdjuster.cells;

        var mouseRect = new Rect(lastMousePosition, relativePos - lastMousePosition);

        foreach(var cell in grid)
        {
            var cellInfo = cell.GetComponent<GridCellInfo>();
            var rectInCell = RectInCell(cell.transform, mouseRect);

            if (!cellInfo.SelectedInSheet &&  rectInCell)
            {
                AddSelectedCell(cellInfo);
            }

            if(cellInfo.SelectedInSheet && !rectInCell)
            {
                RemoveSelectedCell(cellInfo);
            }
        }
    }

    public void OnTouchUp(Vector2 relativePos)
    {
        if (penTip.AttachedFunc != null) return;

        CurrentlyTouching = false;
    }

    private void Communication_OnStylusTouchDown(NormalizedStylusEvent obj)
    {
        OnTouchDown(obj.normalizedPosition -new Vector2(0.5f, 0.5f));
    }

    private void Communication_OnStylusMove(NormalizedStylusEvent obj)
    {
        OnMove(obj.normalizedPosition - new Vector2(0.5f, 0.5f));
    }

    private void Communication_OnStylusTouchUp(NormalizedStylusEvent obj)
    {
        OnTouchUp(obj.normalizedPosition - new Vector2(0.5f, 0.5f));
    }

    private void UpdateActiveParent()
    {
        GameObject[] parents = {
            TwoXParent,
            FourXParent,
            EightXParent
        };

        foreach (var parent in parents)
        {
            if (parent != null) parent.SetActive(false);
        }

        switch (SizeSetting)
        {
            case CellSizeSetting.TWOX:
                TwoXParent.SetActive(true);
                ActiveGridParent = TwoXParent;
                browser.Zoom = 2.0f;
                break;
            case CellSizeSetting.FOURX:
                FourXParent.SetActive(true);
                ActiveGridParent = FourXParent;
                browser.Zoom = 4.0f;
                break;
            case CellSizeSetting.EIGHTX:
                EightXParent.SetActive(true);
                ActiveGridParent = EightXParent;
                browser.Zoom = 8.0f;
                break;
        }

        ActiveCellAdjuster = ActiveGridParent.GetComponent<CellAdjuster>();
        var cells = ActiveCellAdjuster.cells;
        foreach(var cell in cells)
        {
            //cell.SetActive(false);
        }
    }

    public string GetRangeString()
    {
        return GetRangeString(SelectedCells);
    }

    public string GetRangeString(GridCellInfo[] cells)
    {
        string rangeResult = "";
        for (int i = 0; i < cells.Length; ++i)
        {
            rangeResult += cells[i].GetCellName();
            if (i != cells.Length - 1)
            {
                rangeResult += ",";
            }
        }

        return rangeResult;
    }

    // Update is called once per frame
    void Update()
    {
        if(LastSizeSetting != SizeSetting)
        {
            LastSizeSetting = SizeSetting;
            UpdateActiveParent();
        }

        //switch(SizeSetting)
        //{
        //    case CellSizeSetting.TWOX:
        //        browser.Zoom = 2.0f;
        //        break;
        //    case CellSizeSetting.FOURX:
        //        browser.Zoom = 4.0f;
        //        break;
        //    case CellSizeSetting.EIGHTX:
        //        browser.Zoom = 8.0f;
        //        break;
        //}
    }
}
