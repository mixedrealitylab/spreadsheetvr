﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PenHoverAndClickObject : MonoBehaviour
{
    public static PenHoverAndClickObject LastHoveredPenHoverAndClickObject = null;

    public AudioSource AudioSource;
    public AudioClip SelectSoundEffect;

    private Transform enteredTransform;
    private PenTip enteredPenTip;
    public Material RegularMaterial;
    public Material HoverMaterial;

    public bool IsHovered = false;
    public bool RequiresPenTip = true;

    // Start is called before the first frame update
    public void Start()
    {
        Communication.OnKey += Communication_OnKey;

        if (AudioSource == null) AudioSource = GetComponent<AudioSource>();
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        if (IsHovered &&  NetworkStylusInteraction.FirstButtonPressed(obj) && obj.pressed == true)
        {
            OnInteraction(enteredPenTip);
        }
    }

    public virtual void OnInteraction(PenTip pentip)
    {

    }

    public virtual void OnPenEntered(PenTip other)
    {

    }

    public virtual void OnPenExit(PenTip other)
    {

    }

    public virtual void ApplyHoverEffect(PenTip other)
    {
        GetComponent<Renderer>().material = HoverMaterial;
    }

    public virtual void UnapplyHoverEffect(PenTip other)
    {
        GetComponent<Renderer>().material = RegularMaterial;
    }

    private void OnDisable()
    {
        
        if(enteredPenTip)
        {
            OnPenExit(enteredPenTip);
        }

        IsHovered = false;

        UnapplyHoverEffect(null);
    }

    private void OnTriggerEnter(Collider other)
    {
        enteredTransform = other.transform;

        var penTip = enteredTransform.gameObject.GetComponent<PenTip>();
        if (!RequiresPenTip || penTip != null)
        {
            LastHoveredPenHoverAndClickObject = this;
            enteredPenTip = penTip;
            IsHovered = true;

            OnPenEntered(enteredPenTip);
            ApplyHoverEffect(penTip);

            if(AudioSource != null && SelectSoundEffect != null)
            {
                AudioSource.PlayOneShot(SelectSoundEffect);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        enteredTransform = null;
        var penTip = other.gameObject.GetComponent<PenTip>();
        if (!RequiresPenTip || penTip != null)
        {
            enteredPenTip = null;
            IsHovered = false;

            OnPenExit(penTip);
            UnapplyHoverEffect(penTip);
        }
    }

}

