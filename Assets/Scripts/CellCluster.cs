﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

public class CellCluster : MonoBehaviour
{
    public static CellCluster LastHoveredCluster = null;

    public GridCellInfo[] Cells;
    public GameObject[] Relations;
    public string ClusterName = "default";
    private Transform enteredTransform;
    private Collider enteredCollider;
    private PenTip enteredPenTip;
    public bool IsHovered = false;
    public Material DefaultMat;
    public Material HoveredMat;
    public Renderer[] renderers;

    // Start is called before the first frame update
    void Start()
    {
        Communication.OnKey += Communication_OnKey;
    }

    public void ChangeSourceCells(PenTip tip, NewRelationHandle relation)
    {
        var targetCell = relation.Parent.From; //the moverelationhandle has the target cell in its from field 

        if(targetCell.Relationship != null)
        {
            targetCell.Relationship.RemoveAllRelationships(true);
            targetCell.Relationship.AddRelationships(Cells);
            tip.DetachAttachedObject();
            targetCell.Relationship.OnGridCellInteraction(targetCell);
        }
    }

    private void Communication_OnKey(KeyMessage obj)
    {
        if (Cells == null || Cells.Length <= 0) return;

        var tracker = Cells[0].sheetsInteraction.selectedCellTracker; //just assume all cells are from the same sheet
        if (IsHovered && NetworkStylusInteraction.FirstButtonPressed(obj))
        {
            if(enteredPenTip != null && enteredPenTip.AttachedFunc != null && enteredPenTip.AttachedFunc is NewRelationHandle)
            {
                var handle = enteredPenTip.AttachedFunc as NewRelationHandle;
                ChangeSourceCells(enteredPenTip, handle);
            }
            else
            {
                tracker.ClearSelectedCells();

                foreach (var cell in Cells)
                {
                    tracker.AddSelectedCell(cell);
                }
            }
           
            //key input doesn't currently work, so I'm not doing it
            //var browser = Cells[0].sheetsInteraction.browser;
            //var id = browser.ID;
            //var shiftKey = KeyMappings.GetWindowsKeyCode(new Event { keyCode = KeyCode.LeftShift });

            //BrowserNative.zfb_keyEvent(id, true, shiftKey);
            ////BrowserNative.zfb_key
            
            //BrowserNative.zfb_keyEvent(id, false, shiftKey);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnDisable()
    {
        OnDehover(enteredCollider);
    }

    public void OnHover(Collider other)
    {
        IsHovered = true;
        foreach (var cell in Cells)
        {
            cell.VisualizeSelect();
        }

        if(Relations != null)
        {
            foreach(var relation in Relations)
            {
                relation.SetActive(true);
            }
        }
    }

    public void OnDehover(Collider other)
    {
        IsHovered = false;
        foreach (var cell in Cells)
        {
            cell.VisualizeDeselect();
        }

        if(Relations != null)
        {
            foreach(var relation in Relations)
            {
                relation.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        enteredTransform = other.transform;
        enteredCollider = other;

        var penTip = enteredTransform.gameObject.GetComponent<PenTip>();
        if (penTip != null)
        {
            enteredPenTip = penTip;
            
            LastHoveredCluster = this;

            foreach(var r in renderers)
            {
                r.material = HoveredMat;
            }

            OnHover(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        enteredTransform = null;
        enteredCollider = other;

        var penTip = other.gameObject.GetComponent<PenTip>();
        if (penTip != null)
        {
            enteredPenTip = null;
            
            foreach (var r in renderers)
            {
                r.material = DefaultMat;
            }

            OnDehover(other);
        }
    }
}
