﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class GenericGameObjectToggle : PenHoverAndClickObject
{
    public GameObject[] ObjectsToToggle;
    public GameObject[] ObjectsToToggleOpposite; //when the others are turned off, this is turned on, and vice versa
    public bool ToggledOnByDefault = false;
    
    public AudioClip InteractionSoundEffect;

    public bool Toggled = false;

    [Header("Transparency")]
    public bool ChangeAlphaOnToggle = true;
    public float AlphaToggledOn = 1.0f;
    public float AlphaToggledOff = 0.3f;

    private new Renderer renderer;
    public new void Start()
    {
        base.Start();

        foreach(var obj in ObjectsToToggle)
        {
            obj.SetActive(ToggledOnByDefault);
        }

        Toggled = ToggledOnByDefault;

        renderer = GetComponent<Renderer>();
        
    }

    public void AddGameObjectToToggle(GameObject g)
    {
        if(ObjectsToToggle == null)
        {
            ObjectsToToggle = new GameObject[1];
        }
        else
        {
            Array.Resize(ref ObjectsToToggle, ObjectsToToggle.Length + 1);
        }

        ObjectsToToggle[ObjectsToToggle.Length - 1] = g;
    }

    public void RemoveGameObjectToToggle(GameObject g)
    {
        var idx = Array.IndexOf(ObjectsToToggle, g);
        if(idx >=0)
        {
            ObjectsToToggle[idx] = ObjectsToToggle[ObjectsToToggle.Length - 1];
            Array.Resize(ref ObjectsToToggle, ObjectsToToggle.Length - 1);
        }
    }

    [ContextMenu("Toggle")]
    public void Toggle()
    {
        Toggle(!Toggled);
    }

    public virtual void Toggle(bool toggle)
    {
        foreach (var obj in ObjectsToToggle)
        {
            obj.SetActive(toggle);
        }

        foreach (var obj in ObjectsToToggleOpposite)
        {
            obj.SetActive(!toggle);
        }

        if (Toggled != toggle && AudioSource != null && InteractionSoundEffect != null)
        {
            AudioSource.PlayOneShot(InteractionSoundEffect);
        }

        Toggled = toggle;
    }

    public override void OnInteraction(PenTip pentip)
    {
        base.OnInteraction(pentip);

        Toggle(!Toggled);
    }

    public override void OnPenEntered(PenTip other)
    {
        base.OnPenEntered(other);
    }

    public override void OnPenExit(PenTip other)
    {
        base.OnPenExit(other);
    }

    private void Update()
    {
        if(ChangeAlphaOnToggle)
        {
            var col = renderer.material.color;
            if (Toggled)
            {
                col.a = AlphaToggledOn;
            }
            else
            {
                col.a = AlphaToggledOff;
            }
            renderer.material.color = col;
        }
    }
}

