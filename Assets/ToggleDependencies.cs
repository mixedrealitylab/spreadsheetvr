﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

/*
 * todo: we have a lot of these 'hover and interact' type of things now. should probably unify
 */
public class ToggleDependencies : GenericGameObjectToggle
{
    public TextMeshPro Text;

    public AlphaMaskRenderer alphaMaskRenderer;

    public GridCellInfo[] FakedAlphaMasking;

    public override void OnInteraction(PenTip pentip)
    {
        base.OnInteraction(pentip);
        if(!Toggled)
        {
            //Text.text = "Show dependencies";
            alphaMaskRenderer.AlphaMaskValue = 1.0f;
            foreach(var info in FakedAlphaMasking)
            {
                info.ForceAlphaMask = false;
                info.DisableAlphaMasking = true;
            }
        }
        else
        {
            //Text.text = "Hide dependencies";
            alphaMaskRenderer.AlphaMaskValue = 0.0f;

            foreach (var info in FakedAlphaMasking)
            {
                info.ForceAlphaMask = true;
                info.DisableAlphaMasking = false;
            }
        }
    }
}
