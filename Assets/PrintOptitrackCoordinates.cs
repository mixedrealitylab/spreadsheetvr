﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintOptitrackCoordinates : MonoBehaviour
{
    public int RigidBodyId = 555;

    public OptitrackStreamingClient StreamingClient;
    // Start is called before the first frame update
    void Start()
    {
        if (StreamingClient == null) StreamingClient = OptitrackStreamingClient.FindDefaultClient();
    }

    bool getPos = false;
    int num = 0;
    Vector3 middlePos = Vector3.zero;


    private List<Vector3> recordedPoints = new List<Vector3>();

    private string GetOctaveVector()
    {
        string result = "[";

        for (int i = 0; i < recordedPoints.Count; ++i)
        {
            var p = recordedPoints[i];
            result += p.x.ToString().Replace(",", ".") + "  " + p.y.ToString().Replace(",", ".") + "  " + p.z.ToString().Replace(",", ".");
            if (i != recordedPoints.Count - 1)
            {
                result += ";";
            }
        }
        result += "];";

        return result;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            //Debug.Log("Vive: " + transform.position.x.ToString().Replace(",",".") + "  " + transform.position.y.ToString().Replace(",",".") + "  " + transform.position.z.ToString().Replace(",","."));
            getPos = true;
        }

        if (getPos && num < 100)
        {
           var pos = StreamingClient.GetLatestRigidBodyState(RigidBodyId);
            middlePos += pos.Pose.Position;
                
            num++;
        }
        else if (getPos)
        {
            middlePos.x = middlePos.x / num;
            middlePos.y = middlePos.y / num;
            middlePos.z = middlePos.z / num;
            recordedPoints.Add(middlePos);
            Debug.Log("Optitrack: " + middlePos.x.ToString().Replace(",", ".") + "  " + middlePos.y.ToString().Replace(",", ".") + "  " + middlePos.z.ToString().Replace(",", "."));

            if (recordedPoints.Count >= 4)
                Debug.Log("optitrack = " + GetOctaveVector());

            getPos = false;
        }
        else
        {
            num = 0;
        }

    }

}
