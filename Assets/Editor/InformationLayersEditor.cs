﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(InformationLayers))]
public class InformationLayersEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var target = this.target as InformationLayers;
        if(GUILayout.Button("Reposition layers"))
        {
            target.RepositionLayers();
        }

        if (GUILayout.Button("Show Layers"))
        {
            target.ShowLayers();
        }

        if (GUILayout.Button("Hide Layers"))
        {
            target.HideLayers();
        }
    }
}

