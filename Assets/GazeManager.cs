﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeManager : MonoBehaviour
{
    public ActiveSheetSwitcher switcher;
    public string[] GazedLayers = new string[] { "browser" };
    public Transform Gaze;

    public GoogleSheetsInteraction CurrentGazedSheet;

    private int mask;

    // Start is called before the first frame update
    void Start()
    {
        mask = LayerMask.GetMask(GazedLayers);
        Gaze = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        CurrentGazedSheet = null;

        foreach(var sheet in switcher.Browsers)
        {
            sheet.CurrentlyGazedAt = false;
        }

        RaycastHit info;

        if(Physics.Raycast(new Ray { origin = Gaze.transform.position, direction = Gaze.transform.forward}, out info, float.MaxValue, mask))
        {
            var sheetsInteraction = info.collider.GetComponent<GoogleSheetsInteraction>();
            if(sheetsInteraction != null)
            {
                sheetsInteraction.CurrentlyGazedAt = true;
                CurrentGazedSheet = sheetsInteraction;
            }
        }

    }
}
