﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MaskedInputAreas : MonoBehaviour
{
    public Transform[] Masks;
    public Transform InputPlane;

    [Header("Debug")]
    public Transform TestPoint;
    public Transform OutputDebugPointOnPlane;
    public bool Test = false;

    //p should be in world pos
    public bool CollidesWithMasks(Vector3 p)
    {
        var onPlane = p - InputPlane.transform.forward * Vector3.Dot(InputPlane.transform.forward, p - InputPlane.transform.position);
        //OutputDebugPointOnPlane.transform.position = onPlane;

        //onPlane -= -InputPlane.transform.position;
        //var x = Vector3.Dot(onPlane, InputPlane.transform.right);
        //var y = Vector3.Dot(onPlane, InputPlane.transform.up);

        //var relativePlaneCoords = new Vector2(x, y);
        foreach(var mask in Masks)
        {
            if (IsInsideMask(onPlane, mask))
            {
                return true;
            }
        }

        return false;
    }

    public Vector3 NormalizedTouchCoordToWorldCoord(Vector2 touch)
    {
        var scale = InputPlane.transform.lossyScale;
        var right = InputPlane.transform.right * scale.x;
        var up = InputPlane.transform.up * scale.y;


        return InputPlane.transform.position + right * (touch.x - 0.5f) + up * (touch.y - 0.5f);
    }

    //by relative I mean it's already normalized in [0-1] range
    public bool CollidesWithMasksRelativeNormalized(Vector2 relativeP)
    {
        return CollidesWithMasks(NormalizedTouchCoordToWorldCoord(relativeP));
    }

    private bool IsInsideMask(Vector3 projectedPoint, Transform mask)
    {
        var maskPos = mask.transform.position;
        var fromTo = projectedPoint - maskPos;

        var x = Vector3.Dot(fromTo, mask.transform.right);
        var y = Vector3.Dot(fromTo, mask.transform.up);

        var halfWidth = mask.lossyScale.x / 2.0f;
        
        if(x > halfWidth ||x < -halfWidth)
        {
            return false;
        }

        var halfHeight = mask.lossyScale.y / 2.0f;
        if(y > halfHeight ||y < -halfHeight)
        {
            return false;
        }

        return true;
    }

    //Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(Test)
        {
            Test = false;
            if(CollidesWithMasks(TestPoint.transform.position))
            {
                print("collides!");
            }
            else
            {
                print("doesnt collide!");
            }
        }
    }
}
