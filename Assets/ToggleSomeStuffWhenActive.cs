﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleSomeStuffWhenActive : MonoBehaviour
{
    public ActiveSheetSwitcher switcher;
    public GoogleSheetsInteraction me;
    public ToggleSideSheets toggler;
    public Renderer myRenderer;

    public GameObject[] ObjectsToToggleOn;
    public GameObject[] ObjectsToToggleOff;
    public GameObject[] ObjectsToToggleOnWhenImNotActive;
    public GameObject[] ObjectsToToggleOffWhenImNotActive;
    public GameObject LargeBrowser;

    // Start is called before the first frame update
    void Start()
    {
        if (me == null) me = GetComponent<GoogleSheetsInteraction>();
    }

    GoogleSheetsInteraction lastActiveBrowser;
    // Update is called once per frame
    void Update()
    {
        if(lastActiveBrowser != me && switcher.activeBrowser == me)
        {
            toggler.Toggle(false);
        }

        if(switcher.activeBrowser == me)
        {
            foreach (var obj in ObjectsToToggleOn)
            {
                obj.SetActive(true);
            }

            foreach (var obj in ObjectsToToggleOff)
            {
                obj.SetActive(false);
            }
        }
        else
        {
            foreach (var obj in ObjectsToToggleOffWhenImNotActive)
            {
                obj.SetActive(false);
            }

            foreach (var obj in ObjectsToToggleOnWhenImNotActive)
            {
                obj.SetActive(true);
            }
        }

        
        lastActiveBrowser = switcher.activeBrowser;
        //myRenderer.enabled = !LargeBrowser.activeSelf;
    }
}
