﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmazingMatrix : MonoBehaviour
{
    public Matrix4x4 Matrix;

    // Start is called before the first frame update
    void Start()
    {
        Matrix4x4 mat = Matrix4x4.zero;
        mat[0, 0] = -0.93678f;
        mat[1, 0] = -0.059416f;
        mat[2, 0] = 0.19675f;

        mat[0, 1] = -0.78694f;
        mat[1, 1] = 1.9732f;
        mat[2, 1] = -0.86965f;

        mat[0, 2] = -0.10017f;
        mat[1, 2] = -0.020781f;
        mat[2, 2] = -0.97845f;

        mat[0, 3] = -0.92225f;
        mat[1, 3] = 0.62483f;
        mat[2, 3] = -0.96295f;
        mat[3, 3] = 1;

        Matrix = mat;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
