﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptitrackToVive : MonoBehaviour
{
    public OptitrackRigidBody rigidBody;

    public AmazingMatrix Mat;

    public OptitrackStreamingClient streamingClient;

    public bool SetPosition = true;
    public bool SetRotation = true;
    // Start is called before the first frame update
    void Start()
    {
        Mat = FindObjectOfType<AmazingMatrix>();
        if (streamingClient == null) streamingClient = OptitrackStreamingClient.FindDefaultClient();
    }

    // Update is called once per frame
    void Update()
    {
        var mat = Mat.Matrix;

        var state = streamingClient.GetLatestRigidBodyState(rigidBody.RigidBodyId);

        if (state == null) return;

        Vector3 newPos;
        var pos = state.Pose.Position;

        newPos.x = mat[0, 0] * pos.x + mat[0, 1] * pos.y + mat[0, 2] * pos.z + mat[0, 3];
        newPos.y = mat[1, 0] * pos.x + mat[1, 1] * pos.y + mat[1, 2] * pos.z + mat[1, 3];
        newPos.z = mat[2, 0] * pos.x + mat[2, 1] * pos.y + mat[2, 2] * pos.z + mat[2, 3];
       if(SetPosition) transform.position = newPos;

        float w = Mathf.Sqrt(1.0f + mat[0, 0] + mat[1, 1] + mat[2, 2]) / 2.0f;
        float w4 = (4.0f * w);
        float x = (mat[2, 1] - mat[1, 2]) / w4;
        float y = (mat[0, 2] - mat[2, 0]) / w4;
        float z = (mat[1, 0] - mat[0, 1]) / w4;

        if(SetRotation)
        {
            transform.rotation = state.Pose.Orientation;
            transform.Rotate(new Vector4(x, y, z, w));
        }
        
    }
}
