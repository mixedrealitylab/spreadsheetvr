vive = [-0.9928559  0.828595  -1.140789;-1.219893  0.8718259  -1.142081;-0.7968552  0.8271406  -1.197041;-0.9281099  0.8391013  -0.9365122];
optitrack = [-0.02075964  0.10354  0.08555619;0.1954825  0.1321638  0.1049191;-0.2267096  0.09683367  0.1075938;-0.06773038  0.1051369  -0.134084];
y = [vive'(:,1); vive'(:,2); vive'(:,3); vive'(:,4)];

zero_vec = zeros(1, 4);

A = [optitrack(1,:), 1, zero_vec, zero_vec; zero_vec, optitrack(1,:), 1 ,zero_vec; zero_vec, zero_vec, optitrack(1,:), 1;
    optitrack(2,:), 1, zero_vec, zero_vec; zero_vec, optitrack(2,:), 1 ,zero_vec; zero_vec, zero_vec, optitrack(2,:), 1;
    optitrack(3,:), 1, zero_vec, zero_vec; zero_vec, optitrack(3,:), 1 ,zero_vec; zero_vec, zero_vec, optitrack(3,:), 1;
    optitrack(4,:), 1, zero_vec, zero_vec; zero_vec, optitrack(4,:), 1 ,zero_vec; zero_vec, zero_vec, optitrack(4,:), 1];
    
x = zeros(12,1);

x = inv(A) *y;

matrix = [x(1), x(2), x(3), x(4); x(5), x(6), x(7), x(8); x(9), x(10), x(11), x(12); 0 0 0 1]

save matrix.txt matrix

%check if correct

##optitrack = [1 1 0; 2 11 0; -1 1 3; 2 -2 3 ]
##m = [cos(pi/3) -sin(pi/3) 0 7; sin(pi/3) cos(pi/3) 0 6; 0 0 1 5; 0 0 0 1]
##vive = m*[[optitrack'(:,1);1],[optitrack'(:,2);1],[optitrack'(:,3);1],[optitrack'(:,4);1]];
##vive = vive(1:3, :);
##vive = vive'
##
##
##y = [vive'(:,1); vive'(:,2); vive'(:,3); vive'(:,4)];
##
##zero_vec = zeros(1, 4);
##
##A = [optitrack(1,:), 1, zero_vec, zero_vec; zero_vec, optitrack(1,:), 1 ,zero_vec; zero_vec, zero_vec, optitrack(1,:), 1;
##    optitrack(2,:), 1, zero_vec, zero_vec; zero_vec, optitrack(2,:), 1 ,zero_vec; zero_vec, zero_vec, optitrack(2,:), 1;
##    optitrack(3,:), 1, zero_vec, zero_vec; zero_vec, optitrack(3,:), 1 ,zero_vec; zero_vec, zero_vec, optitrack(3,:), 1;
##    optitrack(4,:), 1, zero_vec, zero_vec; zero_vec, optitrack(4,:), 1 ,zero_vec; zero_vec, zero_vec, optitrack(4,:), 1];
##    
##x = zeros(12,1);
##
##x = inv(A) *y;
##
##matrix = [x(1), x(2), x(3), x(4); x(5), x(6), x(7), x(8); x(9), x(10), x(11), x(12); 0 0 0 1]

