		function hoverCallback(pizzaSlice, ev)
		{
			pizzaSlice.element.classList.add("tileHovered");
			
			//console.log("lol " + pizzaSlice.title + " " + ev.screenX + " " + ev.screenY);
			if(window.unityHoverCallback)
			{
				console.log("calling unityHoverCallback");
				window.unityHoverCallback(pizzaSlice.title, ev);
			}
		}

		function endHoverCallback(pizzaSlice, ev)
		{
			pizzaSlice.element.classList.remove("tileHovered");
			//console.log("lol off " + pizzaSlice.title);
			if(window.unityHoverEndCallback)
			{
				console.log("calling unityHoverEndCallback");
				window.unityHoverEndCallback(pizzaSlice.title, ev);
			}
		}

		function mouseMove(pizzaSlice, ev)
		{
			//if the element is triggering the mousemove event but doesn't have the tileHovered class, something went buggy and we have
			//to manually hover it
			if(!pizzaSlice.element.classList.contains("tileHovered"))
			{
				doHover(pizzaSlice.element);
			}

			//console.log("moving around on " + pizzaSlice.title);
			if(window.unityMouseMoveCallback)
			{
				console.log("calling unityMouseMoveCallback");
				window.unityMouseMoveCallback(pizzaSlice.title,ev);
			}
		}
		
		function generateHoverFunc(pizzaSlice)
		{
			return function(ev){ hoverCallback(pizzaSlice, ev); };
		}

		function generateEndHoverFunc(pizzaSlice)
		{
			return function(ev){ endHoverCallback(pizzaSlice, ev); };
		}

		function generateMouseMoveFunc(pizzaSlice)
		{
			return function(ev){ mouseMove(pizzaSlice, ev); };
		}

		var pizzaSlices=[];
		
		function doHover(element)
		{
			var ev = new MouseEvent("mouseover",{bubbles:true, cancelable:true, view: window}); 
			element.dispatchEvent(ev);
		}

		function deHoverAll()
		{
			var tiles = document.getElementsByClassName("tile");

			for(var i=0;i<tiles.length;++i)
			{
				var ev = new MouseEvent("mouseout",{bubbles:true, cancelable:true, view: window}); 
				tiles[i].dispatchEvent(ev);
			}
		}

		function initAnimWheel(config)
		{
			
			let leng = config.length;
			let parent = document.getElementById("animationWrapper");
			console.log (parent);
			let dirOffset = 360/leng;
			let perc = Math.floor(Math.tan((90-dirOffset)/2*Math.PI/180)*100*2);

			console.log("why: "+perc+" dirOffsetn: "+dirOffset);

			for(let i=0;i<leng;i++){
				if(i%2==3){continue;}
				let dir = 225+dirOffset*i;
                
                
				let e = document.createElement("div");				
				e.style.transform = "rotate("+dir+"deg)";
				e.classList.add("tile");				
				e.style.clipPath = "polygon(0 0, "+perc+"% 200%, 200% "+perc+"%)";
				e.id = "id" + config[i];
				let slice = {
                	element: e,
                	title: config[i]
                };

				pizzaSlices.push(slice);

				e.onmouseover = generateHoverFunc(pizzaSlices[i]);		
				e.onmouseout = generateEndHoverFunc(pizzaSlices[i]);
				e.onmousemove = generateMouseMoveFunc(pizzaSlices[i]);

						
				let c = document.createElement("div");		
				var textNode = document.createTextNode(config[i]);		

				c.appendChild(textNode);
				c.style.pointerEvents = "none";
				c.style.transform = "translate(-50%, -50%) rotate("+(-dir)+"deg)";
				e.appendChild(c);
				parent.appendChild(e);
			}
		}