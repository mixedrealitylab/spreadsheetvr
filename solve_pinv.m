path = "./matrix_out.mat";

inputs = load(path);

A = inputs.A;
y = inputs.y;

[u, s, v] = svd(A);

p = v * pinv(s) * (u')* y;

pt = p(4:6)
norm_pt = norm(pt)

pw = p(1:3)
norm_pw = norm(pw)

save("result22.mat", 'pt', 'pw');