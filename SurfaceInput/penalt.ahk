;  File name: surface-pen-slide-show-control.ahk
;
;  This program helps the presenter scroll through slides, using Surface Pen
;  
;  Single-click on pen button to scroll forward
;  Double-click on pen button to scroll backward
;

#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%

#IfWinActive ahk_class screenClass
#F20::
  Send, {Right}
Return

#F19::
  Send, {Left}
Return