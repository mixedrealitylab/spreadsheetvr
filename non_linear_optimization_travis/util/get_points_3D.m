function points = get_points_3D (xs, ys, zs)
    
    point_num = max(size(xs));
    
    if max(size(xs)) ~= max(size(ys))    
        disp("Number of X- and Y-coordinates do not match. Abort.");
        points = 0;
        return
    else
        points = zeros(point_num, 3);
    end
    
    for i = 1 : point_num
       points(i, 1) = xs(i);
       points(i, 2) = ys(i);
       points(i, 3) = zs(i);  
    end
end