function [varargout] = enum(first_index = 1)
  for i=1:nargout
    varargout{i} = i + first_index - 1;
  endfor
endfunction
