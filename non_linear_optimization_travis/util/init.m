function init()  

  % Enum to select optimizer: Gauss-Newton, Gradient Descent, Levenberg-Marquardt
  [gn, gd, lm] = enum();
  global GN;
  GN = gn;
  global GD;
  GD = gd;
  global LM;
  LM = lm;
endfunction
