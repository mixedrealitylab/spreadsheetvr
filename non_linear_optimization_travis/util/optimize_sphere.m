function [p, r, iter, dur] = optimize_sphere(xs, ys, zs, p0, optimizer)
    global GN GD LM;
    
    p = 0;
    r = 0;
    iter = 0;
    
    % construct point array
    points = get_points_3D(xs, ys, zs);
    % if there are not points, exit
    if points == 0
        return
    end
    
    % define "||z-x||"
    norm_px = @(p, x) sqrt((p(1)-x(1))^2 + (p(2)-x(2))^2 + (p(3)-x(3))^2);
    
    % define "f(z1,z2,z3,R) = R - ||z-x||"
    f = @(p, x) p(4) - norm_px(p, x);
    
    % define nabla(f)
    f_par_deriv_z1 = @(p, x) -(p(1)-x(1)) / norm_px(p, x);
    f_par_deriv_z2 = @(p, x) -(p(2)-x(2)) / norm_px(p, x);
    f_par_deriv_z3 = @(p, x) -(p(3)-x(3)) / norm_px(p, x);
    f_par_deriv_R = @(p, x) 1;
    
    nabla_f = {f_par_deriv_z1, f_par_deriv_z2, f_par_deriv_z3, f_par_deriv_R};
    
    % timing start    
    start = 0;
    finish = 0;
    
    % execute non linear least squares
    if optimizer == GN    
      start = clock();
      [p, r, iter] = gauss_newton(f, nabla_f, p0, points);
      finish = clock();
    elseif optimizer == GD    
      start = clock();
      [p, r, iter] = gradient_descent(f, nabla_f, p0, points);
      finish = clock();    
    elseif optimizer == LM    
      start = clock();
      [p, r, iter] = levenberg_marquardt(f, nabla_f, p0, points, 1);
      finish = clock();
    else
      printf("Error. Selected optimizer does not exist. Selected: %d\r\n" + optimizer);
    endif
    
    dur = etime(finish, start);
    
end