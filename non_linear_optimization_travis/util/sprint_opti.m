function s_opti = sprint_opti(selected)
  global GN GD LM;
  switch (selected)
    case GN
      s_opti = "Gauss-Newton";
    case GD
      s_opti = "Gradient-Descent";
    case LM
      s_opti = "Levenberg-Marquardt";
  endswitch
endfunction
