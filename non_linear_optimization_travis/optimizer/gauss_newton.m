function [p, r, iter] = gauss_newton(f, nabla_f, p0, observations)
    
    % initialize residual and parameters
    r = residual(p0, observations, f);
    p = p0;
    
    % exit check 
    last_r = r;
    MIN_DELTA = 1e-6;
    EPSILON = 1e-6;
    
    % iterations counter
    iter = 0;
    MAX_ITER = 1000;

    while iter < MAX_ITER 
        % get new Jacobian Matrix
        J = jacobian(p, observations, nabla_f);
        disp(r);
        disp("#################################");
        Jp = pinv(J);
        
        % update parameter vector
        p = p - (Jp * r)';
        
        draw_circle(p); 
        % update residual vector
        r = residual(p, observations, f);
        
        % check for exit conditions
        %  1: r is small enough
        %  2: r did not change enough since last iteration
        if norm(r) < EPSILON || norm(r - last_r) < MIN_DELTA
            break;
        end
        last_r = r;

        iter = iter + 1;
    end
end