function [p, r, iter] = levenberg_marquardt(f, nabla_f, p0, observations, lambda = 1)
  
  % initialize parameters, residual, Jacobian, gradient, cost gradient and cost
  p = p0;
  r = residual(p, observations, f);
  J = jacobian(p, observations, nabla_f);
  J_square = J' * J;
  g = J_square + lambda * eye(size(J_square));
  C_gradient = J' * r;
  C = 0.5 * norm(r).^2;
    
  % exit check 
  MAX_ITER = 1000;
  EPSILON = 1e-4;
  MIN_DELTA = 1e-6;
  
  % lambda grow and shrink factors
  lambda_up = 1.5;
  lambda_down = 10;
  
  % iterations counter
  iter = 0;
  while iter < MAX_ITER && norm(r) > EPSILON      
    
    % update parameter vector
    p_new = p - (inv(g) * C_gradient)';
    iter = iter + 1;

    % see result of step
    r_new = residual(p_new, observations, f);
    C_new = 0.5 * norm(r_new).^2;
    
    % if cost did not change, break out of loop
    if (C - C_new) < MIN_DELTA
      break;
    endif
    
    % if step is downhill update and decrease lambda (towards Gauss-Newton)
    % else stay and increase lambda (towards Gradient Descent)
    if C_new < C
      p = p_new;
      r = r_new;
      C = C_new;      
      % update Jacobian matrix
      J = jacobian(p, observations, nabla_f);
      
      % update gradient
      J_square = J' * J;
      g = J_square + lambda * eye(size(J_square));
      C_gradient = J' * r;
      lambda = lambda / lambda_down;
    else
      lambda = lambda * lambda_up;
    endif
  endwhile
  
endfunction
