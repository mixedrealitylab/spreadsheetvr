function [p, r, iter] = gradient_descent(f, nabla_f, p0, observations, gamma = 0.01)
  
  % initialize residual and parameters
  p = p0;
  r = residual(p, observations, f);
  J = jacobian(p, observations, nabla_f);
  d = J' * r;
    
  % exit check 
  MAX_ITER = 1000;
  EPSILON = 1e-4;
  
  last_p = p;
  last_d = d;
  
  % iterations counter
  iter = 0;
  while iter < MAX_ITER && norm(r) > EPSILON      
    
    % update parameter vector
    p = p - gamma * d';
    iter = iter + 1;
    
    % update residual vector
    r = residual(p, observations, f);
    % update Jacobian matrix
    J = jacobian(p, observations, nabla_f);
    % update gradient
    d = J' * r;
    % update gamma (step size)
    delta_p = p - last_p;
    delta_d = d - last_d;
    norm_delta_d = norm(delta_d);
    
    if norm_delta_d ~= 0 % sanity check
      gamma = norm((delta_p) * (delta_d)) / norm_delta_d.^2;        
      last_p = p;
      last_d = d;
    else
      break;
    endif
    
  endwhile
endfunction
