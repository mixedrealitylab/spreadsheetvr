function r = residual(p, points, f)
    rows = size(points, 1);
    
    % initialize residual vector
    r = zeros(rows, 1);
    
    % /         \
    %| f(p, x_1) | 
    %|     .     |
    %|     .     |
    %| f(p, x_n) |
    % \         /
    for row = 1:rows
        r(row) = f(p, points(row,:));
    end
end