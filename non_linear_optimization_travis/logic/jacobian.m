
function J = jacobian(p, points, nabla_f)
    rows = size(points, 1);
    cols = size(p, 2);
    
    % init Jacobian
    J = zeros(rows, cols);
    
    % /                                                                  \
    %| d(f(p, x_1))/d(p_1)  d(f(p, x_1))/d(p_2) ...  d(f(p, x_1))/d(p_n) |
    %|         .                    .                           .        |
    %|         .                    .                           .        |
    %| d(f(p, x_n))/d(p_1)  d(f(p, x_n))/d(p_2) ...  d(f(p, x_n))/d(p_n) |
    % \                                                                  /
    for row = 1:rows
       for col = 1:cols
          J(row, col) = nabla_f{col}(p, points(row,:));
       end
    end
end