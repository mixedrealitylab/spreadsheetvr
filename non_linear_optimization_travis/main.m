% Add paths to all other folders
addpath("./draw");
addpath("./logic");
addpath("./optimizer");
addpath("./util");

global GN GD LM;
init();

% SELECT OPTIMIZER HERE %
optimizer = LM;

s_opti = sprint_opti(optimizer);
printf("Selected optimizer: %s\r\n", s_opti);

%% 3D Calculation
file = load("../matrix_out.mat");

points = file.y;
rotations = file.A(1:end, 4:6);

xs = [];
ys = [];
zs = [];

i = 1;
while i <= max(size(points))
  xs = [xs points(i)];
  ys = [ys points(i+1)];
  zs = [zs points(i+2)];
  i = i+3;
endwhile

z1 = 0.34;
z2 = 0.018;
z3 = -0.023;
R = 0.10;
p0 = [z1 z2 z3 R];

[p, r, iter, dur] = optimize_sphere(xs, ys, zs, p0, optimizer);

if (p == 0) | (r == 0)
    return
end

%% Show Results 3D
printf("Finished in %d iterations and %d seconds.\r\n", iter, dur);
disp("Result parameters: ");
disp(p);
disp("Result residual: ");
disp(r);
printf("Result residual norm: %d\r\n", norm(r));

figure 1
scatter3(xs, ys, zs, 'filled');
hold on;
draw_sphere(p0);
hold off;
axis('equal');
title(sprintf("Start Circle"));

result_figure = figure(2);
scatter3(xs, ys, zs, 'filled');
hold on;
draw_sphere(p);
hold off;
axis('equal');

title(sprintf("Timing: %d iterations, %d seconds   \r\nParameter (z1, z2, z3, R): [%d, %d, %d, %d]   \r\nResidual norm: %d", iter, dur, p(1), p(2), p(3), p(4), norm(r)));

save ("result/result_3D.mat", 'p', 'r');
saveas (result_figure, "result/result_3D.png");

%% Save vector for pen head
center = p(1:3);

% get points in matrix
points = get_points_3D(xs, ys, zs);

% init search variables
vector = [0 0 0];
closest_distance = 100000;
best_vector = [];
best_rotation = [];

% look for best point
for i = 1:size(points, 1)
  current_point = points(i,:);
  current_vector = center - current_point;
  
  rot_start_idx = (i-1)*3+1;
  current_rotation = -1 * rotations(rot_start_idx:rot_start_idx+2, 1:end);
  
  current_distance = abs(norm(current_vector) - p(4));
  if current_distance < closest_distance
    best_vector = current_vector;
    best_rotation = current_rotation;
    closest_distance = current_distance; 
  endif
endfor

% invert best vector with its rotation
vector = best_vector * inv(best_rotation);

% print result to file
out_file = fopen("../pen_config.txt", 'w');

fprintf(out_file, "%d\n%d\n%d", vector(1), vector(2), vector(3))
fclose(out_file);
