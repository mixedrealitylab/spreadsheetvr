function draw_sphere(params)
    [xs, ys, zs] = sphere;
    
    % coordinate * sphere_radius + sphere_center_offset
    xs = xs * params(4) + params(1);
    ys = ys * params(4) + params(2);
    zs = zs * params(4) + params(3);
    hidden('on');
    mesh(xs, ys, zs);
    hidden('off');
end